import 'package:UGoBike/EmailRegister.dart';
import 'package:UGoBike/ValidateRail.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './BottomBar.dart';
import './Unlock.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'),
      ],
      home: ButtonDemo(),
    ),
  );
}

class ButtonDemo extends StatefulWidget {
  @override
  ButtonDemoState createState() => ButtonDemoState();
}

class ButtonDemoState extends State<ButtonDemo> {
  TextEditingController RailCodeController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String _scanBarcode = 'Unknown';

  Future scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });

    if (_scanBarcode != null) {
      Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => ValidateRail(_scanBarcode)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: new AppBar(
            title: new Text("Unlock a Bike"),
          ),
          body: new Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(
                  'Rent a Bike',
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30.0,
                    fontFamily: 'Roboto',
                  ),
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new RaisedButton(
                      padding: const EdgeInsets.all(8.0),
                      textColor: Colors.white,
                      color: Colors.blue,
                      onPressed: scanQR,
                      child: new Text("QR Scan"),
                    ),
                    new RaisedButton(
                      textColor: Colors.white,
                      color: Colors.red,
                      padding: const EdgeInsets.all(8.0),
                      child: new Text(
                        "Manual Insert",
                      ),
                      onPressed: () async {
                        return showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                title: Text('Insert Rail Number'),
                                content: SingleChildScrollView(
                                    child: new Form(
                                  key: _formKey,
                                  child: new Column(
                                    children: <Widget>[
                                      new TextFormField(
                                        controller: RailCodeController,
                                        decoration: InputDecoration(
                                          hintText: 'R-xxxxx',
                                        ),
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return 'Enter the Rail Number';
                                          }
                                          return null;
                                        },
                                      ),
                                    ],
                                  ),
                                )),
                                actions: [
                                  RaisedButton(
                                      color: const Color(0xffbfa780),
                                      onPressed: () async {
                                        if (_formKey.currentState.validate()) {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      ValidateRail(
                                                          RailCodeController
                                                              .text)));
                                        }
                                      },
                                      child: Text(
                                        'CONFIRM',
                                        style: new TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontSize: 12,
                                          color: const Color(0xffffffff),
                                          fontWeight: FontWeight.w700,
                                          height: 2.5,
                                        ),
                                        textAlign: TextAlign.center,
                                      ))
                                ],
                              );
                            });
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
