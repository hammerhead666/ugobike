import 'dart:async';
import 'package:flutter/material.dart';
import '../../dbLocator.dart';
import '../services/api.dart';
import '../models/bicycleStopModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class bicycleStopCRUDModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<BicycleStop> stops;

/*  Future<List<BicycleStop>> fetchBicycleStop() async {
    var result = await _api.getDataCollection();
    stops = result.docs
        .map((doc) => BicycleStop.fromMap(doc.data, doc.documentID))
        .toList();
    return stops;
  }*/

  Stream<QuerySnapshot> fetchBicycleStopAsStream() {
    return _api.streamDataCollection();
  }

 /* Future<BicycleStop> getBicycleStopById(String id) async {
    var doc = await _api.getDocumentById(id);
    return  BicycleStop.fromMap(doc.data, doc.documentID) ;
  }*/

  Future removeBicycleStop(String id) async{
    await _api.removeDocument(id) ;
    return ;
  }

  Future updateBicycleStop(BicycleStop data,String id) async{
    await _api.updateDocument(data.toJson(), id) ;
    return ;
  }

  Future addBicycleStop(BicycleStop data) async{
    var result  = await _api.addDocument(data.toJson()) ;

    return ;
  }

  Future setBicycleStop(BicycleStop data, int docCount) async{
    String curId;

    if(docCount >= 100) {
      curId = "S" + docCount.toString();
    }
    else if(docCount >= 10) {
      curId = "S0" + docCount.toString();
    }
    else{
      curId = "S00" + docCount.toString();
    }

    data.stopID = curId;
    var result  = await _api.setDocument(data.toJson(), curId) ;

    return ;
  }

}