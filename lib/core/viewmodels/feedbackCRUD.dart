import 'dart:async';
import 'package:flutter/material.dart' hide Feedback;
import '../../dbLocator.dart';
import '../services/api.dart';
import '../models/feedbackModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class feedbackCRUDModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Feedback> feedbacks;

/*  Future<List<Feedback>> fetchFeedback() async {
    var result = await _api.getDataCollection();
    feedbacks = result.documents
        .map((doc) => Feedback.fromMap(doc.data, doc.documentID))
        .toList();
    return feedbacks;
  }*/

  Stream<QuerySnapshot> fetchFeedbackAsStream() {
    return _api.streamDataCollection();
  }

/*  Future<Feedback> getFeedbackById(String id) async {
    var doc = await _api.getDocumentById(id);
    return  Feedback.fromMap(doc.data, doc.documentID) ;
  }*/


  Future removeFeedback(String id) async{
    await _api.removeDocument(id) ;
    return ;
  }
  Future updateFeedback(Feedback data,String id) async{
    await _api.updateDocument(data.toJson(), id) ;
    return ;
  }

  Future addFeedback(Feedback data) async{
    var result  = await _api.addDocument(data.toJson()) ;

    return ;
  }

  Future setFeedback(Feedback data, int docCount) async{
    String curId;

    if(docCount >= 100) {
      curId = "F" + docCount.toString();
    }
    else if(docCount >= 10) {
      curId = "F0" + docCount.toString();
    }
    else{
      curId = "F00" + docCount.toString();
    }
    data.feedbackID = curId;
    var result  = await _api.setDocument(data.toJson(), curId) ;

    return ;
  }

}