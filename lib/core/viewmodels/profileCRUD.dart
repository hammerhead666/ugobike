import 'dart:async';
import 'package:flutter/material.dart';
import '../../dbLocator.dart';
import '../services/api.dart';
import '../models/profileModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class profileCRUDModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Profile> users;

/*  Future<List<Account>> fetchUser() async {
    var result = await _api.getDataCollection();
    users = result.docs
        .map((snapshot) => Account.fromMap(doc.data, doc.id))
        .toList();
    return users;
  }*/

  Stream<QuerySnapshot> fetchUserAsStream() {
    return _api.streamDataCollection();
  }

/*  Future<Account> getUserById(String id) async {
    var doc = await _api.getDocumentById(id);
    return  Account.fromMap(doc.data, doc.id) ;
  }*/


  Future removeUser(String id) async{
    await _api.removeDocument(id) ;
    return ;
  }
  Future updateUser(Profile data,String id) async{
    await _api.updateDocument(data.toJson(), id) ;
    return ;
  }

  Future addUser(Profile data) async{
    var result  = await _api.addDocument(data.toJson()) ;

    return ;
  }

  Future setUser(Profile data, String id) async{
    var result  = await _api.setDocument (data.toJson(), id) ;

    return ;
  }

}