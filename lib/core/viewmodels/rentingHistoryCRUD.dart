import 'dart:async';
import 'package:flutter/material.dart';
import '../../dbLocator.dart';
import '../services/api.dart';
import '../models/rentingHistoryModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class rentingHistoryCRUDModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<RentingHistory> history;

/*  Future<List<RentingHistory>> fetchRentingHistory() async {
    var result = await _api.getDataCollection();
    history = result.docs
        .map((doc) => RentingHistory.fromMap(doc.data, doc.documentID))
        .toList();
    return history;
  }*/

  Stream<QuerySnapshot> fetchRentingHistoryAsStream() {
    return _api.streamDataCollection();
  }

/*  Future<RentingHistory> getRentingHistoryById(String id) async {
    var doc = await _api.getDocumentById(id);
    return  RentingHistory.fromMap(doc.data, doc.id) ;
  }*/

  Future removeRentingHistory(String id) async{
    await _api.removeDocument(id) ;
    return ;
  }

  Future updateRentingHistory(RentingHistory data,String id) async{
    await _api.updateDocument(data.toJson(), id) ;
    return ;
  }

  Future addRentingHistory(RentingHistory data) async{
    var result  = await _api.addDocument(data.toJson()) ;

    return ;
  }

  // Future setRentingHistory(RentingHistory data, int docCount) async{
  //   String curId;
  //
  //   if(docCount >= 100) {
  //     curId = "H" + docCount.toString();
  //   }
  //   else if(docCount >= 10) {
  //     curId = "H0" + docCount.toString();
  //   }
  //   else{
  //     curId = "H00" + docCount.toString();
  //   }
  //
  //   data.historyID = curId;
  //   var result  = await _api.setDocument(data.toJson(), curId) ;
  //
  //   return ;
  // }

}