import 'dart:async';
import 'package:flutter/material.dart';
import '../../dbLocator.dart';
import '../services/api.dart';
import '../models/railModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class railCRUDModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Rail> rails;

/*
  Future<List<Rail>> fetchRail() async {
    var result = await _api.getDataCollection();
    rails = result.docs
        .map((doc) => Rail.fromMap(doc.data, doc.documentID))
        .toList();
    return rails;
  }
*/

  Stream<QuerySnapshot> fetchRailAsStream() {
    return _api.streamDataCollection();
  }

/*
  Future<Rail> getRailById(String id) async {
    var doc = await _api.getDocumentById(id);
    return  Rail.fromMap(doc.data, doc.id) ;
  }
*/


  Future removeRail(String id) async{
    await _api.removeDocument(id) ;
    return ;
  }
  Future updateRail(Rail data,String id) async{
    await _api.updateDocument(data.toJson(), id) ;
    return ;
  }

  Future addRail(Rail data) async{
    var result  = await _api.addDocument(data.toJson()) ;

    return ;
  }

  Future setRail(Rail data, int docCount) async{
    String curId;

    if(docCount >= 100) {
      curId = "R" + docCount.toString();
    }
    else if(docCount >= 10) {
      curId = "R0" + docCount.toString();
    }
    else{
      curId = "R00" + docCount.toString();
    }

    data.railID = curId;
    var result  = await _api.setDocument(data.toJson(), curId) ;

    return ;
  }

}