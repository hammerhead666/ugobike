import 'dart:async';
import 'package:flutter/material.dart';
import '../../dbLocator.dart';
import '../services/api.dart';
import '../models/bicycleModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class bicycleCRUDModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Bicycle> bicycles;

/*  Future<List<Bicycle>> fetchBicycle() async {
    var result = await _api.getDataCollection();
    bicycles = result.docs
        .map((doc) => Bicycle.fromMap(doc.data, doc.id))
        .toList();
    return bicycles;
  }*/

  Stream<QuerySnapshot> fetchBicycleAsStream() {
    return _api.streamDataCollection();
  }

/*  Future<Bicycle> getBicycleById(String id) async {
    var doc = await _api.getDocumentById(id);
    return  Bicycle.fromMap(doc.data, doc.id) ;
  }*/


  Future removeBicycle(String id) async{
    await _api.removeDocument(id) ;
    return ;
  }
  Future updateBicycle(Bicycle data,String id) async{
    await _api.updateDocument(data.toJson(), id) ;
    return ;
  }

  Future addBicycle(Bicycle data) async{
    var result  = await _api.addDocument(data.toJson()) ;

    return ;
  }

  Future setBicycle(Bicycle data, int docCount) async{
    String curId;

    if(docCount >= 100) {
      curId = "B" + docCount.toString();
    }
    else if(docCount >= 10) {
      curId = "B0" + docCount.toString();
    }
    else{
      curId = "B00" + docCount.toString();
    }

    data.bicycleID = curId;
    var result  = await _api.setDocument(data.toJson(), curId) ;

    return ;
  }

}