class BicycleStop {
  String stopID;
  String stopName;
  double latitude;
  double longitude;

  BicycleStop({this.stopID, this.stopName, this.latitude, this.longitude});

  BicycleStop.fromMap(Map snapshot,String id) :
        stopID = snapshot['stopID'] ?? '',
        stopName = snapshot['stopName'] ?? '',
        latitude = snapshot['latitude'] ?? '',
        longitude = snapshot['longitude'] ?? '';

  toJson() {
    return {
      "stopID": stopID,
      "stopName": stopName,
      "latitude": latitude,
      "longitude": longitude,
    };
  }
}