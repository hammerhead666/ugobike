class Rail {
  String railDBID;
  String railID;
  bool status;
  String bicycleID;
  String stopID;

  Rail({this.railDBID, this.railID, this.status, this.bicycleID, this.stopID});

  Rail.fromMap(Map snapshot,String id) :
        railDBID = id ?? '',
        railID = snapshot['railID'] ?? '',
        status = snapshot['status'] ?? '',
        bicycleID = snapshot['bicycleID'] ?? '',
        stopID = snapshot['stopID'] ?? '';

  toJson() {
    return {
      "railID": railID,
      "status": status,
      "bicycleID": bicycleID,
      "stopID": stopID,
    };
  }
}