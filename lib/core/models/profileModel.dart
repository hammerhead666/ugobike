class Profile {
  String userDBID;
  String userName;
  String userPhone;
  String userEmail;
  String userGender;

  Profile({this.userDBID, this.userName, this.userPhone, this.userEmail, this.userGender});

  Profile.fromMap(Map snapshot,String id) :
        userDBID = id ?? '',
        userName = snapshot['userName'] ?? '',
        userPhone = snapshot['userPhone'] ?? '',
        userEmail = snapshot['userEmail'] ?? '',
        userGender = snapshot['userGender'] ?? '';

  toJson() {
    return {
      "userName": userName,
      "userPhone": userPhone,
      "userEmail": userEmail,
      "userGender": userGender,
    };
  }
}