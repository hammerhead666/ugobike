import 'package:cloud_firestore/cloud_firestore.dart';

class RentingHistory {
  Timestamp dateRented;
  String rentStop;
  String returnStop;
  double totalMin;
  double rentingFee;
  String userID;
  String bicycleID;

  RentingHistory({this.dateRented, this.rentStop, this.returnStop, this.totalMin, this.rentingFee, this.userID, this.bicycleID});

  RentingHistory.fromMap(Map snapshot,String id) :
        dateRented = snapshot['dateRented'] ?? '',
        rentStop = snapshot['rentStop'] ?? '',
        returnStop = snapshot['returnStop'] ?? '',
        totalMin = snapshot['totalMin'] ?? '',
        rentingFee = snapshot['rentingFee'] ?? '',
        userID = snapshot['userID'] ?? '',
        bicycleID = snapshot['bicycleID'] ?? '';

  toJson() {
    return {
      "dateRented": dateRented,
      "rentStop": rentStop,
      "returnStop": returnStop,
      "rentingFee": rentingFee,
      "totalMin": totalMin,
      "userID": userID,
      "bicycleID": bicycleID,
    };
  }
}