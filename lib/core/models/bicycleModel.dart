import 'package:cloud_firestore/cloud_firestore.dart';

class Bicycle  {
  String bicycleDBID;
  String bicycleID;
  Timestamp dateRegister; //use Timestamp.now() while creating / updating to get server time

  Bicycle ({this.bicycleDBID, this.bicycleID, this.dateRegister});

  Bicycle .fromMap(Map snapshot,String id) :
        bicycleDBID = id ?? '',
        bicycleID = snapshot['bicycleID'] ?? '',
        dateRegister = snapshot['dateRegister'] ?? '';

  toJson() {
    return {
      "bicycleID": bicycleID,
      "dateRegister": dateRegister,
    };
  }
}