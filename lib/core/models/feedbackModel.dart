import 'package:cloud_firestore/cloud_firestore.dart';

class Feedback {
  String feedbackDBID;
  String feedbackID;
  String feedback;
  String category;
  String problem;
  Timestamp dateReported;
  String userID;
  String bicycleID;
  String stopID;

  Feedback({this.feedbackDBID, this.feedbackID, this.feedback, this.category, this.problem, this.dateReported, this.userID, this.bicycleID, this.stopID});

  Feedback.fromMap(Map snapshot,String id) :
        feedbackDBID = id ?? '',
        feedbackID = snapshot['feedbackID'] ?? '',
        feedback = snapshot['feedback'] ?? '',
        category = snapshot['category'] ?? '',
        problem = snapshot['problem'] ?? '',
        dateReported = snapshot['dateReported'] ?? '',
        userID = snapshot['userID'] ?? '',
        bicycleID = snapshot['bicycleID'] ?? '',
        stopID = snapshot['stopID'] ?? '';

  toJson() {
    return {
      "feedbackID": feedbackID,
      "feedback": feedback,
      "category": category,
      "problem": problem,
      "dateReported": dateReported,
      "userID": userID,
      "bicycleID": bicycleID,
      "stopID": stopID,
    };
  }
}