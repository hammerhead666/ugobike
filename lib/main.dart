import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import './SplashScreen.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'UGoBike',
      navigatorKey: navigatorKey,
      home: SplashScreen(),
    );
  }
}


