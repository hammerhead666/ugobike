import 'package:get_it/get_it.dart';

import './core/services/api.dart';
import './core/viewmodels/profileCRUD.dart';
import './core/viewmodels/profileCRUD.dart';
import './core/viewmodels/bicycleCRUD.dart';
import './core/viewmodels/rentingHistoryCRUD.dart';
import './core/viewmodels/bicycleStopCRUD.dart';
import './core/viewmodels/railCRUD.dart';
import './core/viewmodels/feedbackCRUD.dart';


GetIt locator = GetIt();


void setupBicycleLocator() {
  locator.registerLazySingleton(() => Api('Bicycle'));
  locator.registerLazySingleton(() => bicycleCRUDModel()) ;
}

void setupBicycleStopLocator() {
  locator.registerLazySingleton(() => Api('BicycleStop'));
  locator.registerLazySingleton(() => bicycleStopCRUDModel()) ;
}

void setupFeedbackLocator() {
  locator.registerLazySingleton(() => Api('Feedback'));
  locator.registerLazySingleton(() => feedbackCRUDModel()) ;
}

void setupRailLocator() {
  locator.registerLazySingleton(() => Api('Rail'));
  locator.registerLazySingleton(() => railCRUDModel()) ;
}

void setupRentingHistoryLocator() {
  locator.registerLazySingleton(() => Api('RentingHistory'));
  locator.registerLazySingleton(() => rentingHistoryCRUDModel()) ;
}

void setupUserLocator() {
  locator.registerLazySingleton(() => Api('Account'));
  locator.registerLazySingleton(() => profileCRUDModel()) ;
}

