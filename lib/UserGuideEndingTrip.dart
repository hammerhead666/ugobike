import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import './UserGuide.dart';

class UserGuideEndingTrip extends StatelessWidget {
  UserGuideEndingTrip({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Image.asset(
            "assets/images/User Guide – Ending Trip.png",
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.center,
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(5.0, 30.5, 71.0, 65.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => UserGuide())),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                  color: const Color(0x00ffffff),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
