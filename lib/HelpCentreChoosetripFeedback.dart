import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './HelpCentreGiveFeedback.dart';

class HelpCentreChoosetripFeedback extends StatefulWidget{
  @override
  HelpCentreChoosetripFeedbackState createState(){
    return HelpCentreChoosetripFeedbackState();
  }
}
class HelpCentreChoosetripFeedbackState extends State<HelpCentreChoosetripFeedback> {

  CollectionReference ref = FirebaseFirestore.instance.collection('RentingHistory');
  final FirebaseAuth auth = FirebaseAuth.instance;
  final DateFormat formatter = DateFormat('yyyy-MM-dd h:m');

  int _selectedSort = 0;
  List<DropdownMenuItem<int>> sortList = [];

  void loadSortList() {
    sortList = [];
    sortList.add(new DropdownMenuItem(
      child: new Text('Sort by Date (Latest to Oldest)'),
      value: 0,
    ));
    sortList.add(new DropdownMenuItem(
      child: new Text('Sort by Date (Oldest to Latest)'),
      value: 1,
    ));
    sortList.add(new DropdownMenuItem(
      child: new Text('Sort by Fee (Lowest to Highest)'),
      value: 2,
    ));
    sortList.add(new DropdownMenuItem(
      child: new Text('Sort by Fee (Highest to Lowest)'),
      value: 3,
    ));
  }

  List<Widget> getFormWidget(){
    List<Widget> formWidget = new List();
    formWidget.add(new DropdownButton(
      items: sortList,
      value: _selectedSort,
      hint: new Text('Sort by'),
      onChanged: (value) {
        setState(() {
          _selectedSort = value;
        });
      },
      isExpanded: true,
    ));

    return formWidget;
  }

  getCurrentUser() {
    final User user = auth.currentUser;
    final uid = user.uid;

    return uid;
  }

  @override
  Widget build(BuildContext context) {
    loadSortList();
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            height: 150,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),
            child: Container(
              margin: const EdgeInsets.only(top:0),
              child:Image(
                height: 160,
                image: const AssetImage('assets/images/logo.png'),
              ),
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 15),
            alignment: Alignment.center,
            child: Text(
              'Choose The Trip To Feedback On',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 20,
                color: const Color(0xff656d74),
              ),
            )
          ),

          Container(
              margin: EdgeInsets.only(bottom: 10, left: 35, right: 35),
              child: new ListView(
                shrinkWrap: true,
                children: getFormWidget(),
              )
          ),

          if(_selectedSort == 0)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where("userID", isEqualTo: getCurrentUser()).orderBy('dateRented', descending: true).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => HelpCentreGiveFeedback(
                                            doc.data()['bicycleID'],
                                            doc.data()['rentStop'],
                                            doc.data()['returnStop'])));
                                      },
                                      child: Container(
                                        height: 160,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black45,
                                          ),
                                          borderRadius: BorderRadius.circular(10),
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                          ],
                                        ),
                                        margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                        padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                        child: Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 4,
                                                  child: Text(
                                                    doc['bicycleID'],
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 6,
                                                  child: Text(
                                                    formatter.format(doc['dateRented'].toDate()).toString(),
                                                    textAlign: TextAlign.right,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),

                                            Divider(
                                              thickness: 1,
                                              color: Colors.black54,
                                            ),

                                            Container(
                                              margin: const EdgeInsets.only(left:5),
                                              child: Row(
                                                children: <Widget>[
                                                  Image(
                                                    height: 22,
                                                    image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                                  ),

                                                  if(doc['rentStop'] == "S001")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Angkasa",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S002")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("M01, Kolej Tun Dr Ismail",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S003")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("L01, Kolej Tun Hussein Onn",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S004")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Cengal",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S005")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("K01, Kolej Tun Razak",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S006")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Food Court Kolej Datin Seri Endon",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S007")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("H01, Kolej Tun Fatimah",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S008")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("L50, Lecture Hall",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S009")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Meranti",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S010")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("G01, Kolej Rahman Putra",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S011")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("N28a, School of Computing",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S012")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Perpustakaan Sultanah Zanariah, PSZ",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 13,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S013")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("P19, Lecture Hall",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S014")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Dewan Astana, Kolej Tuanku Canselor",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 13,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S015")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Lestari",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S016")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Stadium Azman Hashim, UTM",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S017")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Scholar's Inn",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S018")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Perpustakaan Raja Zarith Sofiah",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),
                                                ],
                                              ),
                                            ),

                                            Row(
                                                children: <Widget>[
                                                  Container(
                                                    margin: const EdgeInsets.only(left:15),
                                                    child:Image(
                                                      height: 12,
                                                      image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                    ),
                                                  )
                                                ]
                                            ),

                                            Row(
                                              children: <Widget>[
                                                Image(
                                                  height: 40,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                                ),

                                                if(doc['returnStop'] == "S001")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Angkasa",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S002")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("M01, Kolej Tun Dr Ismail",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S003")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("L01, Kolej Tun Hussein Onn",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S004")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Cengal",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S005")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("K01, Kolej Tun Razak",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S006")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Food Court Kolej Datin Seri Endon",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S007")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("H01, Kolej Tun Fatimah",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S008")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("L50, Lecture Hall",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S009")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Meranti",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S010")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("G01, Kolej Rahman Putra",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S011")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("N28a, School of Computing",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S012")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Perpustakaan Sultanah Zanariah, PSZ",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 13,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S013")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("P19, Lecture Hall",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S014")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Dewan Astana, Kolej Tuanku Canselor",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 13,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S015")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Lestari",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S016")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Stadium Azman Hashim, UTM",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S017")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Scholar's Inn",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S018")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Perpustakaan Raja Zarith Sofiah",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),
                                              ],
                                            ),

                                            Container(
                                              margin: const EdgeInsets.only(top:10, left:5, right: 5),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      flex: 6,
                                                      child: Text
                                                        (doc['totalMin'].toInt().toString() + " min " + ((doc['totalMin'] * 60) - (doc['totalMin'].toInt() * 60 )).toInt().toString() + " sec",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 14,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),
                                                  Expanded(
                                                    flex: 4,
                                                    child: Text(
                                                      "RM " + doc['rentingFee'].toStringAsFixed(2),
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 14,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                      textAlign: TextAlign.right,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      )))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedSort == 1)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where("userID", isEqualTo: getCurrentUser()).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => HelpCentreGiveFeedback(
                                            doc.data()['bicycleID'],
                                            doc.data()['rentStop'],
                                            doc.data()['returnStop'])));
                                      },
                                      child: Container(
                                        height: 160,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black45,
                                          ),
                                          borderRadius: BorderRadius.circular(10),
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                          ],
                                        ),
                                        margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                        padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                        child: Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 4,
                                                  child: Text(
                                                    doc['bicycleID'],
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 6,
                                                  child: Text(
                                                    formatter.format(doc['dateRented'].toDate()).toString(),
                                                    textAlign: TextAlign.right,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),

                                            Divider(
                                              thickness: 1,
                                              color: Colors.black54,
                                            ),

                                            Container(
                                              margin: const EdgeInsets.only(left:5),
                                              child: Row(
                                                children: <Widget>[
                                                  Image(
                                                    height: 22,
                                                    image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                                  ),

                                                  if(doc['rentStop'] == "S001")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Angkasa",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S002")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("M01, Kolej Tun Dr Ismail",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S003")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("L01, Kolej Tun Hussein Onn",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S004")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Cengal",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S005")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("K01, Kolej Tun Razak",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S006")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Food Court Kolej Datin Seri Endon",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S007")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("H01, Kolej Tun Fatimah",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S008")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("L50, Lecture Hall",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S009")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Meranti",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S010")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("G01, Kolej Rahman Putra",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S011")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("N28a, School of Computing",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S012")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Perpustakaan Sultanah Zanariah, PSZ",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 13,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S013")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("P19, Lecture Hall",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S014")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Dewan Astana, Kolej Tuanku Canselor",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 13,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S015")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Lestari",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S016")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Stadium Azman Hashim, UTM",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S017")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Scholar's Inn",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S018")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Perpustakaan Raja Zarith Sofiah",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),
                                                ],
                                              ),
                                            ),

                                            Row(
                                                children: <Widget>[
                                                  Container(
                                                    margin: const EdgeInsets.only(left:15),
                                                    child:Image(
                                                      height: 12,
                                                      image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                    ),
                                                  )
                                                ]
                                            ),

                                            Row(
                                              children: <Widget>[
                                                Image(
                                                  height: 40,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                                ),

                                                if(doc['returnStop'] == "S001")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Angkasa",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S002")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("M01, Kolej Tun Dr Ismail",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S003")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("L01, Kolej Tun Hussein Onn",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S004")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Cengal",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S005")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("K01, Kolej Tun Razak",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S006")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Food Court Kolej Datin Seri Endon",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S007")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("H01, Kolej Tun Fatimah",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S008")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("L50, Lecture Hall",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S009")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Meranti",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S010")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("G01, Kolej Rahman Putra",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S011")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("N28a, School of Computing",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S012")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Perpustakaan Sultanah Zanariah, PSZ",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 13,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S013")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("P19, Lecture Hall",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S014")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Dewan Astana, Kolej Tuanku Canselor",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 13,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S015")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Lestari",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S016")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Stadium Azman Hashim, UTM",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S017")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Scholar's Inn",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S018")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Perpustakaan Raja Zarith Sofiah",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),
                                              ],
                                            ),

                                            Container(
                                              margin: const EdgeInsets.only(top:10, left:5, right: 5),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      flex: 6,
                                                      child: Text
                                                        (doc['totalMin'].toInt().toString() + " min " + ((doc['totalMin'] * 60) - (doc['totalMin'].toInt() * 60 )).toInt().toString() + " sec",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 14,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),
                                                  Expanded(
                                                    flex: 4,
                                                    child: Text(
                                                      "RM " + doc['rentingFee'].toStringAsFixed(2),
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 14,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                      textAlign: TextAlign.right,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      )))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedSort == 2)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where("userID", isEqualTo: getCurrentUser()).orderBy('rentingFee').get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => HelpCentreGiveFeedback(
                                            doc.data()['bicycleID'],
                                            doc.data()['rentStop'],
                                            doc.data()['returnStop'])));
                                      },
                                      child: Container(
                                        height: 160,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black45,
                                          ),
                                          borderRadius: BorderRadius.circular(10),
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                          ],
                                        ),
                                        margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                        padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                        child: Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 4,
                                                  child: Text(
                                                    doc['bicycleID'],
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 6,
                                                  child: Text(
                                                    formatter.format(doc['dateRented'].toDate()).toString(),
                                                    textAlign: TextAlign.right,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),

                                            Divider(
                                              thickness: 1,
                                              color: Colors.black54,
                                            ),

                                            Container(
                                              margin: const EdgeInsets.only(left:5),
                                              child: Row(
                                                children: <Widget>[
                                                  Image(
                                                    height: 22,
                                                    image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                                  ),

                                                  if(doc['rentStop'] == "S001")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Angkasa",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S002")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("M01, Kolej Tun Dr Ismail",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S003")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("L01, Kolej Tun Hussein Onn",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S004")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Cengal",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S005")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("K01, Kolej Tun Razak",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S006")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Food Court Kolej Datin Seri Endon",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S007")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("H01, Kolej Tun Fatimah",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S008")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("L50, Lecture Hall",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S009")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Meranti",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S010")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("G01, Kolej Rahman Putra",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S011")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("N28a, School of Computing",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S012")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Perpustakaan Sultanah Zanariah, PSZ",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 13,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S013")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("P19, Lecture Hall",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S014")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Dewan Astana, Kolej Tuanku Canselor",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 13,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S015")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Lestari",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S016")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Stadium Azman Hashim, UTM",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S017")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Scholar's Inn",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S018")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Perpustakaan Raja Zarith Sofiah",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),
                                                ],
                                              ),
                                            ),

                                            Row(
                                                children: <Widget>[
                                                  Container(
                                                    margin: const EdgeInsets.only(left:15),
                                                    child:Image(
                                                      height: 12,
                                                      image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                    ),
                                                  )
                                                ]
                                            ),

                                            Row(
                                              children: <Widget>[
                                                Image(
                                                  height: 40,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                                ),

                                                if(doc['returnStop'] == "S001")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Angkasa",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S002")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("M01, Kolej Tun Dr Ismail",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S003")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("L01, Kolej Tun Hussein Onn",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S004")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Cengal",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S005")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("K01, Kolej Tun Razak",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S006")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Food Court Kolej Datin Seri Endon",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S007")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("H01, Kolej Tun Fatimah",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S008")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("L50, Lecture Hall",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S009")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Meranti",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S010")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("G01, Kolej Rahman Putra",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S011")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("N28a, School of Computing",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S012")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Perpustakaan Sultanah Zanariah, PSZ",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 13,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S013")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("P19, Lecture Hall",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S014")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Dewan Astana, Kolej Tuanku Canselor",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 13,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S015")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Lestari",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S016")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Stadium Azman Hashim, UTM",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S017")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Scholar's Inn",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S018")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Perpustakaan Raja Zarith Sofiah",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),
                                              ],
                                            ),

                                            Container(
                                              margin: const EdgeInsets.only(top:10, left:5, right: 5),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      flex: 6,
                                                      child: Text
                                                        (doc['totalMin'].toInt().toString() + " min " + ((doc['totalMin'] * 60) - (doc['totalMin'].toInt() * 60 )).toInt().toString() + " sec",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 14,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),
                                                  Expanded(
                                                    flex: 4,
                                                    child: Text(
                                                      "RM " + doc['rentingFee'].toStringAsFixed(2),
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 14,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                      textAlign: TextAlign.right,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      )))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedSort == 3)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where("userID", isEqualTo: getCurrentUser()).orderBy('rentingFee', descending: true).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => HelpCentreGiveFeedback(
                                            doc.data()['bicycleID'],
                                            doc.data()['rentStop'],
                                            doc.data()['returnStop'])));
                                      },
                                      child: Container(
                                        height: 160,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black45,
                                          ),
                                          borderRadius: BorderRadius.circular(10),
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                          ],
                                        ),
                                        margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                        padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                        child: Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 4,
                                                  child: Text(
                                                    doc['bicycleID'],
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 6,
                                                  child: Text(
                                                    formatter.format(doc['dateRented'].toDate()).toString(),
                                                    textAlign: TextAlign.right,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),

                                            Divider(
                                              thickness: 1,
                                              color: Colors.black54,
                                            ),

                                            Container(
                                              margin: const EdgeInsets.only(left:5),
                                              child: Row(
                                                children: <Widget>[
                                                  Image(
                                                    height: 22,
                                                    image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                                  ),

                                                  if(doc['rentStop'] == "S001")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Angkasa",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S002")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("M01, Kolej Tun Dr Ismail",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S003")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("L01, Kolej Tun Hussein Onn",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S004")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Cengal",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S005")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("K01, Kolej Tun Razak",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S006")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Food Court Kolej Datin Seri Endon",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S007")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("H01, Kolej Tun Fatimah",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S008")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("L50, Lecture Hall",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S009")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Meranti",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S010")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("G01, Kolej Rahman Putra",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S011")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("N28a, School of Computing",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S012")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Perpustakaan Sultanah Zanariah, PSZ",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 13,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S013")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("P19, Lecture Hall",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S014")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Dewan Astana, Kolej Tuanku Canselor",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 13,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S015")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Arked Lestari",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S016")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Stadium Azman Hashim, UTM",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S017")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Scholar's Inn",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),

                                                  if(doc['rentStop'] == "S018")
                                                    Container(
                                                        margin: const EdgeInsets.only(left:21),
                                                        child: Text
                                                          ("Perpustakaan Raja Zarith Sofiah",
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 15,
                                                            color: const Color(0xff656d74),
                                                          ),
                                                        )
                                                    ),
                                                ],
                                              ),
                                            ),

                                            Row(
                                                children: <Widget>[
                                                  Container(
                                                    margin: const EdgeInsets.only(left:15),
                                                    child:Image(
                                                      height: 12,
                                                      image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                    ),
                                                  )
                                                ]
                                            ),

                                            Row(
                                              children: <Widget>[
                                                Image(
                                                  height: 40,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                                ),

                                                if(doc['returnStop'] == "S001")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Angkasa",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S002")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("M01, Kolej Tun Dr Ismail",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S003")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("L01, Kolej Tun Hussein Onn",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S004")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Cengal",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S005")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("K01, Kolej Tun Razak",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S006")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Food Court Kolej Datin Seri Endon",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S007")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("H01, Kolej Tun Fatimah",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S008")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("L50, Lecture Hall",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S009")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Meranti",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S010")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("G01, Kolej Rahman Putra",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S011")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("N28a, School of Computing",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S012")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Perpustakaan Sultanah Zanariah, PSZ",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 13,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S013")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("P19, Lecture Hall",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S014")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Dewan Astana, Kolej Tuanku Canselor",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 13,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S015")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Arked Lestari",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S016")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Stadium Azman Hashim, UTM",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S017")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Scholar's Inn",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),

                                                if(doc['returnStop'] == "S018")
                                                  Container(
                                                      margin: const EdgeInsets.only(left:15),
                                                      child: Text
                                                        ("Perpustakaan Raja Zarith Sofiah",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 15,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),
                                              ],
                                            ),

                                            Container(
                                              margin: const EdgeInsets.only(top:10, left:5, right: 5),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      flex: 6,
                                                      child: Text
                                                        (doc['totalMin'].toInt().toString() + " min " + ((doc['totalMin'] * 60) - (doc['totalMin'].toInt() * 60 )).toInt().toString() + " sec",
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 14,
                                                          color: const Color(0xff656d74),
                                                        ),
                                                      )
                                                  ),
                                                  Expanded(
                                                    flex: 4,
                                                    child: Text(
                                                      "RM " + doc['rentingFee'].toStringAsFixed(2),
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 14,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                      textAlign: TextAlign.right,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      )))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),
        ],
      ),
    );
  }
}