import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_svg/flutter_svg.dart';
import './HelpCentreChoosetripIssue.dart';
import './HelpCentreChoosetripFeedback.dart';

class HelpCentre extends StatelessWidget {
  HelpCentre({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Pinned.fromSize(
            bounds: Rect.fromLTWH(95.0, 376.0, 186.0, 35.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: Text(
              'Help Centre',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 25,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 0.5, 375.0, 286.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50.0),
                  bottomLeft: Radius.circular(50.0),
                ),
                color: const Color(0xff656d74),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x26000000),
                    offset: Offset(0, 15),
                    blurRadius: 30,
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(76.0, 59.5, 224.0, 168.0),
            size: Size(375.0, 812.0),
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                  size: Size(224.0, 168.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                        size: Size(224.0, 168.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child:
                            // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                            Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(70.0),
                            image: DecorationImage(
                              image: const AssetImage('assets/images/logo.png'),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(71.0, 114.0, 81.0, 27.0),
                  size: Size(224.0, 168.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Text(
                    'UGoBike',
                    style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 20,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(38.0, 466.0, 125.0, 125.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => HelpCentreChoosetripIssue())),
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 125.0, 125.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        color: const Color(0xffffffff),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x14000000),
                            offset: Offset(5, 5),
                            blurRadius: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(4.0, 92.0, 118.0, 23.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinBottom: true,
                    fixedHeight: true,
                    child: Text(
                      'Report an Issue',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 15,
                        color: const Color(0xff656d74),
                        letterSpacing: -0.75,
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(35.0, 19.0, 55.0, 55.0),
                    size: Size(125.0, 125.0),
                    fixedWidth: true,
                    fixedHeight: true,
                    child:
                        // Adobe XD layer: 'report_problem-24px' (group)
                        Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 55.0, 55.0),
                          size: Size(55.0, 55.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_1lu5qv,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(1.0, 2.0, 53.0, 50.0),
                          size: Size(55.0, 55.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_ixh32c,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(213.0, 466.0, 125.0, 125.0),
            size: Size(375.0, 812.0),
            pinRight: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => HelpCentreChoosetripFeedback())),
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 125.0, 125.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        color: const Color(0xffffffff),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x14000000),
                            offset: Offset(5, 5),
                            blurRadius: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(6.5, 92.0, 112.0, 19.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinBottom: true,
                    fixedHeight: true,
                    child: Text(
                      'Write a Review ',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 15,
                        color: const Color(0xff656d74),
                        letterSpacing: -0.75,
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(35.0, 19.0, 55.0, 55.0),
                    size: Size(125.0, 125.0),
                    fixedWidth: true,
                    fixedHeight: true,
                    child:
                        // Adobe XD layer: 'rate_review-black-1…' (group)
                        Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 55.0, 55.0),
                          size: Size(55.0, 55.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_1lu5qv,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(2.0, 2.0, 51.0, 51.0),
                          size: Size(55.0, 55.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_mdaayn,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(43.0, 646.0, 263.0, 60.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 215.0, 18.0),
                  size: Size(263.0, 60.0),
                  pinLeft: true,
                  pinTop: true,
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Text(
                    'Need immediate assistance? ',
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 14,
                      color: const Color(0xff656d74),
                      fontWeight: FontWeight.w700,
                      height: 1.4285714285714286,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 28.0, 263.0, 32.0),
                  size: Size(263.0, 60.0),
                  pinLeft: true,
                  pinRight: true,
                  pinBottom: true,
                  fixedHeight: true,
                  child: Text.rich(
                    TextSpan(
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 13,
                        color: const Color(0xff656d74),
                        height: 1.2307692307692308,
                      ),
                      children: [
                        TextSpan(
                          text: 'Contact our customer service support at\n',
                        ),
                        TextSpan(
                          text: '+60184324432 ',
                          style: TextStyle(
                            color: const Color(0xffbfa780),
                          ),
                        ),
                        TextSpan(
                          text: 'or ',
                        ),
                        TextSpan(
                          text: '+60146557869',
                          style: TextStyle(
                            color: const Color(0xffbfa780),
                          ),
                        ),
                        TextSpan(
                          text: ' ',
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_1lu5qv =
    '<svg viewBox="0.0 0.0 55.0 55.0" ><path  d="M 0 0 L 55 0 L 55 55 L 0 55 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_ixh32c =
    '<svg viewBox="1.0 2.0 53.0 50.0" ><path transform="translate(0.0, 0.0)" d="M 27.50000190734863 12.49999904632568 L 45.64045715332031 46.73684310913086 L 9.359545707702637 46.73684310913086 L 27.50000190734863 12.49999904632568 M 27.50000190734863 2 L 1.00000011920929 52 L 54 52 L 27.50000190734863 2 Z M 29.90909385681152 38.84210586547852 L 25.09090995788574 38.84210586547852 L 25.09090995788574 44.10526275634766 L 29.90909385681152 44.10526275634766 L 29.90909385681152 38.84210586547852 Z M 29.90909385681152 23.05263137817383 L 25.09090995788574 23.05263137817383 L 25.09090995788574 33.57894897460938 L 29.90909385681152 33.57894897460938 L 29.90909385681152 23.05263137817383 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_mdaayn =
    '<svg viewBox="2.0 2.0 51.0 51.0" ><path  d="M 47.90000152587891 2 L 7.09999942779541 2 C 4.295000076293945 2 2.025499820709229 4.295000076293945 2.025499820709229 7.09999942779541 L 2 53 L 12.19999885559082 42.79999923706055 L 47.90000152587891 42.79999923706055 C 50.70499801635742 42.79999923706055 53 40.50500106811523 53 37.70000076293945 L 53 7.09999942779541 C 53 4.295000076293945 50.70499801635742 2 47.90000152587891 2 Z M 47.90000152587891 37.70000076293945 L 10.08350086212158 37.70000076293945 L 8.578999519348145 39.20449829101563 L 7.09999942779541 40.68350219726563 L 7.09999942779541 7.09999942779541 L 47.90000152587891 7.09999942779541 L 47.90000152587891 37.70000076293945 Z M 23.67499732971191 32.59999847412109 L 42.79999923706055 32.59999847412109 L 42.79999923706055 27.49999618530273 L 28.77499771118164 27.49999618530273 L 23.67499732971191 32.59999847412109 Z M 33.51799774169922 17.63149833679199 C 34.02799606323242 17.12149810791016 34.02799606323242 16.33099746704102 33.51799774169922 15.82100009918213 L 29.00449752807617 11.3075008392334 C 28.49449729919434 10.79750156402588 27.7039966583252 10.79750156402588 27.19399833679199 11.3075008392334 L 12.19999885559082 26.3014965057373 L 12.19999885559082 32.59999847412109 L 18.49849891662598 32.59999847412109 L 33.51799774169922 17.63149833679199 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
