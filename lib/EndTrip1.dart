import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EndTrip1 extends StatelessWidget {
  EndTrip1({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Pinned.fromSize(
            bounds: Rect.fromLTWH(-73.0, 27.0, 522.0, 759.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            pinBottom: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 522.0, 759.0),
                  size: Size(522.0, 759.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 522.0, 759.0),
                        size: Size(522.0, 759.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child: Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 0.0, 522.0, 759.0),
                              size: Size(522.0, 759.0),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child:
                                  // Adobe XD layer: '0' (shape)
                                  Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: const AssetImage(
                                        'assets/images/dummymap.png'),
                                    fit: BoxFit.fill,
                                    colorFilter: new ColorFilter.mode(
                                        Colors.black.withOpacity(0.4),
                                        BlendMode.dstIn),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(79.5, 399.0, 30.5, 42.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                  size: Size(30.5, 42.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                        size: Size(30.5, 42.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child: Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                              size: Size(30.5, 42.0),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child: Stack(
                                children: <Widget>[
                                  Pinned.fromSize(
                                    bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                                    size: Size(30.5, 42.0),
                                    pinLeft: true,
                                    pinRight: true,
                                    pinTop: true,
                                    pinBottom: true,
                                    child: SvgPicture.string(
                                      _svg_h180ld,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Pinned.fromSize(
                                    bounds: Rect.fromLTWH(3.0, 3.0, 25.0, 25.0),
                                    size: Size(30.5, 42.0),
                                    pinLeft: true,
                                    pinRight: true,
                                    pinTop: true,
                                    fixedHeight: true,
                                    child: SvgPicture.string(
                                      _svg_v5a3pk,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Pinned.fromSize(
                                    bounds: Rect.fromLTWH(6.0, 6.0, 19.0, 19.0),
                                    size: Size(30.5, 42.0),
                                    pinTop: true,
                                    fixedWidth: true,
                                    fixedHeight: true,
                                    child:
                                        // Adobe XD layer: 'pedal_bike-24px' (group)
                                        Stack(
                                      children: <Widget>[
                                        Pinned.fromSize(
                                          bounds: Rect.fromLTWH(
                                              0.0, 0.0, 19.0, 19.0),
                                          size: Size(19.0, 19.0),
                                          pinLeft: true,
                                          pinRight: true,
                                          pinTop: true,
                                          pinBottom: true,
                                          child: Stack(
                                            children: <Widget>[
                                              Pinned.fromSize(
                                                bounds: Rect.fromLTWH(
                                                    0.0, 0.0, 19.0, 19.0),
                                                size: Size(19.0, 19.0),
                                                pinLeft: true,
                                                pinRight: true,
                                                pinTop: true,
                                                pinBottom: true,
                                                child: Container(
                                                  decoration: BoxDecoration(),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Pinned.fromSize(
                                          bounds: Rect.fromLTWH(
                                              0.0, 2.0, 19.0, 15.0),
                                          size: Size(19.0, 19.0),
                                          pinLeft: true,
                                          pinRight: true,
                                          pinTop: true,
                                          pinBottom: true,
                                          child: Stack(
                                            children: <Widget>[
                                              Pinned.fromSize(
                                                bounds: Rect.fromLTWH(
                                                    0.0, 0.0, 19.0, 15.0),
                                                size: Size(19.0, 15.0),
                                                pinLeft: true,
                                                pinRight: true,
                                                pinTop: true,
                                                pinBottom: true,
                                                child: SvgPicture.string(
                                                  _svg_yky8kt,
                                                  allowDrawingOutsideViewBox:
                                                      true,
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 191.0, 375.0, 122.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: const Color(0xffffffff),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x14000000),
                    offset: Offset(5, 5),
                    blurRadius: 10,
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(328.5, 524.0, 30.5, 42.0),
            size: Size(375.0, 812.0),
            pinRight: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                  size: Size(30.5, 42.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                        size: Size(30.5, 42.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child: Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                              size: Size(30.5, 42.0),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child: Stack(
                                children: <Widget>[
                                  Pinned.fromSize(
                                    bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                                    size: Size(30.5, 42.0),
                                    pinLeft: true,
                                    pinRight: true,
                                    pinTop: true,
                                    pinBottom: true,
                                    child: SvgPicture.string(
                                      _svg_h180ld,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Pinned.fromSize(
                                    bounds: Rect.fromLTWH(3.0, 3.0, 25.0, 25.0),
                                    size: Size(30.5, 42.0),
                                    pinLeft: true,
                                    pinRight: true,
                                    pinTop: true,
                                    fixedHeight: true,
                                    child: SvgPicture.string(
                                      _svg_v5a3pk,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Pinned.fromSize(
                                    bounds: Rect.fromLTWH(6.0, 6.0, 19.0, 19.0),
                                    size: Size(30.5, 42.0),
                                    pinTop: true,
                                    fixedWidth: true,
                                    fixedHeight: true,
                                    child:
                                        // Adobe XD layer: 'pedal_bike-24px' (group)
                                        Stack(
                                      children: <Widget>[
                                        Pinned.fromSize(
                                          bounds: Rect.fromLTWH(
                                              0.0, 0.0, 19.0, 19.0),
                                          size: Size(19.0, 19.0),
                                          pinLeft: true,
                                          pinRight: true,
                                          pinTop: true,
                                          pinBottom: true,
                                          child: Stack(
                                            children: <Widget>[
                                              Pinned.fromSize(
                                                bounds: Rect.fromLTWH(
                                                    0.0, 0.0, 19.0, 19.0),
                                                size: Size(19.0, 19.0),
                                                pinLeft: true,
                                                pinRight: true,
                                                pinTop: true,
                                                pinBottom: true,
                                                child: Container(
                                                  decoration: BoxDecoration(),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Pinned.fromSize(
                                          bounds: Rect.fromLTWH(
                                              0.0, 2.0, 19.0, 15.0),
                                          size: Size(19.0, 19.0),
                                          pinLeft: true,
                                          pinRight: true,
                                          pinTop: true,
                                          pinBottom: true,
                                          child: Stack(
                                            children: <Widget>[
                                              Pinned.fromSize(
                                                bounds: Rect.fromLTWH(
                                                    0.0, 0.0, 19.0, 15.0),
                                                size: Size(19.0, 15.0),
                                                pinLeft: true,
                                                pinRight: true,
                                                pinTop: true,
                                                pinBottom: true,
                                                child: SvgPicture.string(
                                                  _svg_yky8kt,
                                                  allowDrawingOutsideViewBox:
                                                      true,
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 163.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 146.0),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(50.0),
                        bottomLeft: Radius.circular(50.0),
                      ),
                      color: const Color(0xff656d74),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x26000000),
                          offset: Offset(0, 15),
                          blurRadius: 30,
                        ),
                      ],
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(89.0, 14.0, 198.0, 149.0),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child:
                      // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                      Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(70.0),
                      image: DecorationImage(
                        image: const AssetImage('assets/images/logo.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(95.0, 696.0, 30.5, 42.0),
            size: Size(375.0, 812.0),
            pinBottom: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                  size: Size(30.5, 42.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                        size: Size(30.5, 42.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child: Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                              size: Size(30.5, 42.0),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child: Stack(
                                children: <Widget>[
                                  Pinned.fromSize(
                                    bounds: Rect.fromLTWH(0.0, 0.0, 30.5, 42.0),
                                    size: Size(30.5, 42.0),
                                    pinLeft: true,
                                    pinRight: true,
                                    pinTop: true,
                                    pinBottom: true,
                                    child: SvgPicture.string(
                                      _svg_h180ld,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Pinned.fromSize(
                                    bounds: Rect.fromLTWH(3.0, 3.0, 25.0, 25.0),
                                    size: Size(30.5, 42.0),
                                    pinLeft: true,
                                    pinRight: true,
                                    pinTop: true,
                                    fixedHeight: true,
                                    child: SvgPicture.string(
                                      _svg_v5a3pk,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Pinned.fromSize(
                                    bounds: Rect.fromLTWH(6.0, 6.0, 19.0, 19.0),
                                    size: Size(30.5, 42.0),
                                    pinTop: true,
                                    fixedWidth: true,
                                    fixedHeight: true,
                                    child:
                                        // Adobe XD layer: 'pedal_bike-24px' (group)
                                        Stack(
                                      children: <Widget>[
                                        Pinned.fromSize(
                                          bounds: Rect.fromLTWH(
                                              0.0, 0.0, 19.0, 19.0),
                                          size: Size(19.0, 19.0),
                                          pinLeft: true,
                                          pinRight: true,
                                          pinTop: true,
                                          pinBottom: true,
                                          child: Stack(
                                            children: <Widget>[
                                              Pinned.fromSize(
                                                bounds: Rect.fromLTWH(
                                                    0.0, 0.0, 19.0, 19.0),
                                                size: Size(19.0, 19.0),
                                                pinLeft: true,
                                                pinRight: true,
                                                pinTop: true,
                                                pinBottom: true,
                                                child: Container(
                                                  decoration: BoxDecoration(),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Pinned.fromSize(
                                          bounds: Rect.fromLTWH(
                                              0.0, 2.0, 19.0, 15.0),
                                          size: Size(19.0, 19.0),
                                          pinLeft: true,
                                          pinRight: true,
                                          pinTop: true,
                                          pinBottom: true,
                                          child: Stack(
                                            children: <Widget>[
                                              Pinned.fromSize(
                                                bounds: Rect.fromLTWH(
                                                    0.0, 0.0, 19.0, 15.0),
                                                size: Size(19.0, 15.0),
                                                pinLeft: true,
                                                pinRight: true,
                                                pinTop: true,
                                                pinBottom: true,
                                                child: SvgPicture.string(
                                                  _svg_yky8kt,
                                                  allowDrawingOutsideViewBox:
                                                      true,
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(293.0, 649.0, 41.0, 41.0),
            size: Size(375.0, 812.0),
            pinRight: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 41.0, 41.0),
                  size: Size(41.0, 41.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Transform.rotate(
                    angle: 0.0,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                        color: const Color(0xff656d74),
                        border: Border.all(
                            width: 1.0, color: const Color(0xff707070)),
                      ),
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(7.0, 7.0, 27.0, 27.0),
                  size: Size(41.0, 41.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child:
                      // Adobe XD layer: '3148893-200' (shape)
                      Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: const AssetImage('assets/images/locate.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(88.0, 501.0, 55.0, 55.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: Transform.rotate(
              angle: 0.6109,
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 55.0, 55.0),
                    size: Size(55.0, 55.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                        color: const Color(0x33bfa780),
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(13.0, 13.0, 28.0, 28.0),
                    size: Size(55.0, 55.0),
                    fixedWidth: true,
                    fixedHeight: true,
                    child: Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 28.0, 28.0),
                          size: Size(28.0, 28.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.elliptical(9999.0, 9999.0)),
                              color: const Color(0x59b9aa84),
                            ),
                          ),
                        ),
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(5.0, 5.0, 18.0, 18.0),
                          size: Size(28.0, 28.0),
                          fixedWidth: true,
                          fixedHeight: true,
                          child: Stack(
                            children: <Widget>[
                              Pinned.fromSize(
                                bounds: Rect.fromLTWH(0.0, 0.0, 18.0, 18.0),
                                size: Size(18.0, 18.0),
                                pinLeft: true,
                                pinRight: true,
                                pinTop: true,
                                pinBottom: true,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(9999.0, 9999.0)),
                                    color: const Color(0xffffffff),
                                    border: Border.all(
                                        width: 3.0,
                                        color: const Color(0xffbfa780)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(19.9, 39.7, 9.4, 9.4),
                    size: Size(55.0, 55.0),
                    pinBottom: true,
                    fixedWidth: true,
                    fixedHeight: true,
                    child: SvgPicture.string(
                      _svg_ayh0sc,
                      allowDrawingOutsideViewBox: true,
                      fit: BoxFit.fill,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 730.0, 375.0, 122.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinBottom: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 122.0),
                  size: Size(375.0, 122.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child:
                      // Adobe XD layer: 'Menu Bottom' (shape)
                      Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        topRight: Radius.circular(25.0),
                      ),
                      color: const Color(0xffffffff),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x14656d74),
                          offset: Offset(0, -5),
                          blurRadius: 20,
                        ),
                      ],
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(42.0, 20.0, 42.0, 42.0),
                  size: Size(375.0, 122.0),
                  pinLeft: true,
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(10.2, 0.0, 21.6, 21.6),
                        size: Size(42.0, 42.0),
                        pinTop: true,
                        fixedWidth: true,
                        fixedHeight: true,
                        child: Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 0.0, 21.6, 21.6),
                              size: Size(21.6, 21.6),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child: SvgPicture.string(
                                _svg_4kv6rv,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(5.6, 5.6, 10.3, 10.3),
                              size: Size(21.6, 21.6),
                              fixedWidth: true,
                              fixedHeight: true,
                              child: SvgPicture.string(
                                _svg_j17c66,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 27.0, 42.0, 15.0),
                        size: Size(42.0, 42.0),
                        pinLeft: true,
                        pinRight: true,
                        pinBottom: true,
                        fixedHeight: true,
                        child: Text(
                          'Search',
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 12,
                            color: const Color(0xff656d74),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(123.0, 16.1, 45.0, 45.9),
                  size: Size(375.0, 122.0),
                  pinTop: true,
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 30.9, 45.0, 15.0),
                        size: Size(45.0, 45.9),
                        pinLeft: true,
                        pinRight: true,
                        pinBottom: true,
                        fixedHeight: true,
                        child: Text(
                          'History',
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 12,
                            color: const Color(0xffbabdbf),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(8.3, 0.0, 28.0, 28.0),
                        size: Size(45.0, 45.9),
                        pinTop: true,
                        fixedWidth: true,
                        fixedHeight: true,
                        child:
                            // Adobe XD layer: 'history-24px' (group)
                            Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 0.0, 28.0, 28.0),
                              size: Size(28.0, 28.0),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child: SvgPicture.string(
                                _svg_tytbin,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(1.5, 3.0, 25.0, 22.0),
                              size: Size(28.0, 28.0),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child: SvgPicture.string(
                                _svg_rm7ox6,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(292.0, 20.0, 42.0, 42.0),
                  size: Size(375.0, 122.0),
                  pinRight: true,
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(11.5, 0.0, 19.1, 21.6),
                        size: Size(42.0, 42.0),
                        pinTop: true,
                        fixedWidth: true,
                        fixedHeight: true,
                        child: Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(4.4, 0.0, 9.9, 9.9),
                              size: Size(19.1, 21.6),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              fixedHeight: true,
                              child: SvgPicture.string(
                                _svg_5d1p4k,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 11.1, 19.1, 10.5),
                              size: Size(19.1, 21.6),
                              pinLeft: true,
                              pinRight: true,
                              pinBottom: true,
                              fixedHeight: true,
                              child: SvgPicture.string(
                                _svg_r7yor,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 27.0, 42.0, 15.0),
                        size: Size(42.0, 42.0),
                        pinLeft: true,
                        pinRight: true,
                        pinBottom: true,
                        fixedHeight: true,
                        child: Text(
                          'Profile',
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 12,
                            color: const Color(0xffbabdbf),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(199.0, 20.0, 62.0, 42.0),
                  size: Size(375.0, 122.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 29.0, 62.0, 13.0),
                        size: Size(62.0, 42.0),
                        pinLeft: true,
                        pinRight: true,
                        pinBottom: true,
                        fixedHeight: true,
                        child: Text(
                          'Help Centre',
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 10,
                            color: const Color(0xffbabdbf),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(17.0, 0.0, 28.0, 28.0),
                        size: Size(62.0, 42.0),
                        pinTop: true,
                        fixedWidth: true,
                        fixedHeight: true,
                        child:
                            // Adobe XD layer: 'feedback-24px (1)' (group)
                            Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 0.0, 28.0, 28.0),
                              size: Size(28.0, 28.0),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child: SvgPicture.string(
                                _svg_u271hv,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(2.0, 0.0, 24.0, 24.0),
                              size: Size(28.0, 28.0),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child: SvgPicture.string(
                                _svg_u0femm,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(116.0, 201.0, 145.0, 102.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 61.0, 145.0, 41.0),
                  size: Size(145.0, 102.0),
                  pinLeft: true,
                  pinRight: true,
                  pinBottom: true,
                  fixedHeight: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 145.0, 41.0),
                        size: Size(145.0, 41.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(21.0),
                            color: const Color(0xffbaa47f),
                            border: Border.all(
                                width: 1.0, color: const Color(0x00707070)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(27.0, 0.0, 89.0, 91.4),
                  size: Size(145.0, 102.0),
                  pinTop: true,
                  pinBottom: true,
                  fixedWidth: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(37.0, 73.6, 52.0, 15.0),
                        size: Size(89.0, 91.4),
                        pinRight: true,
                        pinBottom: true,
                        fixedWidth: true,
                        fixedHeight: true,
                        child: Text(
                          'End Trip',
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 12,
                            color: const Color(0xfffafdff),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 58.0, 91.4),
                        size: Size(89.0, 91.4),
                        pinLeft: true,
                        pinTop: true,
                        pinBottom: true,
                        fixedWidth: true,
                        child:
                            // Adobe XD layer: 'assistant_photo-24p…' (group)
                            Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(34.0, 0.0, 24.0, 24.0),
                              size: Size(58.0, 91.4),
                              pinRight: true,
                              pinTop: true,
                              fixedWidth: true,
                              fixedHeight: true,
                              child: SvgPicture.string(
                                _svg_eterkn,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 71.0, 18.0, 20.4),
                              size: Size(58.0, 91.4),
                              pinLeft: true,
                              pinBottom: true,
                              fixedWidth: true,
                              fixedHeight: true,
                              child: SvgPicture.string(
                                _svg_putf1p,
                                allowDrawingOutsideViewBox: true,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(114.0, 201.0, 145.0, 46.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 46.0, 46.0),
                  size: Size(145.0, 46.0),
                  pinLeft: true,
                  pinTop: true,
                  pinBottom: true,
                  fixedWidth: true,
                  child:
                      // Adobe XD layer: 'timer-24px' (group)
                      Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 46.0, 46.0),
                        size: Size(46.0, 46.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child: SvgPicture.string(
                          _svg_t3pjvt,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.1, 1.0, 40.0, 43.0),
                        size: Size(46.0, 46.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child: SvgPicture.string(
                          _svg_q9zp8b,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(65.0, 11.0, 80.0, 30.0),
                  size: Size(145.0, 46.0),
                  pinRight: true,
                  pinBottom: true,
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Text(
                    'xx : xx',
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 25,
                      color: const Color(0xff656d74),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(-64.0, -36.0, 509.0, 905.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            pinBottom: true,
            child: Container(
              decoration: BoxDecoration(
                color: const Color(0x80ffffff),
                border: Border.all(width: 1.0, color: const Color(0x80707070)),
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(39.0, 317.0, 297.0, 178.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: const Color(0xffffffff),
                border: Border.all(width: 1.0, color: const Color(0xffbfa780)),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x41656d74),
                    offset: Offset(0, 10),
                    blurRadius: 20,
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(78.0, 346.0, 220.0, 22.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child: Text(
              'Scan Rail QR Code',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(71.0, 384.0, 234.0, 32.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child: Text(
              'Please scan the QR code on the rail that you have pushed in the bicycle. ',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 13,
                color: const Color(0xff656d74),
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(80.0, 436.0, 85.0, 34.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child:
                // Adobe XD layer: 'Button CTA' (group)
                Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 85.0, 34.0),
                  size: Size(85.0, 34.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      color: const Color(0xffbfa780),
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(16.9, 10.0, 52.0, 15.0),
                  size: Size(85.0, 34.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Text(
                    'Scan QR',
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 12,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                      height: 2.5,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(211.0, 436.0, 85.0, 34.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child:
                // Adobe XD layer: 'Button CTA' (group)
                Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 85.0, 34.0),
                  size: Size(85.0, 34.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      color: const Color(0xff656d74),
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(6.9, 2.0, 71.0, 30.0),
                  size: Size(85.0, 34.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Text(
                    'Manual input',
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 12,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_h180ld =
    '<svg viewBox="28.0 1043.0 30.5 42.0" ><defs><filter id="shadow"><feDropShadow dx="0" dy="3" stdDeviation="6"/></filter></defs><path transform="translate(28.0, 1043.0)" d="M 15.25 0 C 23.67234230041504 0 30.5 7.051515102386475 30.5 15.74999809265137 C 30.5 24.4484806060791 23.67234230041504 42 15.25 42 C 6.827658176422119 42 0 24.4484806060791 0 15.74999809265137 C 0 7.051515102386475 6.827658176422119 0 15.25 0 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" filter="url(#shadow)"/></svg>';
const String _svg_yky8kt =
    '<svg viewBox="0.0 0.0 19.0 15.0" ><path transform="translate(0.0, -4.0)" d="M 4.607499599456787 9.624999046325684 L 5.953333854675293 5.237500190734863 C 6.182916641235352 4.496875286102295 6.776667594909668 4 7.441666126251221 4 L 9.5 4 L 9.5 5.875 L 7.441666126251221 5.875 L 6.285832405090332 9.624999046325684 L 10.09374809265137 9.624999046325684 L 10.37874794006348 8.6875 L 9.5 8.6875 L 9.5 6.8125 L 13.45833396911621 6.8125 L 13.45833396911621 8.6875 L 12.07291698455811 8.6875 L 10.63208389282227 13.37499904632568 L 11.16249942779541 13.37499904632568 C 11.51083469390869 11.28437423706055 12.99124908447266 9.737500190734863 14.84375095367432 9.634374618530273 C 17.06041526794434 9.503125190734863 19 11.6875 19 14.31249904632568 C 19 16.9375 17.25833320617676 19 15.04166698455811 19 C 13.09416770935059 19 11.51875019073486 17.41562461853027 11.16249942779541 15.24999904632568 L 7.83750057220459 15.24999904632568 C 7.4891676902771 17.34062385559082 6.008750438690186 18.88750076293945 4.15625 18.99062538146973 C 1.939583897590637 19.11249732971191 0 16.9375 0 14.30312347412109 C 0 11.67812442779541 1.74166738986969 9.615623474121094 3.958333253860474 9.615623474121094 L 4.607499599456787 9.615623474121094 Z M 12.80916786193848 15.24999904632568 C 13.12583446502686 16.34687423706055 13.98875045776367 17.125 15.04166698455811 17.125 C 16.37166786193848 17.125 17.41666603088379 15.88750076293945 17.41666603088379 14.31249904632568 C 17.41666603088379 12.73749923706055 16.37166786193848 11.49999904632568 15.04166698455811 11.49999904632568 C 13.98875045776367 11.49999904632568 13.12583446502686 12.27812480926514 12.80916786193848 13.37499904632568 L 15.04166698455811 13.37499904632568 L 15.04166698455811 15.24999904632568 L 12.80916786193848 15.24999904632568 Z M 7.837499141693115 13.37499904632568 L 8.945832252502441 13.37499904632568 L 9.523748397827148 11.49999904632568 L 7.125000476837158 11.49999904632568 C 7.47333288192749 12.04374980926514 7.726666450500488 12.67187404632568 7.837499141693115 13.37499904632568 Z M 3.958333253860474 17.125 C 5.288333892822266 17.125 6.333333492279053 15.88750076293945 6.333333492279053 14.31249904632568 C 6.333333492279053 13.44062423706055 6.008750438690186 12.69062423706055 5.502084255218506 12.17499923706055 L 4.742084980010986 14.65000057220459 L 3.2537522315979 14.01249980926514 L 4.021667957305908 11.50937461853027 C 3.997917413711548 11.50937461853027 3.97416877746582 11.49999904632568 3.95041823387146 11.49999904632568 C 2.620417594909668 11.49999904632568 1.57541811466217 12.73749923706055 1.57541811466217 14.31249904632568 C 1.57541811466217 15.88750076293945 2.628333330154419 17.125 3.958333253860474 17.125 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_v5a3pk =
    '<svg viewBox="31.0 1046.0 25.0 25.0" ><path transform="translate(31.0, 1046.0)" d="M 12.5 0 C 19.40355682373047 0 25 5.59644079208374 25 12.5 C 25 19.40355682373047 19.40355682373047 25 12.5 25 C 5.59644079208374 25 0 19.40355682373047 0 12.5 C 0 5.59644079208374 5.59644079208374 0 12.5 0 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_ayh0sc =
    '<svg viewBox="124.9 319.7 9.4 9.4" ><path transform="matrix(-0.981627, -0.190809, 0.190809, -0.981627, 132.74, 329.03)" d="M 3.999999523162842 0 L 7.999999523162842 8 L 0 8 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_4kv6rv =
    '<svg viewBox="0.0 0.0 21.6 21.6" ><path transform="translate(-150.22, -254.68)" d="M 161.0066375732422 254.6779479980469 C 155.0566711425781 254.6779479980469 150.2160034179688 259.5186462402344 150.2160034179688 265.468017578125 C 150.2160034179688 271.4179992675781 155.0566711425781 276.2586669921875 161.0066375732422 276.2586669921875 C 163.3968658447266 276.2586669921875 165.6614227294922 275.4929809570313 167.5553436279297 274.0450744628906 C 167.9118804931641 273.7724914550781 167.9801635742188 273.2625427246094 167.7070770263672 272.9054565429688 C 167.4350433349609 272.5489196777344 166.9251403808594 272.4817504882813 166.5679931640625 272.7537536621094 C 164.9597015380859 273.9832763671875 163.0365142822266 274.6329956054688 161.0066375732422 274.6329956054688 C 155.9529724121094 274.6329956054688 151.8416595458984 270.5216979980469 151.8416595458984 265.468017578125 C 151.8416595458984 260.4149475097656 155.9529724121094 256.3036499023438 161.0066375732422 256.3036499023438 C 166.0602569580078 256.3036499023438 170.1715850830078 260.4149475097656 170.1715850830078 265.468017578125 C 170.1715850830078 266.869384765625 169.8632354736328 268.2143249511719 169.2563323974609 269.4649963378906 C 169.0601348876953 269.8692321777344 169.2286682128906 270.3553161621094 169.6323852539063 270.5515441894531 C 170.0366516113281 270.7471313476563 170.5221710205078 270.5791625976563 170.7188720703125 270.1748657226563 C 171.4341583251953 268.7014770507813 171.7972259521484 267.1175537109375 171.7972259521484 265.468017578125 C 171.7972259521484 259.5186462402344 166.95654296875 254.6779479980469 161.0066375732422 254.6779479980469 Z" fill="#656d74" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_j17c66 =
    '<svg viewBox="5.6 5.6 10.3 10.3" ><path transform="translate(-154.97, -259.43)" d="M 169.8334503173828 265.1091613769531 L 163.4738159179688 267.4539184570313 C 163.4592437744141 267.4588012695313 163.4472961425781 267.4685668945313 163.4332122802734 267.4750366210938 C 163.4012298583984 267.4886169433594 163.3708801269531 267.5048522949219 163.3405456542969 267.5226745605469 C 163.3123474121094 267.5400695800781 163.2847442626953 267.5579528808594 163.2587280273438 267.5779724121094 C 163.232177734375 267.5986328125 163.2077789306641 267.621337890625 163.1839141845703 267.6451721191406 C 163.1600952148438 267.6690368652344 163.1378784179688 267.6933898925781 163.1172943115234 267.719970703125 C 163.0966796875 267.7459716796875 163.0787963867188 267.7730712890625 163.06201171875 267.8018188476563 C 163.0441589355469 267.83154296875 163.0278778076172 267.8619384765625 163.0137786865234 267.8944091796875 C 163.0078125 267.9085693359375 162.9980773925781 267.92041015625 162.9926452636719 267.9350891113281 L 160.6484222412109 274.2947082519531 C 160.5384521484375 274.5922241210938 160.6121368408203 274.9265747070313 160.8364715576172 275.1503601074219 C 160.991455078125 275.3058471679688 161.1995239257813 275.3887634277344 161.4113922119141 275.3887634277344 C 161.5051879882813 275.3887634277344 161.6005401611328 275.3719482421875 161.6921234130859 275.3383178710938 L 168.0517120361328 272.994140625 C 168.0658111572266 272.98876953125 168.0777282714844 272.97900390625 168.0923309326172 272.9730224609375 C 168.1243438720703 272.9589538574219 168.1546783447266 272.9427185058594 168.1850280761719 272.9248352050781 C 168.2132110595703 272.9074401855469 168.2403106689453 272.8901062011719 168.2662963867188 272.8695068359375 C 168.2933959960938 272.8489685058594 168.3177642822266 272.8267517089844 168.3416137695313 272.8028259277344 C 168.365478515625 272.7785339355469 168.3876953125 272.7541198730469 168.4082794189453 272.7276000976563 C 168.4288787841797 272.7015075683594 168.4461975097656 272.6744079589844 168.4635620117188 272.6462707519531 C 168.4814147949219 272.6159362792969 168.4976959228516 272.5855407714844 168.5117950439453 272.5536499023438 C 168.5177612304688 272.5390014648438 168.5274963378906 272.5270690917969 168.5329132080078 272.5123901367188 L 170.8771209716797 266.1533508300781 C 170.9871215820313 265.8553161621094 170.9134368896484 265.5215148925781 170.6891021728516 265.2971801757813 C 170.4652709960938 265.0722961425781 170.1309356689453 264.9985961914063 169.8334503173828 265.1091613769531 Z M 167.4469604492188 270.7583312988281 L 165.2279510498047 268.5392761230469 L 168.7420654296875 267.2442016601563 L 167.4469604492188 270.7583312988281 Z" fill="#656d74" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_tytbin =
    '<svg viewBox="-285.7 -10.1 28.0 28.0" ><path transform="translate(-285.69, -10.12)" d="M 0 0 L 28 0 L 28 28 L 0 28 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_rm7ox6 =
    '<svg viewBox="-284.2 -7.1 25.0 22.0" ><path transform="translate(-285.19, -10.12)" d="M 15.28571224212646 3 C 9.369049072265625 3 4.571428775787354 7.925556182861328 4.571428775787354 14 L 1 14 L 5.630952835083008 18.75444412231445 L 5.714286327362061 18.9255542755127 L 10.5238094329834 14 L 6.952381134033203 14 C 6.952381134033203 9.270000457763672 10.67857074737549 5.444445133209229 15.28571224212646 5.444445133209229 C 19.89285469055176 5.444445133209229 23.61904525756836 9.270000457763672 23.61904525756836 14 C 23.61904525756836 18.72999954223633 19.89285469055176 22.55555534362793 15.28571224212646 22.55555534362793 C 12.9880952835083 22.55555534362793 10.90476131439209 21.58999633789063 9.40476131439209 20.03777885437012 L 7.714284896850586 21.7733325958252 C 9.654762268066406 23.76555442810059 12.32142925262451 25 15.28571224212646 25 C 21.20237922668457 25 25.99999809265137 20.07444381713867 25.99999809265137 14 C 25.99999809265137 7.925556659698486 21.20237922668457 3 15.28571224212646 3 Z M 14.09523868560791 9.111111640930176 L 14.09523868560791 15.22222232818604 L 19.19047546386719 18.32666778564453 L 20.0476188659668 16.8477783203125 L 15.88095188140869 14.30555629730225 L 15.88095188140869 9.111111640930176 L 14.09523868560791 9.111111640930176 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_5d1p4k =
    '<svg viewBox="70.9 764.2 9.9 9.9" ><path transform="translate(-38.12, 509.57)" d="M 113.9467620849609 264.5289306640625 C 111.2308197021484 264.5289306640625 109.02099609375 262.3191223144531 109.02099609375 259.6037292480469 C 109.02099609375 256.8877868652344 111.2308197021484 254.6779479980469 113.9467620849609 254.6779479980469 C 116.6627044677734 254.6779479980469 118.8719787597656 256.8877868652344 118.8719787597656 259.6037292480469 C 118.8719787597656 262.3191223144531 116.6627044677734 264.5289306640625 113.9467620849609 264.5289306640625 Z M 113.9467620849609 256.3036499023438 C 112.1271057128906 256.3036499023438 110.6466522216797 257.7840576171875 110.6466522216797 259.6037292480469 C 110.6466522216797 261.4228515625 112.1271057128906 262.9032897949219 113.9467620849609 262.9032897949219 C 115.7664184570313 262.9032897949219 117.2463226318359 261.4228515625 117.2463226318359 259.6037292480469 C 117.2463226318359 257.7840576171875 115.7664184570313 256.3036499023438 113.9467620849609 256.3036499023438 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_r7yor =
    '<svg viewBox="66.5 775.3 19.1 10.5" ><path transform="translate(-34.37, 500.22)" d="M 119.0939025878906 285.6096496582031 C 118.6452255249023 285.6096496582031 118.2810745239258 285.2460632324219 118.2810745239258 284.7968139648438 C 118.2810745239258 280.3408508300781 114.6558303833008 276.7156372070313 110.2004318237305 276.7156372070313 C 106.7274780273438 276.7156372070313 103.6490173339844 278.9254760742188 102.5408401489258 282.2147521972656 C 102.4032135009766 282.622802734375 102.469856262207 283.0595397949219 102.7239990234375 283.4133911132813 C 102.9846496582031 283.77587890625 103.3889007568359 283.9839782714844 103.8337936401367 283.9839782714844 L 115.0887985229492 283.9839782714844 C 115.5380249023438 283.9839782714844 115.9016418457031 284.3475952148438 115.9016418457031 284.7968139648438 C 115.9016418457031 285.2460632324219 115.5380249023438 285.6096496582031 115.0887985229492 285.6096496582031 L 103.8337936401367 285.6096496582031 C 102.8735580444336 285.6096496582031 101.9653549194336 285.1430969238281 101.4039688110352 284.3616333007813 C 100.8404006958008 283.5776062011719 100.6935501098633 282.6054382324219 101.0002593994141 281.695556640625 C 102.331672668457 277.7446899414063 106.0289916992188 275.0899963378906 110.2004318237305 275.0899963378906 C 115.5526657104492 275.0899963378906 119.9067306518555 279.4446105957031 119.9067306518555 284.7968139648438 C 119.9067306518555 285.2460632324219 119.5431442260742 285.6096496582031 119.0939025878906 285.6096496582031 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_u271hv =
    '<svg viewBox="0.0 0.0 28.0 28.0" ><path  d="M 28 0 L 0 0 L 0 28 L 28 28 L 28 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_u0femm =
    '<svg viewBox="2.0 0.0 24.0 24.0" ><path transform="translate(0.0, -2.0)" d="M 4.400000095367432 2 L 23.60000038146973 2 C 24.92000198364258 2 25.98800086975098 3.080000162124634 25.98800086975098 4.400000095367432 L 26.00000190734863 26.00000190734863 L 21.20000076293945 21.20000076293945 L 4.400000095367432 21.20000076293945 C 3.079999685287476 21.20000076293945 2 20.1200008392334 2 18.80000114440918 L 2 4.400000095367432 C 2 3.080000162124634 3.079999685287476 2 4.400000095367432 2 Z M 4.400000095367432 18.80000114440918 L 22.19600105285645 18.80000114440918 L 22.90400123596191 19.50800132751465 L 23.60000038146973 20.20400047302246 L 23.60000038146973 4.400000095367432 L 4.400000095367432 4.400000095367432 L 4.400000095367432 18.80000114440918 Z M 15.20000076293945 14.00000095367432 L 12.80000114440918 14.00000095367432 L 12.80000114440918 16.40000152587891 L 15.20000076293945 16.40000152587891 L 15.20000076293945 14.00000095367432 Z M 15.20000076293945 6.800000190734863 L 12.80000114440918 6.800000190734863 L 12.80000114440918 11.60000038146973 L 15.20000076293945 11.60000038146973 L 15.20000076293945 6.800000190734863 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_eterkn =
    '<svg viewBox="0.0 0.0 24.0 24.0" ><path  d="M 0 0 L 24 0 L 24 24 L 0 24 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_putf1p =
    '<svg viewBox="-34.0 71.0 18.0 20.4" ><path transform="translate(-39.0, 67.0)" d="M 13.83199882507324 6.400000095367432 L 13.92799758911133 6.868000507354736 L 14.31199836730957 8.800000190734863 L 20.59999847412109 8.800000190734863 L 20.59999847412109 16.00000190734863 L 16.5679988861084 16.00000190734863 L 16.47200012207031 15.53200054168701 L 16.0880012512207 13.60000038146973 L 7.40000057220459 13.60000038146973 L 7.40000057220459 6.400000095367432 L 13.83200073242188 6.400000095367432 M 15.80000114440918 4 L 5.000000476837158 4 L 5.000000476837158 24.39999961853027 L 7.40000057220459 24.39999961853027 L 7.40000057220459 16.00000190734863 L 14.1200008392334 16.00000190734863 L 14.60000038146973 18.39999961853027 L 23 18.39999961853027 L 23 6.400000095367432 L 16.28000068664551 6.400000095367432 L 15.80000114440918 4 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_t3pjvt =
    '<svg viewBox="0.0 0.0 46.0 46.0" ><path  d="M 0 0 L 46 0 L 46 46 L 0 46 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_q9zp8b =
    '<svg viewBox="0.1 1.0 40.0 43.0" ><path transform="translate(-3.0, 0.0)" d="M 29.73666954040527 1.009999990463257 L 16.40333557128906 1.009999990463257 L 16.40333557128906 5.106236457824707 L 29.73666954040527 5.106236457824707 L 29.73666954040527 1.009999990463257 Z M 20.84778022766113 27.63553237915039 L 25.2922248840332 27.63553237915039 L 25.2922248840332 15.34682750701904 L 20.84778022766113 15.34682750701904 L 20.84778022766113 27.63553237915039 Z M 38.69221496582031 14.07699584960938 L 41.84778213500977 11.16866588592529 C 40.8922233581543 10.12412548065186 39.8477783203125 9.14102840423584 38.71444320678711 8.280819892883301 L 35.55888748168945 11.18914604187012 C 32.11444473266602 8.649479866027832 27.7811164855957 7.133872985839844 23.07000160217285 7.133872985839844 C 12.02555561065674 7.133872985839844 3.069999694824219 15.38779067993164 3.069999694824219 25.56693267822266 C 3.069999694824219 35.74607849121094 12.00333213806152 43.99999237060547 23.07000160217285 43.99999237060547 C 34.13666534423828 43.99999237060547 43.07000732421875 35.74607849121094 43.07000732421875 25.56693267822266 C 43.07000732421875 21.24540710449219 41.42555999755859 17.2515754699707 38.69222640991211 14.07699394226074 Z M 23.06999588012695 39.92424011230469 C 14.46999454498291 39.92424011230469 7.514440536499023 33.51362991333008 7.514440536499023 25.58741569519043 C 7.514440536499023 17.66119766235352 14.46999454498291 11.25059223175049 23.06999588012695 11.25059223175049 C 31.67000007629395 11.25059223175049 38.62554931640625 17.66119956970215 38.62554931640625 25.58741569519043 C 38.62554931640625 33.51362991333008 31.67000007629395 39.92424011230469 23.06999588012695 39.92424011230469 Z" fill="#656d74" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
