import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import './HelpCentre.dart';
import './BottomBar.dart';

class HelpCentreGiveFeedback extends StatefulWidget {
  final String bicycleID;
  final String rentstopID;
  final String returnstopID;

  HelpCentreGiveFeedback(this.bicycleID, this.rentstopID, this.returnstopID);

  @override
  Feedback createState() => Feedback(bicycleID, rentstopID, returnstopID);
}

class Feedback extends State<HelpCentreGiveFeedback> {
  final String bicycleID;
  final String rentstopID;
  final String returnstopID;
  bool _validate = false;

  Feedback(this.bicycleID, this.rentstopID, this.returnstopID);

  String feedbackDetail;
  TextEditingController detailedFeedback = TextEditingController();
  final FirebaseAuth auth = FirebaseAuth.instance;
  CollectionReference issue = FirebaseFirestore.instance.collection('Feedback');

  getCurrentUser() {
    final User user = auth.currentUser;
    final uid = user.uid;

    return uid;
  }

  addFeedback(BuildContext context) {
    issue
        .add({
      'bicycleID': bicycleID,
      'category' : 'Feedback',
      'dateReported' : Timestamp.now(),
      'feedback': feedbackDetail,
      'rentStop': rentstopID,
      'returnStop': returnstopID,
      'status' : 'unread',
      'userID' : getCurrentUser()
    }).catchError((error) => print("Failed to add feedback: $error"));

    AlertDialog alert = AlertDialog(
      title: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Image.asset(
              'assets/images/pedalBike.png',
              height: 60,
              width: 60,
            ),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text('Thank you',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    color: const Color(0xff656d74),
                    letterSpacing: 0.8,
                    fontWeight: FontWeight.w700,
                    height: 1.5,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ],
      ),

      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: Text(
              'We will work to improve\nour application\n',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 15,
                color: const Color(0xff656d74),
                letterSpacing: 0.6,
                fontWeight: FontWeight.w700,
                height: 2,
              ),
              textAlign: TextAlign.center,
            ),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    color: const Color(0xffbfa780),
                  ),

                  padding: const EdgeInsets.fromLTRB(30,10,30,10),
                  child: Text("Done",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 12,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),

                onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => BottomBar(2))),
              ),
            ],
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Pinned.fromSize(
            bounds: Rect.fromLTWH(78.0, 321.0, 220.0, 35.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child: Text(
              'Write a Review',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 25,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(33.0, 401.0, 220.0, 22.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Text(
              'Review',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 0.5, 375.0, 286.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50.0),
                  bottomLeft: Radius.circular(50.0),
                ),
                color: const Color(0xff656d74),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x26000000),
                    offset: Offset(0, 15),
                    blurRadius: 30,
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(31.0, 62.6, 9.6, 16.6),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: SvgPicture.string(
              _svg_28lugu,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(76.0, 59.5, 224.0, 168.0),
            size: Size(375.0, 812.0),
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                  size: Size(224.0, 168.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                        size: Size(224.0, 168.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child:
                            // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                            Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(70.0),
                            image: DecorationImage(
                              image: const AssetImage('assets/images/logo.png'),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(71.0, 113.0, 81.0, 27.0),
                  size: Size(224.0, 168.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Text(
                    'UGoBike',
                    style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 20,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(33.0, 433.0, 318.0, 173.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child:
                // Adobe XD layer: 'Input - Required Fi…' (group)
                Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 318.0, 173.0),
                  size: Size(318.0, 173.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: TextFormField(
                    controller: detailedFeedback,
                    maxLines: 9,

                    decoration: InputDecoration(
                      hintText: "Please leave your feedbacks / opinions here.",
                      errorText: _validate ? 'Please write yout feedback / opinion' : null,
                      hintStyle: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 14,
                        color: const Color(0x80bfa780),
                      ),
                      fillColor: const Color(0xffffffff),
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: const Color(0xffe4e4e4), width: 1.0),
                      ),
                    ),

                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 14,
                      color: const Color(0xffbfa780),
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(133.0, 646.0, 109.0, 34.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () {
                setState(() {
                  if (detailedFeedback.text.isEmpty) {
                    _validate = true;
                  }

                  else {
                    _validate = false;
                    feedbackDetail = detailedFeedback.text;
                    addFeedback(context);
                  }
                });
              },
              child:
              // Adobe XD layer: 'Button CTA' (group)
              Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 109.0, 34.0),
                    size: Size(109.0, 34.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        color: const Color(0xffbfa780),
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(30.0, 0.0, 50.0, 35.0),
                    size: Size(109.0, 34.0),
                    fixedWidth: true,
                    fixedHeight: true,
                    child: Text(
                      'SUBMIT',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 12,
                        color: const Color(0xffffffff),
                        fontWeight: FontWeight.w700,
                        height: 2.5,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(18.0, 52.5, 36.0, 36.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => BottomBar(2))),

              child: Container(
                decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                  color: const Color(0x00ffffff),
                  border: Border.all(width: 1.0, color: const Color(0x00ffffff)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_28lugu =
    '<svg viewBox="31.0 62.6 9.6 16.6" ><path transform="translate(-238.19, -75.36)" d="M 278.7493896484375 137.9653625488281 L 269.19140625 146.2719116210938 L 278.7493896484375 154.5784454345703" fill="none" stroke="#ffffff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
