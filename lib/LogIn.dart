import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:google_sign_in/google_sign_in.dart';
import './core/models/profileModel.dart';
import './EmailRegister.dart';
import './BottomBar.dart';
import './AdminLandingPage.dart';

class LoginPage extends StatefulWidget{
  @override
  LoginPageState createState(){
    return new LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
/*  loginPage({
    Key key,
  }) : super(key: key);*/

  String name;
  String email;
  final FirebaseAuth auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  bool isVisible = false;

  final _formKey = GlobalKey<FormState>();
  TextEditingController loginIDController = TextEditingController();
  TextEditingController loginPWController = TextEditingController();

  bool _validate = false;
  bool emailValid = true;
  TextEditingController resetPWController = TextEditingController();

  Future getCurrentUser() async {
    final User user = auth.currentUser;
    print("User: ${user.displayName ?? "None"}");
    return user;}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 260.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50.0),
                  bottomLeft: Radius.circular(50.0),
                ),
                color: const Color(0xff656d74),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x26000000),
                    offset: Offset(0, 15),
                    blurRadius: 30,
                  ),
                ],
              ),
            ),
          ),

          Pinned.fromSize(
            bounds: Rect.fromLTWH(155.0, 316.0, 68.0, 24.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: Text(
              'Login',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 21,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Form(
              key: _formKey,
              child: Container(
                margin: const EdgeInsets.only(top:315.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 22, bottom:5, left: 25, right: 25),
                      child: TextFormField(
                        controller: loginIDController,
                        decoration: InputDecoration(
                          labelText: "Email Address",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),

                        validator: (value){
                          if(value.isEmpty) {
                            return 'Enter your Email Address';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 7, bottom:10, left: 25, right: 25),
                      child: TextFormField(
                        controller: loginPWController,
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: "Password",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),

                        validator: (value){
                          if(value.isEmpty) {
                            return 'Enter your Password';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8, bottom:0, left: 200, right: 0),
                      child: RichText(
                          text: TextSpan(
                            text: 'Forgot Password?',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 13,
                              color: const Color(0xff656d74),
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                showDialog(
                                    context: context,
                                    child: AlertDialog(
                                      title: Text('Reset Password'),
                                      content: TextField(
                                        controller: resetPWController,
                                        decoration: InputDecoration(
                                          hintText: "Enter Email Address",
                                          errorText: _validate ? 'Email Address Cannot Be Empty' : null,
                                        ),
                                      ),
                                      actions: [
                                        FlatButton(
                                            textColor: const Color(0xff656d74),
                                            onPressed: () async {
                                              setState(() {
                                                resetPWController.text.isEmpty ? _validate = true : _validate = false;
                                                emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(resetPWController.text);
                                              });
                                              if((!_validate)&&(emailValid)){
                                                Navigator.of(context, rootNavigator: true).pop('dialog');

                                                try {
                                                  await sendPasswordResetEmail(resetPWController.text);
                                                  showDialog(
                                                      context: context,
                                                      child: new AlertDialog(
                                                        title: Text('Reset Password'),
                                                        content: Text(
                                                            "A password reset link has been sent to your email."
                                                        ),
                                                        actions: [
                                                          FlatButton(
                                                              textColor: const Color(0xff656d74),
                                                              onPressed: (){
                                                                Navigator.of(context, rootNavigator: true).pop('dialog');
                                                              },
                                                              child: Text('Ok')
                                                          )
                                                        ],
                                                      )
                                                  );
                                                } catch (e) {
                                                  if (e.code == 'user-not-found') {
                                                    showDialog(
                                                        context: context,
                                                        child: new AlertDialog(
                                                          title: const Text("User Not Found"),
                                                          content: const Text("There is no user record corresponding to this email address. Please try again."),

                                                          actions: [
                                                            new FlatButton(
                                                                onPressed: () {
                                                                  Navigator.of(context, rootNavigator: true).pop('dialog');
                                                                },
                                                                child: const Text("Ok")
                                                            )
                                                          ],
                                                        )
                                                    );
                                                  }
                                                  print(e);
                                                }
                                              }
                                              else{
                                                showDialog(
                                                    context: context,
                                                    child: new AlertDialog(
                                                      title: Text('Invalid Email Address'),
                                                      content: Text(
                                                          'Please enter a valid email address.'
                                                      ),
                                                      actions: [
                                                        FlatButton(
                                                            textColor: const Color(0xff656d74),
                                                            onPressed: (){
                                                              Navigator.of(context, rootNavigator: true).pop('dialog');
                                                            },
                                                            child: Text('Ok')
                                                        )
                                                      ],
                                                    )
                                                );
                                              }
                                            },
                                            child: Text('Confirm')
                                        ),
                                        FlatButton(
                                            textColor: const Color(0xff656d74),
                                            onPressed: (){
                                              Navigator.of(context, rootNavigator: true).pop('dialog');
                                            },
                                            child: Text('Cancel')
                                        )
                                      ],
                                    )
                                );
                              },
                          )
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 20, bottom:0, left: 25, right: 25),
                        child: isLoading
                            ? CircularProgressIndicator()
                            : ButtonTheme(
                          minWidth: 335.0,
                          height: 45.0,
                          child:
                          RaisedButton(
                            color: const Color(0xff656d74),
                            onPressed: () async{
                              if (_formKey.currentState.validate()){
                                try {
                                  UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
                                      email: loginIDController.text,
                                      password: loginPWController.text
                                  );

                                  if(userCredential != null){

                                    final User curUser = FirebaseAuth.instance.currentUser;
                                    if(curUser.uid.compareTo("wwA9F0mkKKSqta7XXgkmkd2qCpx2") == 0){
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) => AdminLandingPage()
                                          )
                                      );
                                    }

                                    else{
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) => BottomBar(0)
                                          )
                                      );
                                    }

                                  }

                                } on FirebaseAuthException catch (e) {
                                  if (e.code == 'user-not-found') {
                                    showDialog(
                                        context: context,
                                        child: new AlertDialog(
                                          title: const Text("User not found"),
                                          content: const Text("No user found for this email. Please try again."),

                                          actions: [
                                            new FlatButton(
                                                onPressed: () {
                                                  Navigator.of(context, rootNavigator: true).pop('dialog');
                                                },
                                                child: const Text("Ok")
                                            )
                                          ],
                                        )
                                    );

                                  } else if (e.code == 'wrong-password') {
                                    showDialog(
                                        context: context,
                                        child: new AlertDialog(
                                          title: const Text("Incorrect Password"),
                                          content: const Text("Wrong password provided for this user. Please try again."),

                                          actions: [
                                            new FlatButton(
                                                onPressed: (){
                                                  Navigator.of(context, rootNavigator: true).pop('dialog');
                                                },
                                                child: const Text("Ok")
                                            )
                                          ],
                                        )
                                    );
                                  }
                                }
                              }

                            },
                            child: Text('Sign in',
                                style:
                                TextStyle(fontFamily: 'Montserrat',
                                    fontSize: 15,
                                    color: const Color(0xffffffff),
                                    fontWeight: FontWeight.w700)
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 5, bottom:8, left: 25, right: 25),
                      child: ButtonTheme(
                        height: 45.0,
                        minWidth: 335.0,
                        child:SignInButton(
                          Buttons.Google,
                          text: "Sign in with Google",
                          onPressed:(){
                            signInWithGoogle().then((result) {
                              if(result != null){
                                Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return BottomBar(0);
                                      },
                                    ),
                                );
                              }
                            });
                          },
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 30, bottom:10, left: 25, right: 25),
                      child: RichText(
                        text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Don’t have an account yet? ',
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 14,
                                    color: const Color(0xff656d74),
                                    fontWeight: FontWeight.w500
                                ),
                              ),
                              TextSpan(
                                  text: 'Register Now',
                                  style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 15,
                                      color: const Color(0xffbfa780),
                                      fontWeight: FontWeight.w700
                                  ),
                                  recognizer: TapGestureRecognizer()..onTap = () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => EmailRegistration()))
                              ),
                            ]
                        ),
                      ),
                    )
                  ],
                ),
              )
          ),

          Pinned.fromSize(
            bounds: Rect.fromLTWH(76.0, 35.0, 224.0, 168.0),
            size: Size(375.0, 812.0),
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                  size: Size(224.0, 168.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                        size: Size(224.0, 168.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child:
                        // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(70.0),
                            image: DecorationImage(
                              image: const AssetImage('assets/images/logo.png'),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(75.4, 125.9, 135.0, 50.0),
                  size: Size(224.0, 168.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Text(
                    'UGoBike',
                    style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 20,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  CollectionReference ref = FirebaseFirestore.instance.collection("Profile");

  Future<String> signInWithGoogle() async {
    await Firebase.initializeApp();

    final FirebaseAuth _auth = FirebaseAuth.instance;
    final GoogleSignInAccount googleUser  = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleUser.authentication;

    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      idToken: googleSignInAuthentication.idToken,
      accessToken: googleSignInAuthentication.accessToken,
    );

    final UserCredential authResult = await _auth.signInWithCredential(credential);
    final User user = authResult.user;


    if (user != null) {

      name = user.displayName;
      email = user.email;

      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = _auth.currentUser;
      assert(user.uid == currentUser.uid);

      bool docExists = await checkExist(currentUser.uid);

      if(!docExists){
        ref.doc(user.uid).set(Profile(userEmail: email.toString(),
            userPhone: "",
            userName: name.toString(),
            userGender: "").toJson());
      }

      return '$user';
    }

    return null;
  }

  static Future<bool> checkExist(String docID) async {
    bool exists = false;

      await FirebaseFirestore.instance.doc("Profile/$docID").get().then((doc) {
        if (doc.exists)
          exists = true;
        else
          exists = false;
      });
      return exists;

  }

  Future<void> sendPasswordResetEmail(String email) async {
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    await _firebaseAuth.sendPasswordResetEmail(email: email);
  }

  @override
  void dispose(){
    super.dispose();
    loginIDController.dispose();
    loginPWController.dispose();
    resetPWController.dispose();
  }


} //class



const String _svg_109514 =
    '<svg viewBox="31.0 62.1 9.6 16.6" ><path transform="translate(-238.19, -75.86)" d="M 278.7493896484375 137.9653625488281 L 269.19140625 146.2719116210938 L 278.7493896484375 154.5784454345703" fill="none" stroke="#ffffff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
const String _svg_qher97 =
    '<svg viewBox="30.0 414.8 315.0 1.0" ><path transform="translate(279.0, -74.71)" d="M -248.9999694824219 489.5006713867188 L 65.99998474121094 489.5006713867188" fill="none" stroke="#babdbf" stroke-width="2" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_qajnx4 =
    '<svg viewBox="0.0 0.0 11.7 11.5" ><path transform="translate(-276.9, -244.6)" d="M 288.5969848632813 247.09130859375 C 288.5969848632813 246.2289123535156 288.5216979980469 245.3939208984375 288.3780212402344 244.5999908447266 L 276.8999938964844 244.5999908447266 L 276.8999938964844 249.3156890869141 L 283.4569091796875 249.3156890869141 C 283.1763000488281 250.8419799804688 282.3138732910156 252.1286926269531 281.0271606445313 252.9911041259766 L 281.0271606445313 256.0505065917969 L 284.962646484375 256.0505065917969 C 287.2692260742188 253.9219055175781 288.5969848632813 250.8009185791016 288.5969848632813 247.09130859375 L 288.5969848632813 247.09130859375 L 288.5969848632813 247.09130859375 Z M 288.5969848632813 247.09130859375" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_2uvc4p =
    '<svg viewBox="0.0 0.0 19.0 9.9" ><path transform="translate(-117.8, -310.7)" d="M 128.6893615722656 320.5694274902344 C 131.9814910888672 320.5694274902344 134.73974609375 319.4811401367188 136.7519989013672 317.6194763183594 L 132.8164978027344 314.5601196289063 C 131.7282562255859 315.2924194335938 130.3320007324219 315.7236328125 128.6893615722656 315.7236328125 C 125.5135803222656 315.7236328125 122.8306121826172 313.5813903808594 121.8723907470703 310.6999206542969 L 117.8000259399414 310.6999206542969 L 117.8000259399414 313.8551940917969 C 119.8054122924805 317.8385009765625 123.9256973266602 320.5694274902344 128.6893615722656 320.5694274902344 L 128.6893615722656 320.5694274902344 L 128.6893615722656 320.5694274902344 Z M 128.6893615722656 320.5694274902344" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_a230q0 =
    '<svg viewBox="0.0 0.0 5.4 10.9" ><path transform="translate(-98.9, -197.0)" d="M 104.2659530639648 204.7819366455078 C 104.0195617675781 204.0495910644531 103.8826751708984 203.2693481445313 103.8826751708984 202.4685821533203 C 103.8826751708984 201.6677703857422 104.0195617675781 200.8875274658203 104.2659530639648 200.1551971435547 L 104.2659530639648 196.9999694824219 L 100.1935806274414 196.9999694824219 C 99.36541748046875 198.6426086425781 98.90000152587891 200.5042419433594 98.90000152587891 202.4685821533203 C 98.90000152587891 204.4328765869141 99.37226104736328 206.2945251464844 100.1935806274414 207.9371643066406 L 104.2659530639648 204.7819366455078 L 104.2659530639648 204.7819366455078 L 104.2659530639648 204.7819366455078 Z M 104.2659530639648 204.7819366455078" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_8ktnd3 =
    '<svg viewBox="0.0 0.0 19.0 9.9" ><path transform="translate(-117.8, -98.8)" d="M 128.6893310546875 103.6526107788086 C 130.4756927490234 103.6526107788086 132.0841217041016 104.2685928344727 133.3434600830078 105.4732055664063 L 136.8409271240234 101.9757537841797 C 134.7328643798828 100.0114440917969 131.974609375 98.80001831054688 128.6893310546875 98.80001831054688 C 123.925666809082 98.80001831054688 119.8053817749023 101.5308837890625 117.7999877929688 105.5142669677734 L 121.8723754882813 108.6694869995117 C 122.8305816650391 105.7948837280273 125.5135498046875 103.6526107788086 128.6893310546875 103.6526107788086 L 128.6893310546875 103.6526107788086 L 128.6893310546875 103.6526107788086 Z M 128.6893310546875 103.6526107788086" fill="#000000" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
