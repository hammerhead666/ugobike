import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:UGoBike/ValidateRail.dart';

class Unlock extends StatefulWidget {
  @override
  UnlockState createState() => UnlockState();
}

class UnlockState extends State<Unlock> {
  TextEditingController RailCodeController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String _scanBarcode = 'Unknown';

  Future scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });

    if (_scanBarcode != null) {
      Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => ValidateRail(_scanBarcode)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color(0xfffafdff),
        body: Stack(
          children: <Widget>[
            Pinned.fromSize(
              bounds: Rect.fromLTWH(95.0, 350, 186.0, 35.0),
              size: Size(375.0, 812.0),
              fixedWidth: true,
              fixedHeight: true,
              child: Text(
                'Unlock',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 25,
                  color: const Color(0xff656d74),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Pinned.fromSize(
              bounds: Rect.fromLTWH(0.0, 0.5, 375.0, 286.0),
              size: Size(375.0, 812.0),
              pinLeft: true,
              pinRight: true,
              pinTop: true,
              fixedHeight: true,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50.0),
                    bottomLeft: Radius.circular(50.0),
                  ),
                  color: const Color(0xff656d74),
                  boxShadow: [
                    BoxShadow(
                      color: const Color(0x26000000),
                      offset: Offset(0, 15),
                      blurRadius: 30,
                    ),
                  ],
                ),
              ),
            ),
            Pinned.fromSize(
              bounds: Rect.fromLTWH(76.0, 59.5, 224.0, 168.0),
              size: Size(375.0, 812.0),
              pinTop: true,
              fixedWidth: true,
              fixedHeight: true,
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                    size: Size(224.0, 168.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                          size: Size(224.0, 168.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child:
                              // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                              Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(70.0),
                              image: DecorationImage(
                                image:
                                    const AssetImage('assets/images/logo.png'),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(71.0, 113.0, 81.0, 27.0),
                    size: Size(224.0, 168.0),
                    fixedWidth: true,
                    fixedHeight: true,
                    child: Text(
                      'UGoBike',
                      style: TextStyle(
                        fontFamily: 'Segoe UI',
                        fontSize: 20,
                        color: const Color(0xffffffff),
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Pinned.fromSize(
              bounds: Rect.fromLTWH(27.0, 444.0, 147.0, 125.0),
              size: Size(375.0, 812.0),
              pinLeft: true,
              fixedWidth: true,
              fixedHeight: true,
              child: InkWell(
                onTap: scanQR,
                child: Stack(
                  children: <Widget>[
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(11.0, 0.0, 125.0, 125.0),
                      size: Size(147.0, 125.0),
                      pinLeft: true,
                      pinRight: true,
                      pinTop: true,
                      pinBottom: true,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          //scanQR_ButtonIsAtHere
                          color: const Color(0xffffffff),
                          boxShadow: [
                            BoxShadow(
                              color: const Color(0x14000000),
                              offset: Offset(5, 5),
                              blurRadius: 10,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(0.0, 92.0, 147.0, 23.0),
                      size: Size(147.0, 125.0),
                      pinLeft: true,
                      pinRight: true,
                      pinBottom: true,
                      fixedHeight: true,
                      child: Text(
                        'QR Scan',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 15,
                          color: const Color(0xff656d74),
                          letterSpacing: -0.75,
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(46.0, 19.0, 55.0, 55.0),
                      size: Size(147.0, 125.0),
                      fixedWidth: true,
                      fixedHeight: true,
                      child:
                          // Adobe XD layer: 'report_problem-24px' (group)
                          Stack(
                        children: <Widget>[
                          Pinned.fromSize(
                            bounds: Rect.fromLTWH(0.0, 0.0, 55.0, 55.0),
                            size: Size(55.0, 55.0),
                            pinLeft: true,
                            pinRight: true,
                            pinTop: true,
                            pinBottom: true,
                            child: SvgPicture.string(
                              _svg_1lu5qv,
                              allowDrawingOutsideViewBox: true,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(47.0, 26.0, 53.0, 53.0),
                      size: Size(147.0, 125.0),
                      fixedWidth: true,
                      fixedHeight: true,
                      child: SvgPicture.string(
                        _svg_bacpdf,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
              ),
            ),
//****************************************************************************//
            //start_ManualInsertButton
            Pinned.fromSize(
              bounds: Rect.fromLTWH(202.0, 444.0, 147.0, 125.0),
              size: Size(375.0, 812.0),
              pinRight: true,
              fixedWidth: true,
              fixedHeight: true,
              child: InkWell(
                onTap: () async {
                  return showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
                        title: Text('Insert Rail Number'),
                        content: SingleChildScrollView(
                            child: new Form(
                              key: _formKey,
                              child: new Column(
                                children: <Widget>[
                                  new TextFormField(
                                    controller: RailCodeController,
                                    decoration: InputDecoration(
                                      hintText: 'Rxxx',
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Enter the Rail Number';
                                      }
                                      return null;
                                    },
                                  ),
                                ],
                              ),
                            )
                        ),
                        actions: [
                          RaisedButton(
                            color: const Color(0xffbfa780),
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => ValidateRail(RailCodeController.text)));
                              }
                            },
                            child: Text(
                              'CONFIRM',
                              style: new TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 12,
                                color: const Color(0xffffffff),
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      );
                    }
                  );
                },
                child: Stack(
                  children: <Widget>[
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(11.0, 0.0, 125.0, 125.0),
                      size: Size(147.0, 125.0),
                      pinLeft: true,
                      pinRight: true,
                      pinTop: true,
                      pinBottom: true,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          color: const Color(0xffffffff),
                          boxShadow: [
                            BoxShadow(
                              color: const Color(0x14000000),
                              offset: Offset(5, 5),
                              blurRadius: 10,
                            ),
                          ],
                        ),
                      ),
                    ),
                    /*#1*/ Pinned.fromSize(
                      bounds: Rect.fromLTWH(0.0, 92.0, 147.0, 19.0),
                      size: Size(147.0, 125.0),
                      pinLeft: true,
                      pinRight: true,
                      pinBottom: true,
                      fixedHeight: true,
                      child: Text(
                        'Manual Insert',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 15,
                          color: const Color(0xff656d74),
                          letterSpacing: -0.75,
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    /*#2*/ Pinned.fromSize(
                      bounds: Rect.fromLTWH(46.0, 19.0, 55.0, 55.0),
                      size: Size(147.0, 125.0),
                      fixedWidth: true,
                      fixedHeight: true,
                      child:
                          // Adobe XD layer: 'rate_review-black-1…' (group)
                          Stack(
                        children: <Widget>[
                          Pinned.fromSize(
                            bounds: Rect.fromLTWH(0.0, 0.0, 55.0, 55.0),
                            size: Size(55.0, 55.0),
                            pinLeft: true,
                            pinRight: true,
                            pinTop: true,
                            pinBottom: true,
                            child: SvgPicture.string(
                              _svg_1lu5qv,
                              allowDrawingOutsideViewBox: true,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(82.8, 32.2, 24.0, 24.0),
                      size: Size(147.0, 125.0),
                      fixedWidth: true,
                      fixedHeight: true,
                      child: SvgPicture.string(
                        _svg_90cppq,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(51.0, 36.1, 51.0, 35.7),
                      size: Size(147.0, 125.0),
                      fixedWidth: true,
                      fixedHeight: true,
                      child: SvgPicture.string(
                        _svg_q8m88a,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Pinned.fromSize(
              bounds: Rect.fromLTWH(46.0, 62.1, 9.6, 16.6),
              size: Size(375.0, 812.0),
              pinLeft: true,
              pinTop: true,
              fixedWidth: true,
              fixedHeight: true,
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: SvgPicture.string(
                  _svg_p6cjbc,
                  allowDrawingOutsideViewBox: true,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

const String _svg_1lu5qv =
    '<svg viewBox="0.0 0.0 55.0 55.0" ><path  d="M 0 0 L 55 0 L 55 55 L 0 55 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_bacpdf =
    '<svg viewBox="74.0 470.0 53.0 53.0" ><path transform="translate(72.0, 468.0)" d="M 21.87499618530273 13.92499828338623 L 21.87499618530273 21.87499618530273 L 13.92499828338623 21.87499618530273 L 13.92499828338623 13.92499828338623 L 21.87499618530273 13.92499828338623 M 25.84999656677246 9.950000762939453 L 9.950000762939453 9.950000762939453 L 9.950000762939453 25.84999656677246 L 25.84999656677246 25.84999656677246 L 25.84999656677246 9.950000762939453 L 25.84999656677246 9.950000762939453 Z M 21.87499618530273 35.12499618530273 L 21.87499618530273 43.07499694824219 L 13.92499828338623 43.07499694824219 L 13.92499828338623 35.12499618530273 L 21.87499618530273 35.12499618530273 M 25.84999656677246 31.14999771118164 L 9.950000762939453 31.14999771118164 L 9.950000762939453 47.04999542236328 L 25.84999656677246 47.04999542236328 L 25.84999656677246 31.14999771118164 L 25.84999656677246 31.14999771118164 Z M 43.07499694824219 13.92499828338623 L 43.07499694824219 21.87499618530273 L 35.12499618530273 21.87499618530273 L 35.12499618530273 13.92499828338623 L 43.07499694824219 13.92499828338623 M 47.04999542236328 9.950000762939453 L 31.14999771118164 9.950000762939453 L 31.14999771118164 25.84999656677246 L 47.04999542236328 25.84999656677246 L 47.04999542236328 9.950000762939453 L 47.04999542236328 9.950000762939453 Z M 31.14999771118164 31.14999771118164 L 35.12499618530273 31.14999771118164 L 35.12499618530273 35.12499618530273 L 31.14999771118164 35.12499618530273 L 31.14999771118164 31.14999771118164 Z M 35.12499618530273 35.12499618530273 L 39.09999847412109 35.12499618530273 L 39.09999847412109 39.09999847412109 L 35.12499618530273 39.09999847412109 L 35.12499618530273 35.12499618530273 Z M 39.09999847412109 31.14999771118164 L 43.07499694824219 31.14999771118164 L 43.07499694824219 35.12499618530273 L 39.09999847412109 35.12499618530273 L 39.09999847412109 31.14999771118164 Z M 31.14999771118164 39.09999847412109 L 35.12499618530273 39.09999847412109 L 35.12499618530273 43.07499694824219 L 31.14999771118164 43.07499694824219 L 31.14999771118164 39.09999847412109 Z M 35.12499618530273 43.07499694824219 L 39.09999847412109 43.07499694824219 L 39.09999847412109 47.04999542236328 L 35.12499618530273 47.04999542236328 L 35.12499618530273 43.07499694824219 Z M 39.09999847412109 39.09999847412109 L 43.07499694824219 39.09999847412109 L 43.07499694824219 43.07499694824219 L 39.09999847412109 43.07499694824219 L 39.09999847412109 39.09999847412109 Z M 43.07499694824219 35.12499618530273 L 47.04999542236328 35.12499618530273 L 47.04999542236328 39.09999847412109 L 43.07499694824219 39.09999847412109 L 43.07499694824219 35.12499618530273 Z M 43.07499694824219 43.07499694824219 L 47.04999542236328 43.07499694824219 L 47.04999542236328 47.04999542236328 L 43.07499694824219 47.04999542236328 L 43.07499694824219 43.07499694824219 Z M 55 15.24999713897705 L 49.69999694824219 15.24999713897705 L 49.69999694824219 7.299999713897705 L 41.74999618530273 7.299999713897705 L 41.74999618530273 1.999999642372131 L 55 1.999999642372131 L 55 15.24999713897705 Z M 55 55 L 55 41.74999618530273 L 49.69999694824219 41.74999618530273 L 49.69999694824219 49.69999694824219 L 41.74999618530273 49.69999694824219 L 41.74999618530273 55 L 55 55 Z M 1.999999642372131 55 L 15.24999713897705 55 L 15.24999713897705 49.69999694824219 L 7.299999713897705 49.69999694824219 L 7.299999713897705 41.74999618530273 L 1.999999642372131 41.74999618530273 L 1.999999642372131 55 Z M 1.999999642372131 1.999999642372131 L 1.999999642372131 15.24999713897705 L 7.299999713897705 15.24999713897705 L 7.299999713897705 7.299999713897705 L 15.24999713897705 7.299999713897705 L 15.24999713897705 1.999999642372131 L 1.999999642372131 1.999999642372131 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_p6cjbc =
    '<svg viewBox="41.0 62.1 9.6 16.6" ><path transform="translate(-228.19, -75.86)" d="M 278.7493896484375 137.9653625488281 L 269.19140625 146.2719116210938 L 278.7493896484375 154.5784454345703" fill="none" stroke="#ffffff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
const String _svg_90cppq =
    '<svg viewBox="284.8 476.2 24.0 24.0" ><path transform="translate(284.8, 476.2)" d="M 0 0 L 24 0 L 24 24 L 0 24 L 0 0 Z M 0 0 L 24 0 L 24 24 L 0 24 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_q8m88a =
    '<svg viewBox="253.0 480.1 51.0 35.7" ><path transform="translate(251.0, 475.14)" d="M 47.90000152587891 10.10000038146973 L 47.90000152587891 35.59999847412109 L 7.099999904632568 35.59999847412109 L 7.099999904632568 10.10000038146973 L 47.90000152587891 10.10000038146973 M 47.90000152587891 5 L 7.099999904632568 5 C 4.295000076293945 5 2.025499820709229 7.295000076293945 2.025499820709229 10.10000038146973 L 2 35.59999847412109 C 2 38.40499877929688 4.295000076293945 40.70000076293945 7.099999904632568 40.70000076293945 L 47.90000152587891 40.70000076293945 C 50.70500183105469 40.70000076293945 53 38.40499877929688 53 35.59999847412109 L 53 10.10000038146973 C 53 7.295000076293945 50.70500183105469 5 47.90000152587891 5 Z M 24.94999885559082 12.64999961853027 L 30.04999732971191 12.64999961853027 L 30.04999732971191 17.75 L 24.94999885559082 17.75 L 24.94999885559082 12.64999961853027 Z M 24.94999885559082 20.29999923706055 L 30.04999732971191 20.29999923706055 L 30.04999732971191 25.39999771118164 L 24.94999885559082 25.39999771118164 L 24.94999885559082 20.29999923706055 Z M 17.29999923706055 12.64999961853027 L 22.39999961853027 12.64999961853027 L 22.39999961853027 17.75 L 17.29999923706055 17.75 L 17.29999923706055 12.64999961853027 Z M 17.29999923706055 20.29999923706055 L 22.39999961853027 20.29999923706055 L 22.39999961853027 25.39999771118164 L 17.29999923706055 25.39999771118164 L 17.29999923706055 20.29999923706055 Z M 9.649999618530273 20.29999923706055 L 14.75 20.29999923706055 L 14.75 25.39999771118164 L 9.649999618530273 25.39999771118164 L 9.649999618530273 20.29999923706055 Z M 9.649999618530273 12.64999961853027 L 14.75 12.64999961853027 L 14.75 17.75 L 9.649999618530273 17.75 L 9.649999618530273 12.64999961853027 Z M 17.29999923706055 27.95000076293945 L 37.70000076293945 27.95000076293945 L 37.70000076293945 33.04999923706055 L 17.29999923706055 33.04999923706055 L 17.29999923706055 27.95000076293945 Z M 32.60000228881836 20.29999923706055 L 37.70000076293945 20.29999923706055 L 37.70000076293945 25.39999771118164 L 32.60000228881836 25.39999771118164 L 32.60000228881836 20.29999923706055 Z M 32.60000228881836 12.64999961853027 L 37.70000076293945 12.64999961853027 L 37.70000076293945 17.75 L 32.60000228881836 17.75 L 32.60000228881836 12.64999961853027 Z M 40.25 20.29999923706055 L 45.35000228881836 20.29999923706055 L 45.35000228881836 25.39999771118164 L 40.25 25.39999771118164 L 40.25 20.29999923706055 Z M 40.25 12.64999961853027 L 45.35000228881836 12.64999961853027 L 45.35000228881836 17.75 L 40.25 17.75 L 40.25 12.64999961853027 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
