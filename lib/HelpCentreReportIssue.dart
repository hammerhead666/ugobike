import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './HelpCentre.dart';
import './BottomBar.dart';

class HelpCentreReportIssue extends StatefulWidget {
  final String bicycleID;
  final String rentstopID;
  final String returnstopID;

  HelpCentreReportIssue(this.bicycleID, this.rentstopID, this.returnstopID);

  @override
  Issue createState() => Issue(bicycleID, rentstopID, returnstopID);
}

class Issue extends State<HelpCentreReportIssue> {
  final String bicycleID;
  final String rentstopID;
  final String returnstopID;
  bool _validate = false;

  Issue(this.bicycleID, this.rentstopID, this.returnstopID);

  String problem;
  String issueDetail;
  TextEditingController detailedIssue = TextEditingController();
  final FirebaseAuth auth = FirebaseAuth.instance;
  CollectionReference feedback = FirebaseFirestore.instance.collection('Feedback');

  getCurrentUser() {
    final User user = auth.currentUser;
    final uid = user.uid;

    return uid;
  }

  addIssue(BuildContext context) {
    feedback
        .add({
      'bicycleID': bicycleID,
      'category' : 'Issue',
      'dateReported' : Timestamp.now(),
      'feedback': issueDetail,
      'problem': problem,
      'rentStop': rentstopID,
      'returnStop': returnstopID,
      'status' : 'unread',
      'userID' : getCurrentUser()
    }).catchError((error) => print("Failed to add report: $error"));

    AlertDialog alert = AlertDialog(
      title: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Image.asset(
              'assets/images/pedalBike.png',
              height: 60,
              width: 60,
            ),
          ),


          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text('Thank you',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    color: const Color(0xff656d74),
                    letterSpacing: 0.8,
                    fontWeight: FontWeight.w700,
                    height: 1.5,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ],
      ),

      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: Text(
              'Your report has successfully\nsent to UGoBike\n',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 15,
                color: const Color(0xff656d74),
                letterSpacing: 0.6,
                fontWeight: FontWeight.w700,
                height: 2,
              ),
              textAlign: TextAlign.center,
            ),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    color: const Color(0xffbfa780),
                  ),

                  padding: const EdgeInsets.fromLTRB(30,10,30,10),
                  child: Text("Done",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 12,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),

                onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => BottomBar(2))),
              ),
            ],
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 731.0, 375.0, 122.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinBottom: true,
            fixedHeight: true,
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(78.0, 187.0, 220.0, 35.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child: Text(
              'Report an Issue',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 25,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(29.0, 280.0, 220.0, 22.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Text(
              'Problem',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(29.0, 386.0, 220.0, 22.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Text(
              'Details',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(31.0, 62.6, 9.6, 16.6),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: SvgPicture.string(
              _svg_28lugu,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(320.0, 141.5, 25.0, 25.0),
            size: Size(375.0, 812.0),
            pinRight: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 25.0, 25.0),
                  size: Size(25.0, 25.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius:
                      BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                      color: const Color(0x00656d74),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x00656d74),
                          offset: Offset(0, 4),
                          blurRadius: 6,
                        ),
                      ],
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(7.6, 5.6, 9.9, 13.9),
                  size: Size(25.0, 25.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 9.9, 5.5),
                        size: Size(9.9, 13.9),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        fixedHeight: true,
                        child: SvgPicture.string(
                          _svg_3k258u,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.5, 6.7, 8.8, 7.2),
                        size: Size(9.9, 13.9),
                        pinLeft: true,
                        pinRight: true,
                        pinBottom: true,
                        fixedHeight: true,
                        child: SvgPicture.string(
                          _svg_fc4rsc,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(2.5, 2.5, 4.9, 4.9),
                        size: Size(9.9, 13.9),
                        fixedWidth: true,
                        fixedHeight: true,
                        child: SvgPicture.string(
                          _svg_ozzsaz,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(57.0, 192.5, 25.0, 25.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 25.0, 25.0),
                  size: Size(25.0, 25.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius:
                      BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                      color: const Color(0x00656d74),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x00656d74),
                          offset: Offset(0, 4),
                          blurRadius: 6,
                        ),
                      ],
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(7.6, 5.6, 9.9, 13.9),
                  size: Size(25.0, 25.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 9.9, 5.5),
                        size: Size(9.9, 13.9),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        fixedHeight: true,
                        child: SvgPicture.string(
                          _svg_3k258u,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.5, 6.7, 8.8, 7.2),
                        size: Size(9.9, 13.9),
                        pinLeft: true,
                        pinRight: true,
                        pinBottom: true,
                        fixedHeight: true,
                        child: SvgPicture.string(
                          _svg_fc4rsc,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(2.5, 2.5, 4.9, 4.9),
                        size: Size(9.9, 13.9),
                        fixedWidth: true,
                        fixedHeight: true,
                        child: SvgPicture.string(
                          _svg_ozzsaz,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 277.5, 25.0, 25.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 25.0, 25.0),
                  size: Size(25.0, 25.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius:
                      BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                      color: const Color(0x00656d74),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x00656d74),
                          offset: Offset(0, 4),
                          blurRadius: 6,
                        ),
                      ],
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(7.5, 5.5, 10.2, 14.2),
                  size: Size(25.0, 25.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 10.2, 5.7),
                        size: Size(10.2, 14.2),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        fixedHeight: true,
                        child: SvgPicture.string(
                          _svg_sw9dj2,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.6, 6.9, 9.1, 7.4),
                        size: Size(10.2, 14.2),
                        pinLeft: true,
                        pinRight: true,
                        pinBottom: true,
                        fixedHeight: true,
                        child: SvgPicture.string(
                          _svg_kp6kem,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(2.6, 2.6, 5.0, 5.0),
                        size: Size(10.2, 14.2),
                        fixedWidth: true,
                        fixedHeight: true,
                        child: SvgPicture.string(
                          _svg_1d85qi,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(29.0, 414.0, 318.0, 164.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child:
            // Adobe XD layer: 'Input - Required Fi…' (group)
            Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 318.0, 164.0),
                  size: Size(318.0, 164.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: TextFormField(
                    controller: detailedIssue,
                    maxLines: 9,

                    decoration: InputDecoration(
                      hintText: "Please describe your issue",
                      errorText: _validate ? 'Please describe your issue' : null,
                      hintStyle: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 14,
                        color: const Color(0x80bfa780),
                      ),
                      fillColor: const Color(0xffffffff),
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: const Color(0xffe4e4e4), width: 1.0),
                      ),
                    ),

                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 14,
                      color: const Color(0xffbfa780),
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(127.0, 613.0, 109.0, 34.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              //issueDetail = detailedIssue.text
              onTap: () {
                setState(() {
                  if (detailedIssue.text.isEmpty || problem == null) {
                    _validate = true;
                    AlertDialog alert = AlertDialog(
                      title: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topCenter,
                            child: Image.asset(
                              'assets/images/pedalBike.png',
                              height: 60,
                              width: 60,
                            ),
                          ),


                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                child: Text('Error',
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 20,
                                    color: const Color(0xff656d74),
                                    letterSpacing: 0.8,
                                    fontWeight: FontWeight.w700,
                                    height: 1.5,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),

                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            child: Text(
                              'Please choose a problem\nand describe the issue\n',
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 15,
                                color: const Color(0xff656d74),
                                letterSpacing: 0.6,
                                fontWeight: FontWeight.w700,
                                height: 2,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FlatButton(
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    color: const Color(0xffbfa780),
                                  ),

                                  padding: const EdgeInsets.fromLTRB(30,10,30,10),
                                  child: Text("Try Again",
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 12,
                                      color: const Color(0xffffffff),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ),

                                onPressed: () => Navigator.pop(context),
                              ),
                            ],
                          ),
                        ],
                      ),
                    );

                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return alert;
                      },
                    );
                  }

                  else {
                    _validate = false;
                    issueDetail = detailedIssue.text;
                    addIssue(context);
                  }
                });
              },
              child:
              // Adobe XD layer: 'Button CTA' (group)
              Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 109.0, 34.0),
                    size: Size(109.0, 34.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        color: const Color(0xffbfa780),
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(30.0, 0.0, 50.0, 35.0),
                    size: Size(109.0, 34.0),
                    fixedWidth: true,
                    fixedHeight: true,
                    child: Text(
                      'SUBMIT',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 12,
                        color: const Color(0xffffffff),
                        fontWeight: FontWeight.w700,
                        height: 2.5,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 163.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 146.0),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(50.0),
                        bottomLeft: Radius.circular(50.0),
                      ),
                      color: const Color(0xff656d74),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x26000000),
                          offset: Offset(0, 15),
                          blurRadius: 30,
                        ),
                      ],
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(89.0, 14.0, 198.0, 149.0),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child:
                  // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(70.0),
                      image: DecorationImage(
                        image: const AssetImage('assets/images/logo.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(31.4, 54.6, 9.6, 16.6),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  fixedWidth: true,
                  fixedHeight: true,
                  child: SvgPicture.string(
                    _svg_ef2urf,
                    allowDrawingOutsideViewBox: true,
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(29.0, 308.0, 318.0, 48.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child:
            // Adobe XD layer: 'Dropdown' (group)
            Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 318.0, 48.0),
                  size: Size(318.0, 48.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color(0xffffffff),
                      border: Border.all(
                          width: 1.0, color: const Color(0xffe4e4e4)),
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(15.0, 15.0, 270.0, 18.0),
                  size: Size(318.0, 48.0),
                  pinLeft: true,
                  fixedWidth: true,
                  fixedHeight: true,
                  child: DropdownButton<String>(
                    isExpanded: true,
                    hint: new Text('Select An Issue',
                        style: TextStyle(color: const Color(0x80bfa780))),
                    underline: Container(),
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 14,
                      color: const Color(0xffbfa780),
                    ),

                    items: <String>[
                      'Bicycle Renting Issue',
                      'Payment Issue',
                      'Map & Location Issue',
                      'Application Issue',
                      'Other Issue'].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),

                    onChanged: (String newValue) {
                      setState(() {
                        problem = newValue;
                      });
                    },

                    value: problem,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(20.0, 45.5, 36.0, 36.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => BottomBar(2))),

              child: Container(
                decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                  color: const Color(0x00ffffff),
                  border: Border.all(width: 1.0, color: const Color(0x00ffffff)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_3k258u =
    '<svg viewBox="0.0 0.0 9.9 5.5" ><path transform="translate(-165.35, -386.22)" d="M 174.7324676513672 391.7538452148438 C 174.4225006103516 391.7538452148438 174.1713714599609 391.502685546875 174.1713714599609 391.1927490234375 C 174.1713714599609 389.0705261230469 172.4448089599609 387.3441467285156 170.3227844238281 387.3441467285156 C 168.2005310058594 387.3441467285156 166.4741821289063 389.0705261230469 166.4741821289063 391.1927490234375 C 166.4741821289063 391.502685546875 166.2230377197266 391.7538452148438 165.9131164550781 391.7538452148438 C 165.6031494140625 391.7538452148438 165.3520202636719 391.502685546875 165.3520202636719 391.1927490234375 C 165.3520202636719 388.4517211914063 167.5817718505859 386.2219543457031 170.3227844238281 386.2219543457031 C 173.0637817382813 386.2219543457031 175.2935485839844 388.4517211914063 175.2935485839844 391.1927490234375 C 175.2935485839844 391.502685546875 175.0424041748047 391.7538452148438 174.7324676513672 391.7538452148438 Z" fill="#ffffff" fill-opacity="0.0" stroke="none" stroke-width="1" stroke-opacity="0.0" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_fc4rsc =
    '<svg viewBox="0.5 6.7 8.8 7.2" ><path transform="translate(-167.24, -409.41)" d="M 172.2124938964844 423.3257141113281 C 170.9302978515625 423.3257141113281 169.8258666992188 422.544677734375 169.3985290527344 421.3358764648438 L 167.8207702636719 416.872314453125 C 167.7174987792969 416.5801391601563 167.8708190917969 416.2596130371094 168.1627807617188 416.1564025878906 C 168.4552307128906 416.0529174804688 168.7755126953125 416.2062377929688 168.8787536621094 416.4984130859375 L 170.4565124511719 420.9619750976563 C 170.7801818847656 421.8772277832031 171.5757751464844 422.2035522460938 172.2124938964844 422.2035522460938 C 172.8489685058594 422.2035522460938 173.6445922851563 421.8772277832031 173.96826171875 420.9619750976563 L 175.5458068847656 416.4984130859375 C 175.6490173339844 416.2062377929688 175.9695129394531 416.0529174804688 176.26171875 416.1564025878906 C 176.553955078125 416.2596130371094 176.7070007324219 416.5801391601563 176.6037902832031 416.872314453125 L 175.0260009765625 421.3358764648438 C 174.5989074707031 422.544677734375 173.4944458007813 423.3257141113281 172.2124938964844 423.3257141113281 Z" fill="#ffffff" fill-opacity="0.0" stroke="none" stroke-width="1" stroke-opacity="0.0" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_ozzsaz =
    '<svg viewBox="2.5 2.5 4.9 4.9" ><path transform="translate(-174.03, -394.9)" d="M 179.0021362304688 402.3312683105469 C 177.6460876464844 402.3312683105469 176.5429992675781 401.2279357910156 176.5429992675781 399.8721313476563 C 176.5429992675781 398.51611328125 177.6460876464844 397.4130249023438 179.0021362304688 397.4130249023438 C 180.3579406738281 397.4130249023438 181.4612426757813 398.51611328125 181.4612426757813 399.8721313476563 C 181.4612426757813 401.2279357910156 180.3579406738281 402.3312683105469 179.0021362304688 402.3312683105469 Z M 179.0021362304688 398.5351867675781 C 178.264892578125 398.5351867675781 177.6651611328125 399.1348876953125 177.6651611328125 399.8721313476563 C 177.6651611328125 400.6091918945313 178.264892578125 401.2091064453125 179.0021362304688 401.2091064453125 C 179.7391662597656 401.2091064453125 180.3390808105469 400.6091918945313 180.3390808105469 399.8721313476563 C 180.3390808105469 399.1348876953125 179.7391662597656 398.5351867675781 179.0021362304688 398.5351867675781 Z" fill="#ffffff" fill-opacity="0.0" stroke="none" stroke-width="1" stroke-opacity="0.0" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_28lugu =
    '<svg viewBox="31.0 62.6 9.6 16.6" ><path transform="translate(-238.19, -75.36)" d="M 278.7493896484375 137.9653625488281 L 269.19140625 146.2719116210938 L 278.7493896484375 154.5784454345703" fill="none" stroke="#ffffff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
const String _svg_sw9dj2 =
    '<svg viewBox="0.0 0.0 10.2 5.7" ><path transform="translate(-165.35, -386.22)" d="M 174.95458984375 391.8848571777344 C 174.6372985839844 391.8848571777344 174.3801879882813 391.6277465820313 174.3801879882813 391.3104858398438 C 174.3801879882813 389.1380004882813 172.6127624511719 387.3707275390625 170.4404907226563 387.3707275390625 C 168.2679748535156 387.3707275390625 166.5007629394531 389.1380004882813 166.5007629394531 391.3104858398438 C 166.5007629394531 391.6277465820313 166.24365234375 391.8848571777344 165.9263916015625 391.8848571777344 C 165.6091003417969 391.8848571777344 165.3520202636719 391.6277465820313 165.3520202636719 391.3104858398438 C 165.3520202636719 388.5045471191406 167.6345520019531 386.2219848632813 170.4404907226563 386.2219848632813 C 173.2463989257813 386.2219848632813 175.5289611816406 388.5045471191406 175.5289611816406 391.3104858398438 C 175.5289611816406 391.6277465820313 175.2718811035156 391.8848571777344 174.95458984375 391.8848571777344 Z" fill="#ffffff" fill-opacity="0.0" stroke="none" stroke-width="1" stroke-opacity="0.0" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_kp6kem =
    '<svg viewBox="0.6 6.9 9.1 7.4" ><path transform="translate(-167.23, -409.25)" d="M 172.3172454833984 423.4962463378906 C 171.0046691894531 423.4962463378906 169.8740997314453 422.6967163085938 169.4366607666016 421.4592895507813 L 167.821533203125 416.8900451660156 C 167.7158203125 416.5909423828125 167.8727569580078 416.2628173828125 168.1716461181641 416.1571655273438 C 168.4710083007813 416.0512390136719 168.7988739013672 416.2081909179688 168.9045562744141 416.5072937011719 L 170.5196838378906 421.0765380859375 C 170.8509979248047 422.0134582519531 171.6654510498047 422.3475341796875 172.3172454833984 422.3475341796875 C 172.9687957763672 422.3475341796875 173.7832641601563 422.0134582519531 174.1145782470703 421.0765380859375 L 175.7294921875 416.5072937011719 C 175.8351440429688 416.2081909179688 176.1632232666016 416.0512390136719 176.4623718261719 416.1571655273438 C 176.7615051269531 416.2628173828125 176.9181823730469 416.5909423828125 176.8125152587891 416.8900451660156 L 175.1973724365234 421.4592895507813 C 174.7601623535156 422.6967163085938 173.6295471191406 423.4962463378906 172.3172454833984 423.4962463378906 Z" fill="#ffffff" fill-opacity="0.0" stroke="none" stroke-width="1" stroke-opacity="0.0" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_1d85qi =
    '<svg viewBox="2.6 2.6 5.0 5.0" ><path transform="translate(-173.97, -394.84)" d="M 179.0603637695313 402.44775390625 C 177.6722106933594 402.44775390625 176.5429992675781 401.3182983398438 176.5429992675781 399.9303588867188 C 176.5429992675781 398.542236328125 177.6722106933594 397.4130249023438 179.0603637695313 397.4130249023438 C 180.4482879638672 397.4130249023438 181.5777130126953 398.542236328125 181.5777130126953 399.9303588867188 C 181.5777130126953 401.3182983398438 180.4482879638672 402.44775390625 179.0603637695313 402.44775390625 Z M 179.0603637695313 398.561767578125 C 178.3056640625 398.561767578125 177.6917419433594 399.1756591796875 177.6917419433594 399.9303588867188 C 177.6917419433594 400.6848754882813 178.3056640625 401.2990112304688 179.0603637695313 401.2990112304688 C 179.8148651123047 401.2990112304688 180.4289703369141 400.6848754882813 180.4289703369141 399.9303588867188 C 180.4289703369141 399.1756591796875 179.8148651123047 398.561767578125 179.0603637695313 398.561767578125 Z" fill="#ffffff" fill-opacity="0.0" stroke="none" stroke-width="1" stroke-opacity="0.0" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_ef2urf =
    '<svg viewBox="31.4 54.6 9.6 16.6" ><path transform="translate(-237.75, -83.36)" d="M 278.7493896484375 137.9653625488281 L 269.19140625 146.2719116210938 L 278.7493896484375 154.5784454345703" fill="none" stroke="#ffffff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
