import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:adobe_xd/pinned.dart';
import './Search2.dart';
import './Unlock.dart';

class Search extends StatefulWidget {
  @override
  _StopSearching createState() => _StopSearching();
}

class _StopSearching extends State<Search> {
  GoogleMapController _controller;
  Position position;
  Widget _child;
  BitmapDescriptor currentLocIcon, stopLocIcon;

  @override
  void initState() {
    getCurrentLocation();

    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(25, 25)), 'assets/images/currentlocation.png')
        .then((onValue) {
          currentLocIcon = onValue;
        });

    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(25, 25)), 'assets/images/stoplocation.png')
        .then((onValue) {
          stopLocIcon = onValue;
    });

    super.initState();
  }

  void getCurrentLocation() async {
    Position res = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best
    );

    setState(() {
      position = res;
      _child = mapWidget();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _child
    );
  }

  Set<Marker> _createMarker() {
    MarkerId markerId1 = MarkerId("Current");
    MarkerId markerId2 = MarkerId("S001");
    MarkerId markerId3 = MarkerId("S002");
    MarkerId markerId4 = MarkerId("S003");
    MarkerId markerId5 = MarkerId("S004");
    MarkerId markerId6 = MarkerId("S005");
    MarkerId markerId7 = MarkerId("S006");
    MarkerId markerId8 = MarkerId("S007");
    MarkerId markerId9 = MarkerId("S008");
    MarkerId markerId10 = MarkerId("S009");
    MarkerId markerId11 = MarkerId("S010");
    MarkerId markerId12 = MarkerId("S011");
    MarkerId markerId13 = MarkerId("S012");
    MarkerId markerId14 = MarkerId("S013");
    MarkerId markerId15 = MarkerId("S014");
    MarkerId markerId16 = MarkerId("S015");
    MarkerId markerId17 = MarkerId("S016");
    MarkerId markerId18 = MarkerId("S017");
    MarkerId markerId19 = MarkerId("S018");

    Marker marker1 = Marker(
      markerId: markerId1,
      position: LatLng(position.latitude, position.longitude),
      icon: currentLocIcon,
    );

    Marker marker2 = Marker(
      markerId: markerId2,
      position: LatLng(1.566858, 103.634609),
      icon: stopLocIcon,
      consumeTapEvents: true,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S001", "Arked Angkasa", 1.566858, 103.634609))),
    );

    Marker marker3 = Marker(
      markerId: markerId3,
      position: LatLng(1.564163, 103.635089),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S002", "M01, Kolej Tun Dr Ismail", 1.564163, 103.635089))),
    );

    Marker marker4 = Marker(
      markerId: markerId4,
      position: LatLng(1.562851, 103.631900),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S003", "L01, Kolej Tun Hussein Onn", 1.562851, 103.631900))),
    );

    Marker marker5 = Marker(
      markerId: markerId5,
      position: LatLng(1.561551, 103.632121),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S004", "Arked Cengal", 1.561551, 103.632121))),
    );

    Marker marker6 = Marker(
      markerId: markerId6,
      position: LatLng(1.562327, 103.628742),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S005", "K01, Kolej Tun Razak", 1.562327, 103.628742))),
    );

    Marker marker7 = Marker(
      markerId: markerId7,
      position: LatLng(1.567547, 103.624617),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S006", "Food Court Kolej Datin Seri Endon", 1.567547, 103.624617))),
    );

    Marker marker8 = Marker(
      markerId: markerId8,
      position: LatLng(1.559979, 103.631364),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S007", "H01, Kolej Tun Fatimah", 1.559979, 103.631364))),
    );

    Marker marker9 = Marker(
      markerId: markerId9,
      position: LatLng(1.561338, 103.634795),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S008", "L50, Lecture Hall", 1.561338, 103.634795))),
    );

    Marker marker10 = Marker(
      markerId: markerId10,
      position: LatLng(1.559746, 103.633785),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S009", "Arked Meranti", 1.559746, 103.633785))),
    );

    Marker marker11 = Marker(
      markerId: markerId11,
      position: LatLng(1.557973, 103.632795),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S010", "G01, Kolej Rahman Putra", 1.557973, 103.632795))),
    );

    Marker marker12 = Marker(
      markerId: markerId12,
      position: LatLng(1.564535, 103.637235),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S011", "N28a, School of Computing", 1.564535, 103.637235))),
    );

    Marker marker13 = Marker(
      markerId: markerId13,
      position: LatLng(1.559520, 103.636724),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S012", "Perpustakaan Sultanah Zanariah", 1.559520, 103.636724))),
    );

    Marker marker14 = Marker(
      markerId: markerId14,
      position: LatLng(1.559338, 103.641289),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S013", "P19, Lecture Hall", 1.559338, 103.641289))),
    );

    Marker marker15 = Marker(
      markerId: markerId15,
      position: LatLng(1.554764, 103.644877),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S014", "Dewan Astana, Kolej Tuanku Canselor", 1.554764, 103.644877))),
    );

    Marker marker16 = Marker(
      markerId: markerId16,
      position: LatLng(1.559486, 103.648654),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S015", "Arked Lestari", 1.559486, 103.648654))),
    );

    Marker marker17 = Marker(
      markerId: markerId17,
      position: LatLng(1.556679, 103.656014),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S016", "Stadium Azmna Hashim, UTM", 1.556679, 103.656014))),
    );

    Marker marker18 = Marker(
      markerId: markerId18,
      position: LatLng(1.557619, 103.648285),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S017", "Scholar's Inn", 1.557619, 103.648285))),
    );

    Marker marker19 = Marker(
      markerId: markerId19,
      position: LatLng(1.563413, 103.651825),
      icon: stopLocIcon,
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Search2("S018", "Perpustakaan Raja Zarith Sofiah", 1.563413, 103.651825))),
    );

    Map<MarkerId,Marker> markers = {};

    markers[markerId1]=marker1;
    markers[markerId2]=marker2;
    markers[markerId3]=marker3;
    markers[markerId4]=marker4;
    markers[markerId5]=marker5;
    markers[markerId6]=marker6;
    markers[markerId7]=marker7;
    markers[markerId8]=marker8;
    markers[markerId9]=marker9;
    markers[markerId10]=marker10;
    markers[markerId11]=marker11;
    markers[markerId12]=marker12;
    markers[markerId13]=marker13;
    markers[markerId14]=marker14;
    markers[markerId15]=marker15;
    markers[markerId16]=marker16;
    markers[markerId17]=marker17;
    markers[markerId18]=marker18;
    markers[markerId19]=marker19;

    return Set.of(markers.values);
  }

  void _movetocurrentlocation() async {
    _controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        bearing: 0,
        target: LatLng(position.latitude, position.longitude),
        zoom: 16.5,
      ),
    ));
  }

  Widget mapWidget() {
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Pinned.fromSize(
            bounds: Rect.fromLTWH(-84.0, 27.0, 492.0, 1000.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            pinBottom: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 1000.0, 1000.0),
                  size: Size(1000.0, 1000.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 1000.0, 1000.0),
                        size: Size(1000.0, 1000.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child: Stack(
                          children: <Widget>[
                            Pinned.fromSize(
                              bounds: Rect.fromLTWH(0.0, 0.0, 1000.0, 1000.0),
                              size: Size(1000.0, 1000.0),
                              pinLeft: true,
                              pinRight: true,
                              pinTop: true,
                              pinBottom: true,
                              child:
                              // Adobe XD layer: '0' (shape)
                              Container(
                                //height: MediaQuery.of(context).size.height,
                                //width: MediaQuery.of(context).size.width,
                                child: Stack(
                                  children: [
                                    GoogleMap(
                                        mapType: MapType.normal,
                                        markers: _createMarker(),
                                        initialCameraPosition: CameraPosition(
                                          target: LatLng(position.latitude, position.longitude),
                                          zoom: 16.5,
                                        ),
                                        onMapCreated: (GoogleMapController controller) {
                                          _controller = controller;
                                        }
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 163.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 146.0),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(50.0),
                        bottomLeft: Radius.circular(50.0),
                      ),
                      color: const Color(0xff656d74),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x26000000),
                          offset: Offset(0, 15),
                          blurRadius: 30,
                        ),
                      ],
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(89.0, 14.0, 198.0, 149.0),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child:
                  // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(70.0),
                      image: DecorationImage(
                        image: const AssetImage('assets/images/logo.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(293.0, 710.0, 41.0, 41.0),
            size: Size(375.0, 812.0),
            pinRight: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () {
                _movetocurrentlocation();
            },
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 41.0, 41.0),
                    size: Size(41.0, 41.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Transform.rotate(
                      angle: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                          color: const Color(0xff656d74),
                          border: Border.all(
                              width: 1.0, color: const Color(0xff707070)),
                        ),
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(7.0, 7.0, 27.0, 27.0),
                    size: Size(41.0, 41.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child:
                    // Adobe XD layer: '3148893-200' (shape)
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: const AssetImage('assets/images/locate.png'),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(115.0, 710.0, 145.0, 41.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Unlock())),
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 145.0, 41.0),
                    size: Size(145.0, 41.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 145.0, 41.0),
                          size: Size(145.0, 41.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(21.0),
                              color: const Color(0xff656d74),
                              border: Border.all(
                                  width: 1.0, color: const Color(0x00707070)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(15.0, 9.5, 97.0, 21.0),
                    size: Size(145.0, 41.0),
                    pinLeft: true,
                    fixedWidth: true,
                    fixedHeight: true,
                    child: Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(53.0, 3.0, 44.0, 15.0),
                          size: Size(97.0, 21.0),
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          fixedWidth: true,
                          child: Text(
                            'Unlock',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 12,
                              color: const Color(0xfffafdff),
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 24.0, 21.0),
                          size: Size(97.0, 21.0),
                          pinLeft: true,
                          pinTop: true,
                          pinBottom: true,
                          fixedWidth: true,
                          child:
                          // Adobe XD layer: 'Untitled' (shape)
                          Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: const AssetImage('assets/images/Scan.png'),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    /*return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          GoogleMap(
              mapType: MapType.normal,
              markers: _createMarker(),
              initialCameraPosition: CameraPosition(
                target: LatLng(position.latitude, position.longitude),
                zoom: 16.5,
              ),
              onMapCreated: (GoogleMapController controller) {
                _controller = controller;
              }
          ),
        ],
      ),
    );*/
  }
}