//later put for validate user got successfully rent a bike or not then prompt to timer.dart page
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:ui';
import 'package:firebase_auth/firebase_auth.dart';
import 'Payment.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter/services.dart';

class validateReturn extends StatelessWidget {
  final String railCodeStart;
  final String railCodeEnd;
  final String rentedBikeID;
  final seconds;
  final minutes;
  final hours;
  double totalPrice = 0.0;
  double totalMinRented = 0;

  final RailInfo = FirebaseFirestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;
  CollectionReference history = FirebaseFirestore.instance.collection('RentingHistory');
  bool cont2 = true;

  var endTime;
  var tot;

  validateReturn(this.railCodeEnd, this.rentedBikeID, this.seconds, this.minutes, this.hours, this.railCodeStart);

  Future<void> updateData() async {
    try {
      RailInfo.collection('Rail').doc(railCodeEnd).update({'bicycleID': "$rentedBikeID"});
      RailInfo.collection('Rail').doc(railCodeEnd).update({'status': true});
    } catch (e) {
      print(e.toString());
    }
  }

  TextEditingController RailCodeController2 = TextEditingController();
  final _formKey2 = GlobalKey<FormState>();

  getCurrentUser() {
    final User user = auth.currentUser;
    final uid = user.uid;

    return uid;
  }

  addRentingHistory() {
    String rentStop;
    String returnStop;

    if (railCodeStart == 'R001' || railCodeStart == 'R002' || railCodeStart == 'R003') {
      rentStop = 'S001';
    }

    else if (railCodeStart == 'R004' || railCodeStart == 'R005' || railCodeStart == 'R006') {
      rentStop = 'S002';
    }

    else if (railCodeStart == 'R007' || railCodeStart == 'R008' || railCodeStart == 'R009') {
      rentStop = 'S003';
    }

    else if (railCodeStart == 'R010' || railCodeStart == 'R011' || railCodeStart == 'R012') {
      rentStop = 'S004';
    }

    else if (railCodeStart == 'R013' || railCodeStart == 'R014' || railCodeStart == 'R015') {
      rentStop = 'S005';
    }

    else if (railCodeStart == 'R016' || railCodeStart == 'R017' || railCodeStart == 'R018') {
      rentStop = 'S006';
    }

    else if (railCodeStart == 'R019' || railCodeStart == 'R020' || railCodeStart == 'R021') {
      rentStop = 'S007';
    }

    else if (railCodeStart == 'R022' || railCodeStart == 'R023' || railCodeStart == 'R024') {
      rentStop = 'S008';
    }

    else if (railCodeStart == 'R025' || railCodeStart == 'R026' || railCodeStart == 'R027') {
      rentStop = 'S009';
    }

    else if (railCodeStart == 'R028' || railCodeStart == 'R029' || railCodeStart == 'R030') {
      rentStop = 'S010';
    }

    else if (railCodeStart == 'R031' || railCodeStart == 'R032' || railCodeStart == 'R033') {
      rentStop = 'S011';
    }

    else if (railCodeStart == 'R034' || railCodeStart == 'R035' || railCodeStart == 'R036') {
      rentStop = 'S012';
    }

    else if (railCodeStart == 'R037' || railCodeStart == 'R038' || railCodeStart == 'R039') {
      rentStop = 'S013';
    }

    else if (railCodeStart == 'R040' || railCodeStart == 'R041' || railCodeStart == 'R042') {
      rentStop = 'S014';
    }

    else if (railCodeStart == 'R043' || railCodeStart == 'R044' || railCodeStart == 'R045') {
      rentStop = 'S015';
    }

    else if (railCodeStart == 'R046' || railCodeStart == 'R047' || railCodeStart == 'R048') {
      rentStop = 'S016';
    }

    else if (railCodeStart == 'R049' || railCodeStart == 'R050' || railCodeStart == 'R051') {
      rentStop = 'S017';
    }

    else if (railCodeStart == 'R052' || railCodeStart == 'R053' || railCodeStart == 'R054') {
      rentStop = 'S018';
    }

    if (railCodeEnd == 'R001' || railCodeEnd == 'R002' || railCodeEnd == 'R003') {
      returnStop = 'S001';
    }

    else if (railCodeEnd == 'R004' || railCodeEnd == 'R005' || railCodeEnd == 'R006') {
      returnStop = 'S002';
    }

    else if (railCodeEnd == 'R007' || railCodeEnd == 'R008' || railCodeEnd == 'R009') {
      returnStop = 'S003';
    }

    else if(railCodeEnd == 'R010' || railCodeEnd == 'R011' || railCodeEnd == 'R012') {
      returnStop = 'S004';
    }

    else if (railCodeEnd == 'R013' || railCodeEnd == 'R014' || railCodeEnd == 'R015') {
      returnStop = 'S005';
    }

    else if (railCodeEnd == 'R016' || railCodeEnd == 'R017' || railCodeEnd == 'R018') {
      returnStop = 'S006';
    }

    else if (railCodeEnd == 'R019' || railCodeEnd == 'R020' || railCodeEnd == 'R021') {
      returnStop = 'S007';
    }

    else if (railCodeEnd == 'R022' || railCodeEnd == 'R023' || railCodeEnd == 'R024') {
      returnStop = 'S008';
    }

    else if (railCodeEnd == 'R025' || railCodeEnd == 'R026' || railCodeEnd == 'R027') {
      returnStop = 'S009';
    }

    else if (railCodeEnd == 'R028' || railCodeEnd == 'R029' || railCodeEnd == 'R030') {
      returnStop = 'S010';
    }

    else if (railCodeEnd == 'R031' || railCodeEnd == 'R032' || railCodeEnd == 'R033') {
      returnStop = 'S011';
    }

    else if (railCodeEnd == 'R034' || railCodeEnd == 'R035' || railCodeEnd == 'R036') {
      returnStop = 'S012';
    }

    else if (railCodeEnd == 'R037' || railCodeEnd == 'R038' || railCodeEnd == 'R039') {
      returnStop = 'S013';
    }

    else if (railCodeEnd == 'R040' || railCodeEnd == 'R041' || railCodeEnd == 'R042') {
      returnStop = 'S014';
    }

    else if (railCodeEnd == 'R043' || railCodeEnd == 'R044' || railCodeEnd == 'R045') {
      returnStop = 'S015';
    }

    else if (railCodeEnd == 'R046' || railCodeEnd == 'R047' || railCodeEnd == 'R048') {
      returnStop = 'S016';
    }

    else if (railCodeEnd == 'R049' || railCodeEnd == 'R050' || railCodeEnd == 'R051') {
      returnStop = 'S017';
    }

    else if (railCodeEnd == 'R052' || railCodeEnd == 'R053' || railCodeEnd == 'R054') {
      returnStop = 'S018';
    }

    history
        .add({
      'bicycleID': rentedBikeID,
      'dateRented': Timestamp.now(),
      'rentStop': rentStop,
      'rentingFee': totalPrice,
      'returnStop': returnStop,
      'totalMin' : totalMinRented,
      'userID' : getCurrentUser()
    }).catchError((error) => print("Failed to add feedback: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTextStyle(
        style: Theme.of(context).textTheme.headline6,
        textAlign: TextAlign.center,
        child: Container(
          alignment: FractionalOffset.center,
          color: Colors.white,
          child: StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance .collection('Rail') .where("railID", isEqualTo: railCodeEnd) .snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              List<Widget> children;
              if (!snapshot.hasData) {
                children = <Widget>[CircularProgressIndicator()];
              }

              snapshot.data.docs.map((document) {
                if (document['bicycleID'] == "") {
                  int totalMin = 0;

                  if (seconds >= 30) {
                    totalMin = totalMin + 1;
                  }

                  if (seconds >= 0) {
                    totalMinRented = (seconds/60);
                  }

                  totalMinRented = double.parse((totalMinRented).toStringAsFixed(2));

                  if (hours >= 1) {
                    for (int a = hours; a >= 1; a--) {
                      totalMin = totalMin + 60;
                      totalMinRented = totalMinRented + 60;
                    }
                  }

                  totalMin = totalMin + minutes;
                  totalMinRented = totalMinRented + minutes;

                  if (totalMin <= 30) {
                    totalPrice = 1.0;
                  }

                  else {
                    totalPrice = 1.0;

                    int min = totalMin;

                    min = min - 30;

                    for (int a = min; a >= 30; min = min-30) {
                      totalPrice = totalPrice + 0.5;
                      min = min - 30;
                    }

                    if (min > 0) {
                      totalPrice = totalPrice + 0.5;
                    }
                  }

                  children = <Widget>[
                    Icon(
                      Icons.check_circle_outline,
                      color: Colors.green,
                      size: 100,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Text('Return Bike $rentedBikeID Successfully'),
                    )
                  ];

                  Future.delayed(const Duration(seconds: 5), () {
                    if (cont2) {
                      updateData();
                      addRentingHistory();
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => Payment(totalPrice, hours, minutes, seconds)));
                      cont2 = false;
                    }
                  });
                }

                else if (document['bicycleID'] != "") {
                  children = <Widget>[
                    Icon(
                      Icons.error_outline,
                      color: Colors.red,
                      size: 100,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Text('Fail to Return, Invalid Rail.'),
                    ),
                    new RaisedButton(
                      color: const Color(0xffbfa780),
                      onPressed: () async {
                        String barcodeScanRes2;
                        try {
                          barcodeScanRes2 =
                          await FlutterBarcodeScanner.scanBarcode(
                              "#ff6666", "Cancel", true, ScanMode.QR);
                          print(barcodeScanRes2);
                        } on PlatformException {
                          barcodeScanRes2 =
                          'Failed to get platform version.';
                        }

                        if (barcodeScanRes2 != null) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => validateReturn(barcodeScanRes2, rentedBikeID, seconds, minutes, hours, railCodeStart)));
                        }
                      },
                      child: Text(
                        'Scan QR',
                        style: new TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12,
                          color: const Color(0xffffffff),
                          fontWeight: FontWeight.w700,
                          height: 2.5,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    new RaisedButton(
                      color: const Color(0xff656d74),
                      onPressed: () async {return showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.circular(15.0)),
                            title: Text('Insert Rail Number'),
                            content: SingleChildScrollView(
                              child: new Form(
                                key: _formKey2,
                                child: new Column(
                                  children: <Widget>[
                                    new TextFormField(
                                      controller: RailCodeController2,
                                      decoration: InputDecoration(
                                        hintText: 'Rxxx',
                                      ),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return 'Enter the Rail Number';
                                        }
                                        return null;
                                      },
                                    ),
                                  ],
                                ),
                              )
                            ),
                            actions: [
                              RaisedButton(
                                color: const Color(0xffbfa780),
                                onPressed: () async {
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) =>validateReturn(
                                      RailCodeController2.text, rentedBikeID, hours, minutes, seconds, railCodeStart)));
                                },
                                child: Text(
                                  'CONFIRM',
                                  style: new TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12,
                                    color: const Color(0xffffffff),
                                    fontWeight: FontWeight.w700,
                                    height: 2.5,
                                  ),
                                  textAlign: TextAlign.center,
                                )
                              )
                            ],
                          );
                        });
                      },
                      child: Text(
                        'Manual Insert',
                        style: new TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12,
                          color: const Color(0xffffffff),
                          fontWeight: FontWeight.w700,
                          height: 2.5,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ];
                }
              }).toList();
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: children,
              );
            },
          ),
        ),
      ),
    );
  }
}
