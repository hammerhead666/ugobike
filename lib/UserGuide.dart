import 'package:UGoBike/UserGuideEndingTrip.dart';
import 'package:UGoBike/UserGuidePricing.dart';
import 'package:UGoBike/UserGuideUnlocking.dart';
import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_svg/flutter_svg.dart';
import './UserProfile.dart';
import './BottomBar.dart';

class UserGuide extends StatelessWidget {
  UserGuide({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Pinned.fromSize(
            bounds: Rect.fromLTWH(114.0, 340.0, 147.0, 35.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: Text(
              'User Guide',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 25,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 0.5, 375.0, 286.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50.0),
                  bottomLeft: Radius.circular(50.0),
                ),
                color: const Color(0xff656d74),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x26000000),
                    offset: Offset(0, 15),
                    blurRadius: 30,
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(31.0, 62.6, 9.6, 16.6),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: SvgPicture.string(
              _svg_28lugu,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(76.0, 59.5, 224.0, 168.0),
            size: Size(375.0, 812.0),
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                  size: Size(224.0, 168.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 224.0, 168.0),
                        size: Size(224.0, 168.0),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child:
                            // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                            Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(70.0),
                            image: DecorationImage(
                              image: const AssetImage('assets/images/logo.png'),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(71.0, 113.0, 81.0, 27.0),
                  size: Size(224.0, 168.0),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Text(
                    'UGoBike',
                    style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 20,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(38.0, 410.0, 125.0, 125.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => UserGuideUnlocking())),
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 125.0, 125.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        color: const Color(0xffffffff),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x14000000),
                            offset: Offset(5, 5),
                            blurRadius: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(30.0, 18.0, 65.0, 65.0),
                    size: Size(125.0, 125.0),
                    pinTop: true,
                    fixedWidth: true,
                    fixedHeight: true,
                    child:
                        // Adobe XD layer: 'lock_open-24px' (group)
                        Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 65.0, 65.0),
                          size: Size(65.0, 65.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_5zbxp3,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(4.0, 1.0, 57.0, 62.0),
                          size: Size(65.0, 65.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_szx9hy,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(4.0, 92.0, 118.0, 23.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinBottom: true,
                    fixedHeight: true,
                    child: Text(
                      'Unlocking',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                        color: const Color(0xff656d74),
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(38.0, 580.0, 125.0, 125.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinBottom: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => UserGuidePricing())),

              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 125.0, 125.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        color: const Color(0xffffffff),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x14000000),
                            offset: Offset(5, 5),
                            blurRadius: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(30.0, 18.0, 65.0, 65.0),
                    size: Size(125.0, 125.0),
                    pinTop: true,
                    fixedWidth: true,
                    fixedHeight: true,
                    child:
                        // Adobe XD layer: 'request_quote-24px' (group)
                        Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 65.0, 65.0),
                          size: Size(65.0, 65.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: Container(
                            decoration: BoxDecoration(),
                          ),
                        ),
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(4.0, 2.0, 57.0, 61.0),
                          size: Size(65.0, 65.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_7kaqeq,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(10.0, 92.0, 106.0, 23.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinBottom: true,
                    fixedHeight: true,
                    child: Text(
                      'Pricing',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                        color: const Color(0xff656d74),
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(213.0, 410.0, 125.0, 125.0),
            size: Size(375.0, 812.0),
            pinRight: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => UserGuideEndingTrip())),

              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 125.0, 125.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        color: const Color(0xffffffff),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x14000000),
                            offset: Offset(5, 5),
                            blurRadius: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(6.0, 92.0, 114.0, 23.0),
                    size: Size(125.0, 125.0),
                    pinLeft: true,
                    pinRight: true,
                    pinBottom: true,
                    fixedHeight: true,
                    child: Text(
                      'Ending Trip',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                        color: const Color(0xff656d74),
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(30.0, 18.0, 65.0, 65.0),
                    size: Size(125.0, 125.0),
                    pinTop: true,
                    fixedWidth: true,
                    fixedHeight: true,
                    child:
                        // Adobe XD layer: 'assistant_photo-24p…' (group)
                        Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 65.0, 65.0),
                          size: Size(65.0, 65.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_5zbxp3,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(5.0, 4.0, 56.0, 58.0),
                          size: Size(65.0, 65.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_wsmo7p,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(20.0, 52.5, 36.0, 36.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => BottomBar(3))),

              child: Container(
                decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                  color: const Color(0x00ffffff),
                  border: Border.all(width: 1.0, color: const Color(0x00ffffff)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_28lugu =
    '<svg viewBox="31.0 62.6 9.6 16.6" ><path transform="translate(-238.19, -75.36)" d="M 278.7493896484375 137.9653625488281 L 269.19140625 146.2719116210938 L 278.7493896484375 154.5784454345703" fill="none" stroke="#ffffff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
const String _svg_5zbxp3 =
    '<svg viewBox="0.0 0.0 65.0 65.0" ><path  d="M 0 0 L 65 0 L 65 65 L 0 65 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_szx9hy =
    '<svg viewBox="4.0 1.0 57.0 62.0" ><path  d="M 32.50000381469727 48.23809432983398 C 36.41875457763672 48.23809432983398 39.62500381469727 45.58095550537109 39.62500381469727 42.33333587646484 C 39.62500381469727 39.08571243286133 36.41875457763672 36.42856979370117 32.50000381469727 36.42856979370117 C 28.58124923706055 36.42856979370117 25.375 39.08571243286133 25.375 42.33333587646484 C 25.375 45.58095550537109 28.58124923706055 48.23809432983398 32.50000381469727 48.23809432983398 Z M 53.87500381469727 21.66666793823242 L 50.31250381469727 21.66666793823242 L 50.31250381469727 15.7619047164917 C 50.31250381469727 7.613333225250244 42.33250045776367 1 32.50000381469727 1 C 22.66749954223633 1 14.6875 7.613333225250244 14.6875 15.7619047164917 L 21.45624923706055 15.7619047164917 C 21.45624923706055 10.71333312988281 26.40812492370605 6.609524250030518 32.50000381469727 6.609524250030518 C 38.59187698364258 6.609524250030518 43.54375457763672 10.71333312988281 43.54375457763672 15.7619047164917 L 43.54375457763672 21.66666793823242 L 11.125 21.66666793823242 C 7.20625114440918 21.66666793823242 4 24.32380867004395 4 27.57143020629883 L 4 57.09524154663086 C 4 60.34285736083984 7.20625114440918 63 11.125 63 L 53.87500381469727 63 C 57.79375839233398 63 61.00000381469727 60.34285736083984 61.00000381469727 57.09524154663086 L 61.00000381469727 27.57143020629883 C 61.00000381469727 24.32380867004395 57.79375839233398 21.66666793823242 53.87500381469727 21.66666793823242 Z M 53.87500381469727 57.09524154663086 L 11.125 57.09524154663086 L 11.125 27.57143020629883 L 53.87500381469727 27.57143020629883 L 53.87500381469727 57.09524154663086 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_7kaqeq =
    '<svg viewBox="4.0 2.0 57.0 61.0" ><path  d="M 39.62500381469727 2 L 11.12499904632568 2 C 7.20625114440918 2 4 4.745000839233398 4 8.100000381469727 L 4 56.90000534057617 C 4 60.25500106811523 7.20625114440918 63 11.12499904632568 63 L 53.87500381469727 63 C 57.79375076293945 63 61.00000381469727 60.25500106811523 61.00000381469727 56.90000534057617 L 61.00000381469727 20.30000114440918 L 39.62500381469727 2 Z M 11.12499904632568 56.90000534057617 L 11.12499904632568 8.100000381469727 L 36.06250381469727 8.100000381469727 L 36.06250381469727 20.30000114440918 L 53.87500381469727 20.30000114440918 L 53.87500381469727 56.90000534057617 L 11.12499904632568 56.90000534057617 Z M 28.9375 53.85000610351563 L 36.06250381469727 53.85000610351563 L 36.06250381469727 50.79999923706055 L 39.62500381469727 50.79999923706055 C 41.58438110351563 50.79999923706055 43.18750381469727 49.42750549316406 43.18750381469727 47.75 L 43.18750381469727 38.60000228881836 C 43.18750381469727 36.92250061035156 41.58438110351563 35.54999923706055 39.62500381469727 35.54999923706055 L 28.9375 35.54999923706055 L 28.9375 32.5 L 43.18750381469727 32.5 L 43.18750381469727 26.40000152587891 L 36.06250381469727 26.40000152587891 L 36.06250381469727 23.34999847412109 L 28.9375 23.34999847412109 L 28.9375 26.40000152587891 L 25.375 26.40000152587891 C 23.41562271118164 26.40000152587891 21.8125 27.77249908447266 21.8125 29.45000076293945 L 21.8125 38.60000228881836 C 21.8125 40.27750396728516 23.41562271118164 41.65000152587891 25.375 41.65000152587891 L 36.06250381469727 41.65000152587891 L 36.06250381469727 44.70000457763672 L 21.8125 44.70000457763672 L 21.8125 50.79999923706055 L 28.9375 50.79999923706055 L 28.9375 53.85000610351563 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_wsmo7p =
    '<svg viewBox="5.0 4.0 56.0 58.0" ><path transform="translate(0.0, 0.0)" d="M 32.47732925415039 10.82352924346924 L 32.7760009765625 12.15411758422852 L 33.97066497802734 17.64705848693848 L 53.53332901000977 17.64705848693848 L 53.53332901000977 38.11764526367188 L 40.98933410644531 38.11764526367188 L 40.69066619873047 36.78705596923828 L 39.49600219726563 31.29412078857422 L 12.46666717529297 31.29412078857422 L 12.46666717529297 10.82352924346924 L 32.47733306884766 10.82352924346924 M 38.60000228881836 4 L 4.999998569488525 4 L 4.999998569488525 62 L 12.46666717529297 62 L 12.46666717529297 38.11764526367188 L 33.37333679199219 38.11764526367188 L 34.86666488647461 44.94117736816406 L 60.99999237060547 44.94117736816406 L 60.99999237060547 10.82352924346924 L 40.09333038330078 10.82352924346924 L 38.60000228881836 4 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
