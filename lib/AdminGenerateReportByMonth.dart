import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'dart:async';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './MonthReportPreview.dart';
import './core/models/rentingHistoryModel.dart';
import './AdminGenerateReport.dart';


class AdminGenerateReportByMonthPage extends StatefulWidget{
  @override
  AdminGenerateReportByMonthPageState createState(){
    return AdminGenerateReportByMonthPageState();
  }
}

class AdminGenerateReportByMonthPageState extends State<AdminGenerateReportByMonthPage>{

  String month;
  String sort;
  final pdf = pw.Document(deflate: zlib.encode);
  CollectionReference ref = FirebaseFirestore.instance.collection('RentingHistory');
  final List<RentingHistory> historyList = [];
  double totalProfit = 0;

  @override
  Widget build(BuildContext context){

    return Scaffold(
      backgroundColor: const Color(0xfffafdff),

      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 20),
            height: 150,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  child: IconButton(
                      icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (BuildContext context) => AdminGenerateReportPage(),
                            fullscreenDialog: true,
                          ),
                        );
                      }
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 28),
                  child: Image(
                    height: 200,
                    image: const AssetImage('assets/images/logo.png'),
                  ),
                ),
              ],
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 40),
            child: Text(
              'View Report By Month',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 25,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
                height: 1.2,
              ),
              textAlign: TextAlign.center,
            ),
          ),

          Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: const Color(0xffbfa780),
                ),
              ),
              margin: const EdgeInsets.only(top: 70, left: 40, right: 40),
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: DropdownButton<String>(
                isExpanded: true,
                hint: new Text('Select a Month',
                    style: TextStyle(
                        color: const Color(0x80bfa780),
                        fontSize: 16
                    )
                ),
                underline: Container(),
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 15,
                  color: const Color(0xffbfa780),
                ),

                items: <String>[
                  'December 2020',
                  'January 2021',
                  'February 2021'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),

                onChanged: (String newValue) {
                  setState(() {
                    month = newValue;
                  });
                },

                value: month,
              ),
            ),
          ),

          Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: const Color(0xffbfa780),
                ),
              ),
              margin: const EdgeInsets.only(top: 20, left: 40, right: 40),
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: DropdownButton<String>(
                isExpanded: true,
                hint: new Text('Sort by',
                    style: TextStyle(
                        color: const Color(0x80bfa780),
                        fontSize: 16
                    )
                ),
                underline: Container(),
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 15,
                  color: const Color(0xffbfa780),
                ),

                items: <String>[
                  'Date (Latest to Oldest)',
                  'Date (Oldest to Latest)'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),

                onChanged: (String newValue) {
                  setState(() {
                    sort = newValue;
                  });
                },

                value: sort,
              ),
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 160),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: ButtonTheme(
                minWidth: 135.0,
                height: 40.0,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    //side: BorderSide(color: Colors.red)
                  ),
                  color: const Color(0xffbfa780),
                  onPressed: () async{

                    if(month == null || sort == null){
                      showDialog(
                          context: context,
                          child: new AlertDialog(
                            title: Text('Invalid Choice'),
                            content: Text(
                                "Please select Month and Sort."
                            ),
                            actions: [
                              FlatButton(
                                  textColor: const Color(0xff656d74),
                                  onPressed: (){
                                    Navigator.of(context, rootNavigator: true).pop('dialog');
                                  },
                                  child: Text('Ok')
                              )
                            ],
                          )
                      );
                    }

                    else{
                      await fetchAndSetList();
                      await writeOnPDF();
                      await savePdf();


                      Directory documentDirectory = await getApplicationDocumentsDirectory();

                      String documentPath = documentDirectory.path;

                      String fullPath = "$documentPath/report_${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}.pdf";

                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => MonthReportPreview(fullPath)
                      ));
                    }

                  },
                  child: Text('Generate',
                      style:
                      TextStyle(fontFamily: 'Montserrat',
                          fontSize: 16.5,
                          color: const Color(0xffffffff),
                          fontWeight: FontWeight.w700)
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future <void> fetchAndSetList() async{

    if(month == "December 2020" && sort == "Date (Latest to Oldest)"){
      DateTime start = new DateTime(2020,12,1);
      DateTime end = new DateTime(2020,12,31);

      await ref.where("dateRented", isGreaterThanOrEqualTo: start).where("dateRented", isLessThanOrEqualTo: end).orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach((f) => {
            historyList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(month == "December 2020" && sort == "Date (Oldest to Latest)"){
      DateTime start = new DateTime(2020,12,1);
      DateTime end = new DateTime(2020,12,31);

      await ref.where("dateRented", isGreaterThanOrEqualTo: start).where("dateRented", isLessThanOrEqualTo: end).orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach((f) => {
            historyList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(month == "January 2021" && sort == "Date (Latest to Oldest)"){
      DateTime start = new DateTime(2021,1,1);
      DateTime end = new DateTime(2021,1,31);

      await ref.where("dateRented", isGreaterThanOrEqualTo: start).where("dateRented", isLessThanOrEqualTo: end).orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach((f) => {
            historyList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(month == "January 2021" && sort == "Date (Oldest to Latest)"){
      DateTime start = new DateTime(2021,1,1);
      DateTime end = new DateTime(2021,1,31);

      await ref.where("dateRented", isGreaterThanOrEqualTo: start).where("dateRented", isLessThanOrEqualTo: end).orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach((f) => {
            historyList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(month == "February 2021" && sort == "Date (Latest to Oldest)"){
      DateTime start = new DateTime(2021,2,1);
      DateTime end = new DateTime(2021,2,28);

      await ref.where("dateRented", isGreaterThanOrEqualTo: start).where("dateRented", isLessThanOrEqualTo: end).orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach((f) => {
            historyList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(month == "February 2021" && sort == "Date (Oldest to Latest)"){
      DateTime start = new DateTime(2021,2,1);
      DateTime end = new DateTime(2021,2,28);

      await ref.where("dateRented", isGreaterThanOrEqualTo: start).where("dateRented", isLessThanOrEqualTo: end).orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach((f) => {
            historyList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    print("length: " + historyList.length.toString());

  }


  Future writeOnPDF() async{

    const imageProvider = const AssetImage('assets/images/logo.png');
    final image = await flutterImageProvider(imageProvider);


    for(var index in historyList)
      totalProfit = totalProfit + index.rentingFee;


    if(month == "December 2020"){
      pdf.addPage(
          pw.MultiPage(
            pageFormat: PdfPageFormat.a4,
            margin: pw.EdgeInsets.only(top:32, bottom:32, left:20, right:20),
            orientation: pw.PageOrientation.portrait,
            maxPages: 3,

            build: (pw.Context context){
              return <pw.Widget>  [

                pw.Row(
                    children: <pw.Widget>[
                      pw.Container(
                        height: 120,
                        width: 1000,
                        decoration: pw.BoxDecoration(
                          borderRadius: pw.BorderRadius.circular(10.0),
                          color: const PdfColor.fromInt(0xff656d74),
                        ),
                        child: pw.Image(image),
                      ),

                      pw.Expanded(
                        // flex: 7,
                          child: pw.Container(
                              margin: pw.EdgeInsets.only(left: 35),
                              child: pw.Text(
                                "UGoBike Report December 2020",
                                style: pw.TextStyle(
                                  fontSize: 35,
                                  fontWeight: pw.FontWeight.bold,
                                ),
                              )
                          )
                      )
                    ]
                ),

                pw.Container(
                  margin: pw.EdgeInsets.only(top: 40),
                  child: pw.Header(
                      level: 0,
                      child: pw.Text("")
                  ),
                ),

                pw.Align(
                  alignment: pw.Alignment.topRight,
                  child: pw.Container(
                    margin: pw.EdgeInsets.only(top: 30, bottom: 50),
                    child: pw.Text(
                      "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                      style: pw.TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: pw.TextAlign.right,
                    ),
                  ),
                ),

                pw.Expanded(
                  child: pw.Container(
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Row(
                                children: <pw.Widget>[
                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      width: 122,
                                      height: 40,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Date Rented",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 82,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Rent Stop",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 96,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Return Stop",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 82,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Bicycle ID",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 84,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Total Time",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 95,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Renting Fee",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),
                                ]
                            ),
                            pw.Container(
                                child: pw.Column(
                                    children: <pw.Widget>[
                                      for(var index in historyList)
                                        pw.Row(
                                            children: [
                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 122,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Container(
                                                      child: pw.Text(
                                                        "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                                        style: pw.TextStyle(
                                                          fontSize: 12,
                                                        ),
                                                        textAlign: pw.TextAlign.center,
                                                      )
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 82,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.rentStop.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 96,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.returnStop.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 82,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.bicycleID.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 84,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    "${index.totalMin.toString()}" + " min",
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 95,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                            ]
                                        ),
                                    ]
                                )
                            ),

                          ]
                      )
                  ),
                ),


                pw.Container(
                  // margin: pw.EdgeInsets.only(top: 35),
                    child: pw.Text(
                      "Total Number of Rent(s):   " + "${historyList.length.toString()}",
                      style: pw.TextStyle(
                        fontSize: 20,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

                pw.Container(
                    margin: pw.EdgeInsets.only(top: 15),
                    child: pw.Text(
                      "Total Profit of the Month:  RM ${totalProfit.toStringAsFixed(2)}",
                      style: pw.TextStyle(
                        fontSize: 21,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

              ];
            },

          )
      );
    }

    if(month == "January 2021"){
      pdf.addPage(
          pw.MultiPage(
            pageFormat: PdfPageFormat.a4,
            margin: pw.EdgeInsets.only(top:32, bottom:32, left:20, right:20),
            orientation: pw.PageOrientation.portrait,
            maxPages: 3,

            build: (pw.Context context){
              return <pw.Widget>  [

                pw.Row(
                    children: <pw.Widget>[
                      pw.Container(
                        height: 120,
                        width: 1000,
                        decoration: pw.BoxDecoration(
                          borderRadius: pw.BorderRadius.circular(10.0),
                          color: const PdfColor.fromInt(0xff656d74),
                        ),
                        child: pw.Image(image),
                      ),

                      pw.Expanded(
                        // flex: 7,
                          child: pw.Container(
                              margin: pw.EdgeInsets.only(left: 35),
                              child: pw.Text(
                                "UGoBike Report January 2021",
                                style: pw.TextStyle(
                                  fontSize: 35,
                                  fontWeight: pw.FontWeight.bold,
                                ),
                              )
                          )
                      )
                    ]
                ),

                pw.Container(
                  margin: pw.EdgeInsets.only(top: 40),
                  child: pw.Header(
                      level: 0,
                      child: pw.Text("")
                  ),
                ),

                pw.Align(
                  alignment: pw.Alignment.topRight,
                  child: pw.Container(
                    margin: pw.EdgeInsets.only(top: 30, bottom: 50),
                    child: pw.Text(
                      "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                      style: pw.TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: pw.TextAlign.right,
                    ),
                  ),
                ),

                pw.Expanded(
                  child: pw.Container(
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Row(
                                children: <pw.Widget>[
                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      width: 122,
                                      height: 40,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Date Rented",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 82,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Rent Stop",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 96,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Return Stop",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 82,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Bicycle ID",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 84,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Total Time",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 95,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Renting Fee",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),
                                ]
                            ),
                            pw.Container(
                                child: pw.Column(
                                    children: <pw.Widget>[
                                      for(var index in historyList)
                                        pw.Row(
                                            children: [
                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 122,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Container(
                                                      child: pw.Text(
                                                        "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                                        style: pw.TextStyle(
                                                          fontSize: 12,
                                                        ),
                                                        textAlign: pw.TextAlign.center,
                                                      )
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 82,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.rentStop.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 96,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.returnStop.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 82,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.bicycleID.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 84,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    "${index.totalMin.toString()}" + " min",
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 95,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                            ]
                                        ),
                                    ]
                                )
                            ),

                          ]
                      )
                  ),
                ),


                pw.Container(
                  // margin: pw.EdgeInsets.only(top: 35),
                    child: pw.Text(
                      "Total Number of Rent(s):   " + "${historyList.length.toString()}",
                      style: pw.TextStyle(
                        fontSize: 20,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

                pw.Container(
                    margin: pw.EdgeInsets.only(top: 15),
                    child: pw.Text(
                      "Total Profit of the Month:  RM ${totalProfit.toStringAsFixed(2)}",
                      style: pw.TextStyle(
                        fontSize: 21,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

              ];
            },

          )
      );
    }

    if(month == "February 2021"){
      pdf.addPage(
          pw.MultiPage(
            pageFormat: PdfPageFormat.a4,
            margin: pw.EdgeInsets.only(top:32, bottom:32, left:20, right:20),
            orientation: pw.PageOrientation.portrait,
            maxPages: 3,

            build: (pw.Context context){
              return <pw.Widget>  [

                pw.Row(
                    children: <pw.Widget>[
                      pw.Container(
                        height: 120,
                        width: 1000,
                        decoration: pw.BoxDecoration(
                          borderRadius: pw.BorderRadius.circular(10.0),
                          color: const PdfColor.fromInt(0xff656d74),
                        ),
                        child: pw.Image(image),
                      ),

                      pw.Expanded(
                        // flex: 7,
                          child: pw.Container(
                              margin: pw.EdgeInsets.only(left: 35),
                              child: pw.Text(
                                "UGoBike Report February 2021",
                                style: pw.TextStyle(
                                  fontSize: 35,
                                  fontWeight: pw.FontWeight.bold,
                                ),
                              )
                          )
                      )
                    ]
                ),

                pw.Container(
                  margin: pw.EdgeInsets.only(top: 40),
                  child: pw.Header(
                      level: 0,
                      child: pw.Text("")
                  ),
                ),

                pw.Align(
                  alignment: pw.Alignment.topRight,
                  child: pw.Container(
                    margin: pw.EdgeInsets.only(top: 30, bottom: 50),
                    child: pw.Text(
                      "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                      style: pw.TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: pw.TextAlign.right,
                    ),
                  ),
                ),


                  pw.Container(
                      child: pw.Row(
                          children: <pw.Widget>[
                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                width: 122,
                                height: 40,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Date Rented",
                                  style: pw.TextStyle(
                                    fontSize: 14.5,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),

                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                height: 40,
                                width: 82,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Rent Stop",
                                  style: pw.TextStyle(
                                    fontSize: 14.5,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),

                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                height: 40,
                                width: 96,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Return Stop",
                                  style: pw.TextStyle(
                                    fontSize: 14.5,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),

                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                height: 40,
                                width: 82,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Bicycle ID",
                                  style: pw.TextStyle(
                                    fontSize: 14.5,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),

                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                height: 40,
                                width: 84,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Total Time",
                                  style: pw.TextStyle(
                                    fontSize: 14.5,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),

                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                height: 40,
                                width: 95,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Renting Fee",
                                  style: pw.TextStyle(
                                    fontSize: 14.5,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),
                          ]
                        ),
                      ),

                        if(historyList.length == 0)
                          pw.Container(
                              child: pw.Row(
                                  children: [
                                    pw.Container(
                                        padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                        height: 38,
                                        width: 122,
                                        decoration: pw.BoxDecoration(
                                            border: pw.Border.all(
                                              color: PdfColors.grey800,
                                            )
                                        ),
                                        child: pw.Container(
                                            child: pw.Text(
                                              "NIL",
                                              style: pw.TextStyle(
                                                fontSize: 12,
                                              ),
                                              textAlign: pw.TextAlign.center,
                                            )
                                        )
                                    ),

                                    pw.Container(
                                        padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                        height: 38,
                                        width: 82,
                                        decoration: pw.BoxDecoration(
                                            border: pw.Border.all(
                                              color: PdfColors.grey800,
                                            )
                                        ),
                                        child: pw.Text(
                                          "NIL",
                                          style: pw.TextStyle(
                                            fontSize: 15,
                                          ),
                                          textAlign: pw.TextAlign.center,
                                        )
                                    ),

                                    pw.Container(
                                        padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                        height: 38,
                                        width: 96,
                                        decoration: pw.BoxDecoration(
                                            border: pw.Border.all(
                                              color: PdfColors.grey800,
                                            )
                                        ),
                                        child: pw.Text(
                                          "NIL",
                                          style: pw.TextStyle(
                                            fontSize: 15,
                                          ),
                                          textAlign: pw.TextAlign.center,
                                        )
                                    ),

                                    pw.Container(
                                        padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                        height: 38,
                                        width: 82,
                                        decoration: pw.BoxDecoration(
                                            border: pw.Border.all(
                                              color: PdfColors.grey800,
                                            )
                                        ),
                                        child: pw.Text(
                                          "NIL",
                                          style: pw.TextStyle(
                                            fontSize: 15,
                                          ),
                                          textAlign: pw.TextAlign.center,
                                        )
                                    ),

                                    pw.Container(
                                        padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                        height: 38,
                                        width: 84,
                                        decoration: pw.BoxDecoration(
                                            border: pw.Border.all(
                                              color: PdfColors.grey800,
                                            )
                                        ),
                                        child: pw.Text(
                                          "NIL",
                                          style: pw.TextStyle(
                                            fontSize: 15,
                                          ),
                                          textAlign: pw.TextAlign.center,
                                        )
                                    ),

                                    pw.Container(
                                        padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                        height: 38,
                                        width: 95,
                                        decoration: pw.BoxDecoration(
                                            border: pw.Border.all(
                                              color: PdfColors.grey800,
                                            )
                                        ),
                                        child: pw.Text(
                                          "NIL",
                                          style: pw.TextStyle(
                                            fontSize: 15,
                                          ),
                                          textAlign: pw.TextAlign.center,
                                        )
                                    ),
                                  ]
                              ),
                          ),

                          for(var index in historyList)
                            pw.Container(
                                child: pw.Row(
                                    children: [
                                      pw.Container(
                                          padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                          height: 38,
                                          width: 122,
                                          decoration: pw.BoxDecoration(
                                              border: pw.Border.all(
                                                color: PdfColors.grey800,
                                              )
                                          ),
                                          child: pw.Container(
                                              child: pw.Text(
                                                "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                                style: pw.TextStyle(
                                                  fontSize: 12,
                                                ),
                                                textAlign: pw.TextAlign.center,
                                              )
                                          )
                                      ),

                                      pw.Container(
                                          padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                          height: 38,
                                          width: 82,
                                          decoration: pw.BoxDecoration(
                                              border: pw.Border.all(
                                                color: PdfColors.grey800,
                                              )
                                          ),
                                          child: pw.Text(
                                            index.rentStop.toString(),
                                            style: pw.TextStyle(
                                              fontSize: 15,
                                            ),
                                            textAlign: pw.TextAlign.center,
                                          )
                                      ),

                                      pw.Container(
                                          padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                          height: 38,
                                          width: 96,
                                          decoration: pw.BoxDecoration(
                                              border: pw.Border.all(
                                                color: PdfColors.grey800,
                                              )
                                          ),
                                          child: pw.Text(
                                            index.returnStop.toString(),
                                            style: pw.TextStyle(
                                              fontSize: 15,
                                            ),
                                            textAlign: pw.TextAlign.center,
                                          )
                                      ),

                                      pw.Container(
                                          padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                          height: 38,
                                          width: 82,
                                          decoration: pw.BoxDecoration(
                                              border: pw.Border.all(
                                                color: PdfColors.grey800,
                                              )
                                          ),
                                          child: pw.Text(
                                            index.bicycleID.toString(),
                                            style: pw.TextStyle(
                                              fontSize: 15,
                                            ),
                                            textAlign: pw.TextAlign.center,
                                          )
                                      ),

                                      pw.Container(
                                          padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                          height: 38,
                                          width: 84,
                                          decoration: pw.BoxDecoration(
                                              border: pw.Border.all(
                                                color: PdfColors.grey800,
                                              )
                                          ),
                                          child: pw.Text(
                                            "${index.totalMin.toString()}" + " min",
                                            style: pw.TextStyle(
                                              fontSize: 15,
                                            ),
                                            textAlign: pw.TextAlign.center,
                                          )
                                      ),

                                      pw.Container(
                                          padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                          height: 38,
                                          width: 95,
                                          decoration: pw.BoxDecoration(
                                              border: pw.Border.all(
                                                color: PdfColors.grey800,
                                              )
                                          ),
                                          child: pw.Text(
                                            "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                            style: pw.TextStyle(
                                              fontSize: 15,
                                            ),
                                            textAlign: pw.TextAlign.center,
                                          )
                                      ),

                                    ]
                                ),
                            ),


                pw.Container(
                   margin: pw.EdgeInsets.only(top: 265),
                    child: pw.Text(
                      "Total Number of Rent(s):   " + "${historyList.length.toString()}",
                      style: pw.TextStyle(
                        fontSize: 20,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

                pw.Container(
                    margin: pw.EdgeInsets.only(top: 15),
                    child: pw.Text(
                      "Total Profit of the Month:  RM ${totalProfit.toStringAsFixed(2)}",
                      style: pw.TextStyle(
                        fontSize: 21,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

              ];
            },

          )
      );
    }

  }

  Future savePdf() async{
    Directory documentDirectory = await getApplicationDocumentsDirectory();

    String documentPath = documentDirectory.path;

    print("Path:" + "$documentPath/report_${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}.pdf");

    File file = File("$documentPath/report_${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}.pdf");

    file.writeAsBytesSync(await pdf.save());
  }

}

const String _svg_ef2urf =
    '<svg viewBox="31.4 54.6 9.6 16.6" ><path transform="translate(-237.75, -83.36)" d="M 278.7493896484375 137.9653625488281 L 269.19140625 146.2719116210938 L 278.7493896484375 154.5784454345703" fill="none" stroke="#ffffff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
