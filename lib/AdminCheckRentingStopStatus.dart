import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import './AdminLandingPage.dart';

class AdminCheckRentingStopStatus extends StatefulWidget {
  @override
  StopStatus createState() => StopStatus();
}

class StopStatus extends State<AdminCheckRentingStopStatus>
{
  String stop;
  GoogleMapController _controller;
  BitmapDescriptor stopLocIcon;
  CollectionReference ref = FirebaseFirestore.instance.collection('Rail');
  String bikeInRail = "";
  int railCount = 1;

  @override
  void initState() {
    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(25, 25)), 'assets/images/stoplocation.png')
        .then((onValue) {
      stopLocIcon = onValue;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Pinned.fromSize(
            bounds: Rect.fromLTWH(89.0, 14.0, 198.0, 149.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child:
            // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(70.0),
                image: DecorationImage(
                  image: const AssetImage('assets/images/logo.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 163.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 146.0),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(50.0),
                        bottomLeft: Radius.circular(50.0),
                      ),
                      color: const Color(0xff656d74),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x26000000),
                          offset: Offset(0, 15),
                          blurRadius: 30,
                        ),
                      ],
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(89.0, 14.0, 198.0, 149.0),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child:
                  // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(70.0),
                      image: DecorationImage(
                        image: const AssetImage('assets/images/logo.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(31.4, 54.6, 9.6, 16.6),
                  size: Size(375.0, 163.0),
                  pinLeft: true,
                  fixedWidth: true,
                  fixedHeight: true,
                  child: InkWell(
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => AdminLandingPage())),
                    child: SvgPicture.string(
                      _svg_ef2urf,
                      allowDrawingOutsideViewBox: true,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(68.0, 181.0, 240.0, 30.0),
            size: Size(375.0, 812.0),
            fixedWidth: true,
            fixedHeight: true,
            child: Text(
              'Check Stop Status',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 25,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
                height: 1.2,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(29.0, 232.0, 318.0, 48.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            fixedHeight: true,
            child:
            // Adobe XD layer: 'Dropdown' (group)
            Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 318.0, 48.0),
                  size: Size(318.0, 48.0),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color(0xffffffff),
                      border: Border.all(
                          width: 1.0, color: const Color(0xffe4e4e4)),
                    ),
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(15.0, 15.0, 270.0, 18.0),
                  size: Size(318.0, 48.0),
                  pinLeft: true,
                  fixedWidth: true,
                  fixedHeight: true,
                  child: DropdownButton<String>(
                    isExpanded: true,
                    hint: new Text('Select a Stop',
                        style: TextStyle(color: const Color(0x80bfa780))),
                    underline: Container(),
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 14,
                      color: const Color(0xffbfa780),
                    ),

                    items: <String>[
                      'S001 - Arked Angkasa',
                      'S002 - M01, Kolej Tun Dr Ismail',
                      'S003 - L01, Kolej Tun Hussein Onn',
                      'S004 - Arked Cengal',
                      'S005 - K01, Kolej Tun Razak',
                      'S006 - Food Court Kolej Datin Seri Endon',
                      'S007 - H01, Kolej Tun Fatimah',
                      'S008 - L50, Lecture Hall',
                      'S009 - Arked Meranti',
                      'S010 - G01, Kolej Rahman Putra',
                      'S011 - N28a, School of Computing',
                      'S012 - Perpustakaan Sultan Zanariah',
                      'S013 - P19, Lecture Hall',
                      'S014 - Dewan Astana Kolej Tuanku Canselor',
                      'S015 - Arked Lestari',
                      'S016 - Stadium Azman Hashim, UTM',
                      'S017 - Scholars Inn',
                      'S018 - Perpustakaan Raja Zarith Sofiah'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),

                    onChanged: (String newValue) {
                      setState(() {
                        stop = newValue;
                      });

                      if (stop == 'S001 - Arked Angkasa')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.566858, 103.634609),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S002 - M01, Kolej Tun Dr Ismail')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.564163, 103.635089),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S003 - L01, Kolej Tun Hussein Onn')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.562851, 103.6319),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S004 - Arked Cengal')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.561551, 103.632121),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S005 - K01, Kolej Tun Razak')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.562327, 103.628742),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S006 - Food Court Kolej Datin Seri Endon')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.567547, 103.624617),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S007 - H01, Kolej Tun Fatimah')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.559979, 103.631364),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S008 - L50, Lecture Hall')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.561338, 103.634795),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S009 - Arked Meranti')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.559746, 103.633785),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S010 - G01, Kolej Rahman Putra')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.557973, 103.632795),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S011 - N28a, School of Computing')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.564535, 103.637235),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S012 - Perpustakaan Sultan Zanariah')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.559520, 103.636724),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S013 - P19, Lecture Hall')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.559338, 103.641289),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S014 - Dewan Astana Kolej Tuanku Canselor')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.554764, 103.644877),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S015 - Arked Lestari')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.559486, 103.648654),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S016 - Stadium Azman Hashim, UTM')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.556679, 103.656014),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S017 - Scholars Inn')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.557619, 103.648285),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }

                      if (stop == 'S018 - Perpustakaan Raja Zarith Sofiah')
                      {
                        _controller.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                                target: LatLng(1.563413, 103.651825),
                                zoom: 16.5
                            ),
                          ),
                        );
                      }
                    },

                    value: stop,
                  ),
                ),
              ],
            ),
          ),

          if (stop == 'S001 - Arked Angkasa')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                future: ref.where("stopID", isEqualTo: "S001").get(),
                builder: (context, snapshot){
                  if(snapshot.data == null) return CircularProgressIndicator();

                  if(snapshot.hasError){
                    return Text("Something went wrong!");
                  }

                  else {
                    final List<DocumentSnapshot> documents = snapshot.data.docs;

                    return Container(
                      margin: EdgeInsets.only(top: 300),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 25, right: 25),
                            height: 200,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black45,
                              ),
                            ),
                            child: GoogleMap(
                              mapType: MapType.normal,
                              markers: _createMarker(),
                              initialCameraPosition: CameraPosition(
                                target: LatLng(1.566858, 103.634609),
                                zoom: 16.5,
                              ),
                              onMapCreated: (GoogleMapController controller) {
                                _controller = controller;
                              }

                            ),
                          ),

                          Container(
                            margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                            child: Text(
                              '1.566858, 103.634609',
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 13,
                                color: const Color(0xff656d74),
                                fontWeight: FontWeight.w700,
                              ),
                            )
                          ),

                          Container(
                            margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                            color: Colors.white,
                            child: Table(
                              columnWidths: {
                                0: FlexColumnWidth(2),
                                1: FlexColumnWidth(5),
                              },
                              border: TableBorder.all(color: Colors.black45),
                              children: [
                                TableRow(
                                  children: [
                                    TableCell(
                                      child: Container(
                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                        child: Center(
                                          child: Text(
                                            'RailID',
                                            style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontSize: 15,
                                              color: const Color(0xff656d74),
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    TableCell(
                                      child: Container(
                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                        child: Center(
                                          child: Text(
                                            'BicycleID',
                                            style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontSize: 15,
                                              color: const Color(0xff656d74),
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]
                                ),
                              ]
                            ),
                          ),

                          Container(
                            margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                            child: ListView(
                              children: documents.map((doc) =>
                                Table(
                                  columnWidths: {
                                    0: FlexColumnWidth(2),
                                    1: FlexColumnWidth(5),
                                  },
                                  border: TableBorder.all(color: Colors.black45),
                                  children: [
                                    TableRow(
                                      children: [
                                        TableCell(
                                          child: Container(
                                            color: Colors.white,
                                            padding: EdgeInsets.only(top: 5, bottom: 5),
                                            child: Center(
                                              child: Text(
                                                doc['railID'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 12,
                                                  color: const Color(0xff656d74),
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        TableCell(
                                          child: Container(
                                            color: Colors.white,
                                            padding: EdgeInsets.only(top: 5, bottom: 5),
                                            child: Center(
                                              child: Text(
                                                doc['bicycleID'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 12,
                                                  color: const Color(0xff656d74),
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ]
                                    ),
                                  ]
                                ),
                              ).toList()
                            ),
                          ),
                        ]
                      ),
                    );
                  }
                }
              ),
            ),

          if (stop == 'S002 - M01, Kolej Tun Dr Ismail')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S002").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: new GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.564163, 103.635089),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.564163, 103.635089',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S003 - L01, Kolej Tun Hussein Onn')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S003").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.562851, 103.6319),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.562851, 103.631900',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S004 - Arked Cengal')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S004").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.561551, 103.632121),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.561551, 103.632121',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S005 - K01, Kolej Tun Razak')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S005").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.562327, 103.628742),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.562327, 103.628742',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S006 - Food Court Kolej Datin Seri Endon')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S006").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.567547, 103.624617),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.567547, 103.624617',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S007 - H01, Kolej Tun Fatimah')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S007").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.559979, 103.631364),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.559979, 103.631364',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S008 - L50, Lecture Hall')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S008").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.561338, 103.634795),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.561338, 103.634795',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S009 - Arked Meranti')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S009").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.559746, 103.633785),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.559746, 103.633785',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S010 - G01, Kolej Rahman Putra')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S010").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.557973, 103.632795),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.557973, 103.632795',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S011 - N28a, School of Computing')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S011").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.564535, 103.637235),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.564535, 103.637235',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S012 - Perpustakaan Sultan Zanariah')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S012").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.559520, 103.636724),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.559520, 103.636724',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S013 - P19, Lecture Hall')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S013").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.559338, 103.641289),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.559338, 103.641289',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S014 - Dewan Astana Kolej Tuanku Canselor')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S014").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.554764, 103.644877),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.554764, 103.644877',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S015 - Arked Lestari')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S015").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.559486, 103.648654),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.559486, 103.648654',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S016 - Stadium Azman Hashim, UTM')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S016").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.556679, 103.656014),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.556679, 103.656014',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S017 - Scholars Inn')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S017").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.557619, 103.648285),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.557619, 103.648285',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),

          if (stop == 'S018 - Perpustakaan Raja Zarith Sofiah')
            Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                  future: ref.where("stopID", isEqualTo: "S018").get(),
                  builder: (context, snapshot){
                    if(snapshot.data == null) return CircularProgressIndicator();

                    if(snapshot.hasError){
                      return Text("Something went wrong!");
                    }

                    else {
                      final List<DocumentSnapshot> documents = snapshot.data.docs;

                      return Container(
                        margin: EdgeInsets.only(top: 300),
                        child: Stack(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 25, right: 25),
                                height: 200,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black45,
                                  ),
                                ),
                                child: GoogleMap(
                                    mapType: MapType.normal,
                                    markers: _createMarker(),
                                    initialCameraPosition: CameraPosition(
                                      target: LatLng(1.563413, 103.651825),
                                      zoom: 16.5,
                                    ),
                                    onMapCreated: (GoogleMapController controller) {
                                      _controller = controller;
                                    }
                                ),
                              ),

                              Container(
                                  margin: EdgeInsets.only(top: 203, left: 200, right: 25),
                                  child: Text(
                                    '1.563413, 103.651825',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 13,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 250, left: 25, right: 25),
                                color: Colors.white,
                                child: Table(
                                    columnWidths: {
                                      0: FlexColumnWidth(2),
                                      1: FlexColumnWidth(5),
                                    },
                                    border: TableBorder.all(color: Colors.black45),
                                    children: [
                                      TableRow(
                                          children: [
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'RailID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            TableCell(
                                              child: Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                child: Center(
                                                  child: Text(
                                                    'BicycleID',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]
                                      ),
                                    ]
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 243, left: 25, right: 25),
                                child: ListView(
                                    children: documents.map((doc) =>
                                        Table(
                                            columnWidths: {
                                              0: FlexColumnWidth(2),
                                              1: FlexColumnWidth(5),
                                            },
                                            border: TableBorder.all(color: Colors.black45),
                                            children: [
                                              TableRow(
                                                  children: [
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['railID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    TableCell(
                                                      child: Container(
                                                        color: Colors.white,
                                                        padding: EdgeInsets.only(top: 5, bottom: 5),
                                                        child: Center(
                                                          child: Text(
                                                            doc['bicycleID'],
                                                            style: TextStyle(
                                                              fontFamily: 'Montserrat',
                                                              fontSize: 12,
                                                              color: const Color(0xff656d74),
                                                              fontWeight: FontWeight.w700,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ]
                                        ),
                                    ).toList()
                                ),
                              ),
                            ]
                        ),
                      );
                    }
                  }
              ),
            ),
        ],
      ),
    );
  }

  Set<Marker> _createMarker() {
    MarkerId markerId2 = MarkerId("S001");
    MarkerId markerId3 = MarkerId("S002");
    MarkerId markerId4 = MarkerId("S003");
    MarkerId markerId5 = MarkerId("S004");
    MarkerId markerId6 = MarkerId("S005");
    MarkerId markerId7 = MarkerId("S006");
    MarkerId markerId8 = MarkerId("S007");
    MarkerId markerId9 = MarkerId("S008");
    MarkerId markerId10 = MarkerId("S009");
    MarkerId markerId11 = MarkerId("S010");
    MarkerId markerId12 = MarkerId("S011");
    MarkerId markerId13 = MarkerId("S012");
    MarkerId markerId14 = MarkerId("S013");
    MarkerId markerId15 = MarkerId("S014");
    MarkerId markerId16 = MarkerId("S015");
    MarkerId markerId17 = MarkerId("S016");
    MarkerId markerId18 = MarkerId("S017");
    MarkerId markerId19 = MarkerId("S018");

    Marker marker2 = Marker(
      markerId: markerId2,
      position: LatLng(1.566858, 103.634609),
      icon: stopLocIcon,
      consumeTapEvents: true,
    );

    Marker marker3 = Marker(
      markerId: markerId3,
      position: LatLng(1.564163, 103.635089),
      icon: stopLocIcon,
    );

    Marker marker4 = Marker(
      markerId: markerId4,
      position: LatLng(1.562851, 103.631900),
      icon: stopLocIcon,
    );

    Marker marker5 = Marker(
      markerId: markerId5,
      position: LatLng(1.561551, 103.632121),
      icon: stopLocIcon,
    );

    Marker marker6 = Marker(
      markerId: markerId6,
      position: LatLng(1.562327, 103.628742),
      icon: stopLocIcon,
    );

    Marker marker7 = Marker(
      markerId: markerId7,
      position: LatLng(1.567547, 103.624617),
      icon: stopLocIcon,
    );

    Marker marker8 = Marker(
      markerId: markerId8,
      position: LatLng(1.559979, 103.631364),
      icon: stopLocIcon,
    );

    Marker marker9 = Marker(
      markerId: markerId9,
      position: LatLng(1.561338, 103.634795),
      icon: stopLocIcon,
    );

    Marker marker10 = Marker(
      markerId: markerId10,
      position: LatLng(1.559746, 103.633785),
      icon: stopLocIcon,
    );

    Marker marker11 = Marker(
      markerId: markerId11,
      position: LatLng(1.557973, 103.632795),
      icon: stopLocIcon,
    );

    Marker marker12 = Marker(
      markerId: markerId12,
      position: LatLng(1.564535, 103.637235),
      icon: stopLocIcon,
    );

    Marker marker13 = Marker(
      markerId: markerId13,
      position: LatLng(1.559520, 103.636724),
      icon: stopLocIcon,
    );

    Marker marker14 = Marker(
      markerId: markerId14,
      position: LatLng(1.559338, 103.641289),
      icon: stopLocIcon,
    );

    Marker marker15 = Marker(
      markerId: markerId15,
      position: LatLng(1.554764, 103.644877),
      icon: stopLocIcon,
    );

    Marker marker16 = Marker(
      markerId: markerId16,
      position: LatLng(1.559486, 103.648654),
      icon: stopLocIcon,
    );

    Marker marker17 = Marker(
      markerId: markerId17,
      position: LatLng(1.556679, 103.656014),
      icon: stopLocIcon,
    );

    Marker marker18 = Marker(
      markerId: markerId18,
      position: LatLng(1.557619, 103.648285),
      icon: stopLocIcon,
    );

    Marker marker19 = Marker(
      markerId: markerId19,
      position: LatLng(1.563413, 103.651825),
      icon: stopLocIcon,
    );

    Map<MarkerId,Marker> markers = {};

    markers[markerId2]=marker2;
    markers[markerId3]=marker3;
    markers[markerId4]=marker4;
    markers[markerId5]=marker5;
    markers[markerId6]=marker6;
    markers[markerId7]=marker7;
    markers[markerId8]=marker8;
    markers[markerId9]=marker9;
    markers[markerId10]=marker10;
    markers[markerId11]=marker11;
    markers[markerId12]=marker12;
    markers[markerId13]=marker13;
    markers[markerId14]=marker14;
    markers[markerId15]=marker15;
    markers[markerId16]=marker16;
    markers[markerId17]=marker17;
    markers[markerId18]=marker18;
    markers[markerId19]=marker19;

    return Set.of(markers.values);
  }
}

const String _svg_ef2urf =
    '<svg viewBox="31.4 54.6 9.6 16.6" ><path transform="translate(-237.75, -83.36)" d="M 278.7493896484375 137.9653625488281 L 269.19140625 146.2719116210938 L 278.7493896484375 154.5784454345703" fill="none" stroke="#ffffff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
