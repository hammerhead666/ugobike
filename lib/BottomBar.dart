import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './Search.dart';
import './RentingHistory.dart';
import './HelpCentre.dart';
import './UserProfile.dart';


// ignore: must_be_immutable
class BottomBar extends StatefulWidget{
  int curPage = 0;

  BottomBar(int page){
    curPage = page;
  }

  @override
  BottomBarState createState() => new BottomBarState(curPage);
}

class BottomBarState extends State<BottomBar>{
  int _pageIndex = 0;
  PageController _pageController;

  BottomBarState(int page){
    _pageIndex = page;
  }

  List<Widget> tabPages = [
    Search(),
    HistoryPage(),
    HelpCentre(),
    UserProfile()
  ];

  @override
  void initState(){
    super.initState();
    _pageController = PageController(initialPage: _pageIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      bottomNavigationBar: SizedBox(
        height: 75,
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: const Color(0xff656d74),
          unselectedItemColor: Colors.grey,
          currentIndex: _pageIndex,
          onTap: onTabTapped,
          backgroundColor: Colors.white,

          items: <BottomNavigationBarItem>[
            // ignore: deprecated_member_use
            BottomNavigationBarItem(icon: Icon(Icons.explore_outlined), title: Text("Search", style:TextStyle(fontFamily: 'Montserrat'),)),
            BottomNavigationBarItem(icon: Icon(Icons.history_outlined), title: Text("History", style:TextStyle(fontFamily: 'Montserrat'),)),
            BottomNavigationBarItem(icon: Icon(Icons.help_center_outlined), title: Text("Help Centre", style:TextStyle(fontFamily: 'Montserrat'),)),
            BottomNavigationBarItem(icon: Icon(Icons.person_outlined), title: Text("Profile", style:TextStyle(fontFamily: 'Montserrat'),)),
          ],
        ),
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        children: tabPages,
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
    );
  }

  void onPageChanged(int page) {
    setState(() {
      this._pageIndex = page;
    });
  }

  void onTabTapped(int index) {
    this._pageController.animateToPage(index,duration: const Duration(milliseconds: 500),curve: Curves.easeInOut);
  }
} //class