import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_svg/flutter_svg.dart';

class support_agent24px extends StatelessWidget {
  support_agent24px({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Pinned.fromSize(
          bounds: Rect.fromLTWH(0.0, 0.0, 35.0, 35.0),
          size: Size(35.0, 35.0),
          pinLeft: true,
          pinRight: true,
          pinTop: true,
          pinBottom: true,
          child: Stack(
            children: <Widget>[
              Pinned.fromSize(
                bounds: Rect.fromLTWH(0.0, 0.0, 35.0, 35.0),
                size: Size(35.0, 35.0),
                pinLeft: true,
                pinRight: true,
                pinTop: true,
                pinBottom: true,
                child: Container(
                  decoration: BoxDecoration(),
                ),
              ),
            ],
          ),
        ),
        Pinned.fromSize(
          bounds: Rect.fromLTWH(2.0, 3.0, 31.0, 29.0),
          size: Size(35.0, 35.0),
          pinLeft: true,
          pinRight: true,
          pinTop: true,
          pinBottom: true,
          child: Stack(
            children: <Widget>[
              Pinned.fromSize(
                bounds: Rect.fromLTWH(0.0, 0.0, 31.0, 29.0),
                size: Size(31.0, 29.0),
                pinLeft: true,
                pinRight: true,
                pinTop: true,
                pinBottom: true,
                child: Stack(
                  children: <Widget>[
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(0.0, 0.0, 31.0, 29.0),
                      size: Size(31.0, 29.0),
                      pinLeft: true,
                      pinRight: true,
                      pinTop: true,
                      pinBottom: true,
                      child: SvgPicture.string(
                        _svg_9y986w,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(9.7, 15.2, 2.0, 2.0),
                      size: Size(31.0, 29.0),
                      fixedWidth: true,
                      fixedHeight: true,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.elliptical(9999.0, 9999.0)),
                          color: const Color(0xff656d74),
                        ),
                      ),
                    ),
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(19.3, 15.2, 2.0, 2.0),
                      size: Size(31.0, 29.0),
                      fixedWidth: true,
                      fixedHeight: true,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.elliptical(9999.0, 9999.0)),
                          color: const Color(0xff656d74),
                        ),
                      ),
                    ),
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(4.0, 5.9, 23.0, 6.4),
                      size: Size(31.0, 29.0),
                      pinLeft: true,
                      pinRight: true,
                      fixedHeight: true,
                      child: SvgPicture.string(
                        _svg_oneztz,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

const String _svg_9y986w =
    '<svg viewBox="0.0 0.0 31.0 29.0" ><path transform="translate(-2.0, -3.0)" d="M 31.44999885559082 17.85444641113281 C 31.44999885559082 9.009445190429688 24.84700012207031 3 17.5 3 C 10.23049926757813 3 3.549999713897705 8.880556106567383 3.549999713897705 17.95111083984375 C 2.619999885559082 18.4988899230957 1.999999761581421 19.53000068664551 1.999999761581421 20.72222518920898 L 1.999999761581421 23.9444465637207 C 1.999999761581421 25.7166690826416 3.395000219345093 27.16666793823242 5.099999904632568 27.16666793823242 L 6.650000095367432 27.16666793823242 L 6.650000095367432 17.33889007568359 C 6.650000095367432 11.10388946533203 11.50149917602539 6.061110496520996 17.5 6.061110496520996 C 23.49849891662598 6.061110496520996 28.34999847412109 11.10388946533203 28.34999847412109 17.33889007568359 L 28.34999847412109 28.77777862548828 L 15.94999885559082 28.77777862548828 L 15.94999885559082 32 L 28.34999847412109 32 C 30.05500030517578 32 31.44999885559082 30.55000305175781 31.44999885559082 28.77777862548828 L 31.44999885559082 26.81222534179688 C 32.36449813842773 26.31278038024902 32.99999618530273 25.33000183105469 32.99999618530273 24.17000389099121 L 32.99999618530273 20.46444702148438 C 32.99999618530273 19.33666801452637 32.36449813842773 18.35388946533203 31.44999885559082 17.85444641113281 Z" fill="#656d74" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_oneztz =
    '<svg viewBox="4.0 5.9 23.0 6.4" ><path transform="translate(-2.0, -0.14)" d="M 28.99999809265137 11.02999973297119 C 28.07979965209961 8.180000305175781 23.32544326782227 6 17.59337425231934 6 C 11.78462696075439 6 5.534946918487549 8.510000228881836 6.033387660980225 12.44999980926514 C 10.76857376098633 11.4399995803833 14.33434200286865 9.239999771118164 15.3503942489624 6.559999942779541 C 17.86176681518555 9.189999580383301 23.01871109008789 11 28.99999809265137 11.02999973297119 Z" fill="#656d74" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
