import 'package:flutter/material.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import './Search.dart';
import 'package:firebase_auth/firebase_auth.dart';
import './BottomBar.dart';

class Payment extends StatefulWidget {
  final double price;
  final int hours;
  final int minutes;
  final int seconds;

  Payment(this.price, this.hours, this.minutes, this.seconds);

  @override
  PaymentState createState() => PaymentState(price, hours, minutes, seconds);
}

class PaymentState extends State<Payment> {
  final double price;
  final int hours;
  final int minutes;
  final int seconds;
  int finalPrice;
  String phone;
  String mail;

  final FirebaseAuth auth = FirebaseAuth.instance;

  PaymentState(this.price, this.hours, this.minutes, this.seconds);
  //static const platform = const MethodChannel("razorpay_flutter");

  Razorpay _razorpay;

  getUserEmail() {
    final User user = auth.currentUser;
    final email = user.email;

    return email;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      backgroundColor: const Color(0xfffafdff),
      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            height: 150,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),

            child: Container(
              alignment: Alignment.center,
              child: Image(
                height: 200,
                image: const AssetImage('assets/images/logo.png'),
              ),
            ),
          ),

          Container(
            margin: const EdgeInsets.only(left: 30),
            width: 500,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 150),
                Text(
                  'Total Time Rented: ',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    color: const Color(0xff656d74),
                  ),
                ),

                Text(
                  hours.toString() + 'hour ' + minutes.toString() + 'minutes ' + seconds.toString() + 'seconds',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    color: const Color(0xff656d74),
                  ),
                ),

                SizedBox(height: 20),

                Text(
                  'Rental Fees: RM' + price.toStringAsFixed(2),
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    color: const Color(0xff656d74),
                  ),
                ),

                SizedBox(height: 50),
              ],
            )
          ),

          InkWell(
            onTap: openCheckout,
            child: Container(
              alignment: Alignment.center,
              height: 45,
              width: 250,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(7.0),
                ),
                color: const Color(0xff656d74),
              ),
              child: Text(
                'Continue to Payment',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  color: const Color(0xffffffff),
                ),
              )
            )
          )
        ]
      ),
    );
  }

  @override
  void initState() {
    finalPrice = price.toInt() * 100;
    mail = getUserEmail();


    super.initState();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override

  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void openCheckout() async {
    var options = {
      'key': 'rzp_test_b79HdfzWyaPxOw',
      'amount': finalPrice,
      'currency': "MYR",
      'name': 'UGoBike',
      'description': 'Rental Fees',
      'prefill': {'email': mail},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint(e);
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId, timeInSecForIos: 4);

    Navigator.of(context).push(MaterialPageRoute(builder: (context) => BottomBar(0)));
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        timeInSecForIos: 4);

    Navigator.of(context).push(MaterialPageRoute(builder: (context) => Payment(price, hours, minutes, seconds)));
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, timeInSecForIos: 4);
  }
}