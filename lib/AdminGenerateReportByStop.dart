import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'dart:async';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './StopReportPreview.dart';
import './core/models/rentingHistoryModel.dart';
import './AdminGenerateReport.dart';

class AdminGenerateReportByStopPage extends StatefulWidget{
  @override
  AdminGenerateReportByStopPageState createState(){
    return AdminGenerateReportByStopPageState();
  }
}

class AdminGenerateReportByStopPageState extends State<AdminGenerateReportByStopPage>{

  String stop;
  String sort;
  final pdf = pw.Document(deflate: zlib.encode);
  CollectionReference ref = FirebaseFirestore.instance.collection('RentingHistory');
  final List<RentingHistory> rentList = [];
  final List<RentingHistory> returnList = [];
  double totalProfit = 0;


  @override
  Widget build(BuildContext context){

    return Scaffold(
      backgroundColor: const Color(0xfffafdff),

      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 20),
            height: 150,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  child: IconButton(
                      icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (BuildContext context) => AdminGenerateReportPage(),
                            fullscreenDialog: true,
                          ),
                        );
                      }
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 28),
                  child: Image(
                    height: 200,
                    image: const AssetImage('assets/images/logo.png'),
                  ),
                ),
              ],
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 40),
            child: Text(
              'View Report By Stop',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 25,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
                height: 1.2,
              ),
              textAlign: TextAlign.center,
            ),
          ),


          Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: const Color(0xffbfa780),
                ),
              ),
              margin: const EdgeInsets.only(top: 70, left: 40, right: 40),
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: DropdownButton<String>(
                isExpanded: true,
                hint: new Text('Select a Stop',
                    style: TextStyle(
                        color: const Color(0x80bfa780),
                        fontSize: 16
                    )
                ),
                underline: Container(),
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 15,
                  color: const Color(0xffbfa780),
                ),

                items: <String>[
                  'S001 Arked Angkasa',
                  'S002 M01, Kolej Tun Dr Ismail',
                  'S003 L01, Kolej Tun Hussein Onn',
                  'S004 Arked Cengal',
                  'S005 K01, Kolej Tun Razak',
                  'S006 Food Court Kolej Datin Seri Endon',
                  'S007 H01, Kolej Tun Fatimah',
                  'S008 L50, Lecture Hall',
                  'S009 Arked Meranti',
                  'S010 G01, Kolej Rahman Putra',
                  'S011 N28a, School of Computing',
                  'S012 Perpustakaan Sultan Zanariah',
                  'S013 P19, Lecture Hall',
                  'S014 Dewan Astana Kolej Tuanku Canselor',
                  'S015 Arked Lestari',
                  'S016 Stadium Azman Hashim, UTM',
                  'S017 Scholars Inn',
                  'S018 Perpustakaan Raja Zarith Sofiah'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),

                onChanged: (String newValue) {
                  setState(() {
                    stop = newValue;
                  });
                },

                value: stop,
              ),
            ),
          ),

          Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: const Color(0xffbfa780),
                ),
              ),
              margin: const EdgeInsets.only(top: 20, left: 40, right: 40),
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: DropdownButton<String>(
                isExpanded: true,
                hint: new Text('Sort by',
                    style: TextStyle(
                        color: const Color(0x80bfa780),
                        fontSize: 16
                    )
                ),
                underline: Container(),
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 15,
                  color: const Color(0xffbfa780),
                ),

                items: <String>[
                  'Date (Latest to Oldest)',
                  'Date (Oldest to Latest)'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),

                onChanged: (String newValue) {
                  setState(() {
                    sort = newValue;
                  });
                },

                value: sort,
              ),
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 160),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: ButtonTheme(
                minWidth: 135.0,
                height: 40.0,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    //side: BorderSide(color: Colors.red)
                  ),
                  color: const Color(0xffbfa780),
                  onPressed: () async{

                    if(stop == null || sort == null){
                      showDialog(
                          context: context,
                          child: new AlertDialog(
                            title: Text('Invalid Choice'),
                            content: Text(
                                "Please select Stop and Sort."
                            ),
                            actions: [
                              FlatButton(
                                  textColor: const Color(0xff656d74),
                                  onPressed: (){
                                    Navigator.of(context, rootNavigator: true).pop('dialog');
                                  },
                                  child: Text('Ok')
                              )
                            ],
                          )
                      );
                    }

                    else{
                      await fetchAndSetList();
                      await writeOnPDF();
                      await savePdf();

                      Directory documentDirectory = await getApplicationDocumentsDirectory();

                      String documentPath = documentDirectory.path;

                      String fullPath = "$documentPath/stop_report_${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}.pdf";

                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => StopReportPreview(fullPath)
                      ));
                    }


                  },
                  child: Text('Generate',
                      style:
                      TextStyle(fontFamily: 'Montserrat',
                          fontSize: 16.5,
                          color: const Color(0xffffffff),
                          fontWeight: FontWeight.w700)
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future <void> fetchAndSetList() async{

    if(stop == "S001 Arked Angkasa" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S001").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S001").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S001 Arked Angkasa" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S001").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S001").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S002 M01, Kolej Tun Dr Ismail" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S002").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S002").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S002 M01, Kolej Tun Dr Ismail" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S002").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S002").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S003 L01, Kolej Tun Hussein Onn" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S003").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S003").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S003 L01, Kolej Tun Hussein Onn" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S003").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S003").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S004 Arked Cengal" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S004").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S004").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S004 Arked Cengal" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S004").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S004").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S005 K01, Kolej Tun Razak" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S005").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S005").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S005 K01, Kolej Tun Razak" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S005").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S005").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S006 Food Court Kolej Datin Seri Endon" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S006").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S006").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S006 Food Court Kolej Datin Seri Endon" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S006").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S006").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S007 H01, Kolej Tun Fatimah" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S007").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S007").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S007 H01, Kolej Tun Fatimah" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S007").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S007").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S008 L50, Lecture Hall" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S008").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S008").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S008 L50, Lecture Hall" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S008").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S008").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S009 Arked Meranti" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S009").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S009").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S009 Arked Meranti" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S009").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S009").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S010 G01, Kolej Rahman Putra" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S010").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S010").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S010 G01, Kolej Rahman Putra" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S010").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S010").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S011 N28a, School of Computing" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S011").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S011").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S011 N28a, School of Computing" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S011").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S011").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S012 Perpustakaan Sultan Zanariah" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S012").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S012").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S012 Perpustakaan Sultan Zanariah" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S012").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S012").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S013 P19, Lecture Hall" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S013").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S013").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S013 P19, Lecture Hall" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S013").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S013").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S014 Dewan Astana Kolej Tuanku Canselor" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S014").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S014").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S014 Dewan Astana Kolej Tuanku Canselor" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S014").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S014").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S015 Arked Lestari" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S015").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S015").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S015 Arked Lestari" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S015").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S015").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S016 Stadium Azman Hashim, UTM" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S016").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S016").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S016 Stadium Azman Hashim, UTM" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S016").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S016").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S017 Scholars Inn" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S017").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S017").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S017 Scholars Inn" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S017").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S017").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S018 Perpustakaan Raja Zarith Sofiah" && sort == "Date (Latest to Oldest)"){
      await ref.where("rentStop", isEqualTo: "S018").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S018").orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(stop == "S018 Perpustakaan Raja Zarith Sofiah" && sort == "Date (Oldest to Latest)"){
      await ref.where("rentStop", isEqualTo: "S018").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            rentList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );

      await ref.where("returnStop", isEqualTo: "S018").orderBy('dateRented', descending: false).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach( (f) => {
            returnList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

  }

  Future writeOnPDF() async{
    const imageProvider = const AssetImage('assets/images/logo.png');
    final image = await flutterImageProvider(imageProvider);

    for(var index in returnList)
      totalProfit = totalProfit + index.rentingFee;

    if(stop == "S001 Arked Angkasa"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-001",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Arked Angkasa",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Column(
                    children: [
                      pw.Row(
                          children: <pw.Widget>[
                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                width: 150,
                                height: 40,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Date Rented",
                                  style: pw.TextStyle(
                                    fontSize: 15,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),

                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                height: 40,
                                width: 100,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Return Stop",
                                  style: pw.TextStyle(
                                    fontSize: 15,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),

                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                height: 40,
                                width: 88,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Bicycle ID",
                                  style: pw.TextStyle(
                                    fontSize: 15,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),

                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                height: 40,
                                width: 90,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Total Time",
                                  style: pw.TextStyle(
                                    fontSize: 15,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),

                            pw.Container(
                                padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                height: 40,
                                width: 100,
                                decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                      color: PdfColors.black,
                                    )
                                ),
                                child: pw.Text(
                                  "Renting Fee",
                                  style: pw.TextStyle(
                                    fontSize: 15,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                  textAlign: pw.TextAlign.center,
                                )
                            ),
                          ]
                      ),

                      if(rentList.length == 0)
                        pw.Row(
                            children: <pw.Widget>[
                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 150,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Container(
                                      child: pw.Text(
                                        "NIL",
                                        style: pw.TextStyle(
                                          fontSize: 15,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  )
                              ),

                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 100,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              ),

                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 88,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              ),

                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 90,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              ),

                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 100,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              ),
                            ]
                        ),

                      for(var count in rentList)
                        pw.Row(
                            children: <pw.Widget>[
                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 150,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Container(
                                      child: pw.Text(
                                        "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                        style: pw.TextStyle(
                                          fontSize: 15,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  )
                              ),

                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 100,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Text(
                                    count.returnStop.toString(),
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              ),

                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 88,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Text(
                                    count.bicycleID.toString(),
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              ),

                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 90,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Text(
                                    "${count.totalMin.toString()}" + " min",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              ),

                              pw.Container(
                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                  height: 38,
                                  width: 100,
                                  decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                        color: PdfColors.grey800,
                                      )
                                  ),
                                  child: pw.Text(
                                    "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              ),
                            ]
                        ),
                    ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),


                  pw.Column(
                      children: [
                        pw.Container(
                          margin: pw.EdgeInsets.all(0),
                          child: pw.Row(
                              children: <pw.Widget>[
                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    width: 150,
                                    height: 40,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.black,
                                        )
                                    ),
                                    child: pw.Text(
                                      "Date Rented",
                                      style: pw.TextStyle(
                                        fontSize: 15,
                                        fontWeight: pw.FontWeight.bold,
                                      ),
                                      textAlign: pw.TextAlign.center,
                                    )
                                ),

                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    height: 40,
                                    width: 100,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.black,
                                        )
                                    ),
                                    child: pw.Text(
                                      "Rent Stop",
                                      style: pw.TextStyle(
                                        fontSize: 15,
                                        fontWeight: pw.FontWeight.bold,
                                      ),
                                      textAlign: pw.TextAlign.center,
                                    )
                                ),

                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    height: 40,
                                    width: 88,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.black,
                                        )
                                    ),
                                    child: pw.Text(
                                      "Bicycle ID",
                                      style: pw.TextStyle(
                                        fontSize: 15,
                                        fontWeight: pw.FontWeight.bold,
                                      ),
                                      textAlign: pw.TextAlign.center,
                                    )
                                ),

                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    height: 40,
                                    width: 90,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.black,
                                        )
                                    ),
                                    child: pw.Text(
                                      "Total Time",
                                      style: pw.TextStyle(
                                        fontSize: 15,
                                        fontWeight: pw.FontWeight.bold,
                                      ),
                                      textAlign: pw.TextAlign.center,
                                    )
                                ),

                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    height: 40,
                                    width: 100,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.black,
                                        )
                                    ),
                                    child: pw.Text(
                                      "Renting Fee",
                                      style: pw.TextStyle(
                                        fontSize: 15,
                                        fontWeight: pw.FontWeight.bold,
                                      ),
                                      textAlign: pw.TextAlign.center,
                                    )
                                ),
                              ]
                          ),
                        ),

                        if(returnList.length == 0)
                          pw.Row(
                              children: [
                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    height: 38,
                                    width: 150,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.grey800,
                                        )
                                    ),
                                    child: pw.Container(
                                        child: pw.Text(
                                          "NIL",
                                          style: pw.TextStyle(
                                            fontSize: 15,
                                          ),
                                          textAlign: pw.TextAlign.center,
                                        )
                                    )
                                ),

                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    height: 38,
                                    width: 100,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.grey800,
                                        )
                                    ),
                                    child: pw.Text(
                                      "NIL",
                                      style: pw.TextStyle(
                                        fontSize: 15,
                                      ),
                                      textAlign: pw.TextAlign.center,
                                    )
                                ),

                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    height: 38,
                                    width: 88,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.grey800,
                                        )
                                    ),
                                    child: pw.Text(
                                      "NIL",
                                      style: pw.TextStyle(
                                        fontSize: 15,
                                      ),
                                      textAlign: pw.TextAlign.center,
                                    )
                                ),

                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    height: 38,
                                    width: 90,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.grey800,
                                        )
                                    ),
                                    child: pw.Text(
                                      "NIL",
                                      style: pw.TextStyle(
                                        fontSize: 15,
                                      ),
                                      textAlign: pw.TextAlign.center,
                                    )
                                ),

                                pw.Container(
                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                    height: 38,
                                    width: 100,
                                    decoration: pw.BoxDecoration(
                                        border: pw.Border.all(
                                          color: PdfColors.grey800,
                                        )
                                    ),
                                    child: pw.Text(
                                      "NIL",
                                      style: pw.TextStyle(
                                        fontSize: 15,
                                      ),
                                      textAlign: pw.TextAlign.center,
                                    )
                                ),
                              ]
                          ),

                        for(var index in returnList)
                          pw.Container(
                            margin: pw.EdgeInsets.only(top:0),
                            child: pw.Row(
                                children: [
                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 38,
                                      width: 150,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.grey800,
                                          )
                                      ),
                                      child: pw.Container(
                                          child: pw.Text(
                                            "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                            style: pw.TextStyle(
                                              fontSize: 15,
                                            ),
                                            textAlign: pw.TextAlign.center,
                                          )
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 38,
                                      width: 100,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.grey800,
                                          )
                                      ),
                                      child: pw.Text(
                                        index.rentStop.toString(),
                                        style: pw.TextStyle(
                                          fontSize: 15,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 38,
                                      width: 88,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.grey800,
                                          )
                                      ),
                                      child: pw.Text(
                                        index.bicycleID.toString(),
                                        style: pw.TextStyle(
                                          fontSize: 15,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 38,
                                      width: 90,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.grey800,
                                          )
                                      ),
                                      child: pw.Text(
                                        "${index.totalMin.toString()}" + " min",
                                        style: pw.TextStyle(
                                          fontSize: 15,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 38,
                                      width: 100,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.grey800,
                                          )
                                      ),
                                      child: pw.Text(
                                        "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                        style: pw.TextStyle(
                                          fontSize: 15,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),
                                ]
                            ),
                          ),
                      ]
                  ),




                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S002 M01, Kolej Tun Dr Ismail"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-002",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Kolej Tun Dr Ismail",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S003 L01, Kolej Tun Hussein Onn"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-003",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "L01, Kolej Tun Hussein Onn",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S004 Arked Cengal"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-004",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Arked Cengal",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S005 K01, Kolej Tun Razak"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-005",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "K01, Kolej Tun Razak",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S006 Food Court Kolej Datin Seri Endon"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-006",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Food Court Kolej Datin Seri Endon",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S007 H01, Kolej Tun Fatimah"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-007",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "H01, Kolej Tun Fatimah",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S008 L50, Lecture Hall"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-008",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "L50, Lecture Hall",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S009 Arked Meranti"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-009",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Arked Meranti",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S010 G01, Kolej Rahman Putra"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-010",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "G01, Kolej Rahman Putra",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S011 N28a, School of Computing"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-011",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "N28a, School of Computing",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S012 Perpustakaan Sultan Zanariah"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-012",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Perpustakaan Sultan Zanariah",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S013 P19, Lecture Hall"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-013",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "P19, Lecture Hall",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S014 Dewan Astana Kolej Tuanku Canselor"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-014",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Dewan Astana Kolej Tuanku Canselor",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S015 Arked Lestari"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-015",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Arked Lestari",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S016 Stadium Azman Hashim, UTM"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-016",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Stadium Azman Hashim, UTM",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S017 Scholars Inn"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-017",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Scholars Inn",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }

    if(stop == "S018 Perpustakaan Raja Zarith Sofiah"){
      pdf.addPage(
          pw.MultiPage(
              pageFormat: PdfPageFormat.a4,
              margin: pw.EdgeInsets.all(30),
              orientation: pw.PageOrientation.portrait,
              maxPages: 5,

              build: (pw.Context context){
                return <pw.Widget>[
                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                          height: 120,
                          width: 1000,
                          decoration: pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.circular(10.0),
                            color: const PdfColor.fromInt(0xff656d74),
                          ),
                          child: pw.Image(image),
                        ),

                        pw.Expanded(
                          // flex: 7,
                            child: pw.Container(
                                margin: pw.EdgeInsets.only(left: 35),
                                child: pw.Text(
                                  "UGoBike Report S-018",
                                  style: pw.TextStyle(
                                    fontSize: 35,
                                    fontWeight: pw.FontWeight.bold,
                                  ),
                                )
                            )
                        )
                      ]
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(top: 40),
                    child: pw.Header(
                        level: 0,
                        child: pw.Text("")
                    ),
                  ),

                  pw.Align(
                    alignment: pw.Alignment.topRight,
                    child: pw.Container(
                      margin: pw.EdgeInsets.only(top: 30, bottom: 30),
                      child: pw.Text(
                        "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                        style: pw.TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: pw.TextAlign.right,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 30),
                    child: pw.Text(
                      "Perpustakaan Raja Zarith Sofiah",
                      style: pw.TextStyle(
                        fontSize: 30,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),

                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Pick Up",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Return Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(rentList.length == 0)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var count in rentList)
                    pw.Row(
                        children: <pw.Widget>[
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(count.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.returnStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                count.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${count.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${count.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                    margin: pw.EdgeInsets.only(top:50, bottom: 20),
                    child: pw.Header(
                        level: 1,
                        child: pw.Text("Return",
                          style: pw.TextStyle(
                            fontSize: 28,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                    ),
                  ),

                  pw.Row(
                      children: <pw.Widget>[
                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            width: 150,
                            height: 40,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Date Rented",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Rent Stop",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 88,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Bicycle ID",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 90,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Total Time",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),

                        pw.Container(
                            padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                            height: 40,
                            width: 100,
                            decoration: pw.BoxDecoration(
                                border: pw.Border.all(
                                  color: PdfColors.black,
                                )
                            ),
                            child: pw.Text(
                              "Renting Fee",
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                              textAlign: pw.TextAlign.center,
                            )
                        ),
                      ]
                  ),

                  if(returnList.length == 0)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "NIL",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "NIL",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),

                  for(var index in returnList)
                    pw.Row(
                        children: [
                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 150,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Container(
                                  child: pw.Text(
                                    "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                    style: pw.TextStyle(
                                      fontSize: 15,
                                    ),
                                    textAlign: pw.TextAlign.center,
                                  )
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.rentStop.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 88,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                index.bicycleID.toString(),
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 90,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "${index.totalMin.toString()}" + " min",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),

                          pw.Container(
                              padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                              height: 38,
                              width: 100,
                              decoration: pw.BoxDecoration(
                                  border: pw.Border.all(
                                    color: PdfColors.grey800,
                                  )
                              ),
                              child: pw.Text(
                                "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                style: pw.TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: pw.TextAlign.center,
                              )
                          ),
                        ]
                    ),


                  pw.Container(
                      margin: pw.EdgeInsets.only(top: 20),
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  child: pw.Text(
                                    "Total Number of Pick Up(s):    " + "${rentList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Number of Return(s):   " + "${returnList.length.toString()}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                            pw.Align(
                              alignment: pw.Alignment.bottomLeft,
                              child: pw.Container(
                                  margin: pw.EdgeInsets.only(top: 10),
                                  child: pw.Text(
                                    "Total Profit of the Stop (Return):  RM ${totalProfit.toStringAsFixed(2)}",
                                    style: pw.TextStyle(
                                      fontSize: 21,
                                      fontWeight: pw.FontWeight.bold,
                                    ),
                                    textAlign: pw.TextAlign.left,
                                  )
                              ),
                            ),

                          ]
                      )
                  )

                ];
              }
          )
      );
    }



  }

  Future savePdf() async{
    Directory documentDirectory = await getApplicationDocumentsDirectory();

    String documentPath = documentDirectory.path;

    print("Path:" + "$documentPath/stop_report_${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}.pdf");

    File file = File("$documentPath/stop_report_${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}.pdf");

    file.writeAsBytesSync(await pdf.save());
  }

}

