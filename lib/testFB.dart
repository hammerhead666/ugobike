import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide Feedback;
import 'package:provider/provider.dart';
import './dbLocator.dart';
import './core/viewmodels/profileCRUD.dart';
import './core/viewmodels/bicycleCRUD.dart';
import './core/viewmodels/rentingHistoryCRUD.dart';
import './core/viewmodels/bicycleStopCRUD.dart';
import './core/viewmodels/railCRUD.dart';
import './core/viewmodels/feedbackCRUD.dart';
import './core/models/bicycleModel.dart';
import './core/models/bicycleStopModel.dart';
import './core/models/feedbackModel.dart';
import './core/models/railModel.dart';
import './core/models/rentingHistoryModel.dart';
import './core/models/profileModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


void main() {
  //setupAccountLocator();
  //setupBicycleLocator();
  //setupBicycleStopLocator();
  //setupFeedbackLocator();
  //setupRailLocator();
  //setupRentingHistoryLocator();
  setupUserLocator();
  runApp(TryApp());
}

class TryApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(builder: (_) => locator<profileCRUDModel>(), create: (BuildContext context) {  },),
      ],
      child: MaterialApp(
        title: 'Flutter Realtime Database Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: FirebaseFirestoreDemoScreen(),
      ),
    );
  }
}


//dummy data
final accID = 'A123';
final accName = 'bobAcc';
final pass = 'bob0808';

final bicID = "B001";

final stID = 'S001';
final stName = 'N28a, School of Computing';
final lat = 12.34567895;
final long = 98.76543215;

final fb = "not bad";
final cat = "Review";
final prob = "ok";

final raID = "R001";
final stat = true;

final fee = 12.50;

final usID = "U001";
final email = "Mary@gmail.com";
final gender = "Female";
final name = "Mary Spear";
final phone = "0123456789";


class FirebaseFirestoreDemoScreen extends StatelessWidget {

  //final databaseReference = FirebaseDatabase.instance.reference();
  profileCRUDModel call = new profileCRUDModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Realtime Database Demo'),
      ),
      body: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[

                RaisedButton(
                  child: Text('Create Data'),
                  color: Colors.redAccent,
                  onPressed: () async{
                    final QuerySnapshot qSnap = await Firestore.instance.collection('User').getDocuments();
                    final int docCount = qSnap.documents.length + 1;

                    call.setUser(Profile(userName: name, userPhone: phone, userEmail: email, userGender: gender), "lala");

                  },
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                ),
                SizedBox(height: 8,),
                RaisedButton(
                  child: Text('Read/View Data'),
                  color: Colors.redAccent,

                  onPressed: () {
                    //fetchData();
                  },
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),

                ),
                SizedBox(height: 8,),

                RaisedButton(
                  child: Text('Update Data'),
                  color: Colors.redAccent,

                  onPressed: () {
                    call.updateUser(Profile(userEmail: "bob@test.com", userGender: "Male", userName: "Bob Marcus", userPhone: "0102256633"), "U001");
                  },
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),

                ),
                SizedBox(height: 8,),

                RaisedButton(
                  child: Text('Delete Data'),
                  color: Colors.redAccent,

                  onPressed: () {
                    call.removeUser("U001");
                  },
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),

                ),
              ],
            ),
          )
      ), //center
    );
  }

/*void fetchData(){
    Map testData;

    CollectionReference colRef = Firestore.instance.collection('User');
    colRef.snapshots().listen((snapshot){
      testData = snapshot.documents[0].data;
      print(testData);
    });
}*/

/*void getID() async{
    userCRUDModel call = new userCRUDModel();

    await Firestore.instance.collection('User').where(
        'accountID',
        isEqualTo: "A888"
    ).getDocuments().then((event) {
      if (event.documents.isNotEmpty) {
        print(FieldPath.documentId);
        //call.updateUser(User(accountID: "YESPLEASE"), FieldPath.documentId.toString());
        //DocumentSnapshot docSnap = event.documents.first;
        //var docID = docSnap.reference.documentID;
        //return FieldPath.documentId;
        //Map<String, dynamic> documentData = event.documents.single.data; //if it is a single document
      }
    });
  }*/



} //jangan sentuh

