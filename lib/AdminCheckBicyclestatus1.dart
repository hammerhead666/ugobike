import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:intl/intl.dart';

class AdminCheckBicyclestatus1 extends StatefulWidget {
  final String bicycleID;

  AdminCheckBicyclestatus1(this.bicycleID);

  @override
  bicyclestatus1 createState() => bicyclestatus1(bicycleID);
}

class bicyclestatus1 extends State<AdminCheckBicyclestatus1> {
  final String bicycleID;
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd h:m');

  bicyclestatus1(this.bicycleID);

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            height: 150,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),

            child: Stack(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 30, left: 20),
                  child: IconButton(
                      icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                      onPressed: () {
                        Navigator.pop(context);
                      }
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Image(
                    height: 200,
                    image: const AssetImage('assets/images/logo.png'),
                  ),
                ),
              ],
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 170),
            child: FutureBuilder<QuerySnapshot>(
              future: FirebaseFirestore.instance.collection('Rail').where("bicycleID", isEqualTo: bicycleID).get(),
              builder: (context, snapshot) {
                if (snapshot.data == null) return CircularProgressIndicator();

                if (snapshot.hasError) {
                  return Text("Something went wrong!");
                }

                else {
                  final List<DocumentSnapshot> documents = snapshot.data.docs;

                  return Stack(
                    children: documents.map((doc) =>
                      Container(
                        margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                        height: 105,
                        width: 500,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black45,
                            width: 1.5,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(7.0),
                          ),
                          color: Colors.white,
                        ),

                        child: Column(
                          children: <Widget> [
                            Container(
                              alignment: Alignment.topLeft,
                              margin: const EdgeInsets.only(top: 10, left: 10),
                              child: Text(
                                ("Bicycle ID        :  " + doc['bicycleID']),
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 18,
                                  color: const Color(0xff656d74),
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),

                            Container(
                              alignment: Alignment.topLeft,
                              margin: const EdgeInsets.only(top: 10, left: 10),
                              child: Text(
                                ("Current Stop   :  " + doc['stopID']),
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 18,
                                  color: const Color(0xff656d74),
                                  fontWeight: FontWeight.w700,
                                ),
                              )
                            ),

                            Container(
                              alignment: Alignment.topLeft,
                              margin: const EdgeInsets.only(top: 10, left: 10),
                              child: Text(
                                ("Current Rail     :  " + doc['railID']),
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 18,
                                  color: const Color(0xff656d74),
                                  fontWeight: FontWeight.w700,
                                ),
                              )
                            ),
                          ]
                        )
                      )
                    ).toList());
                }
              }
            ),
          ),

          Container(
            child: Column(
              children: <Widget> [
                Container(
                  margin: const EdgeInsets.only(top: 300),
                  child: Text(
                    'Rented History',
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 20,
                      color: const Color(0xff656d74),
                      fontWeight: FontWeight.w700,
                      height: 1.5,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),

                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    child: FutureBuilder<QuerySnapshot>(
                      future: FirebaseFirestore.instance.collection('RentingHistory').where("bicycleID", isEqualTo: bicycleID).get(),
                      builder: (context, snapshot) {
                        if (snapshot.data == null) return CircularProgressIndicator();

                        if (snapshot.hasError) {
                          return Text("Something went wrong!");
                        }

                        else {
                          final List<DocumentSnapshot> documents = snapshot.data.docs;

                          if(documents.isEmpty){
                            return Align(
                              alignment: Alignment.center,
                              child: Image(
                                height: 200,
                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                              ),
                            );
                          }

                          else{
                            return Stack(
                              children: <Widget> [
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.black45,
                                      width: 1.5,
                                    ),
                                    color: Colors.white,
                                  ),
                                  child: Table(
                                      columnWidths: {
                                        0: FlexColumnWidth(5),
                                        1: FlexColumnWidth(4),
                                      },
                                    border: TableBorder(
                                      verticalInside: BorderSide(
                                        color: Colors.black45,
                                        width: 1.5,
                                      ),
                                    ),
                                    children: [
                                      TableRow(
                                        children: [
                                          TableCell(
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: const Color(0xffffffff),
                                                ),
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(7.0)
                                                ),
                                                color: Colors.white,
                                              ),
                                              //color: Colors.white,
                                              padding: EdgeInsets.only(top: 5, bottom: 5),
                                              child: Center(
                                                child: Text(
                                                  'Date Rented',
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 15,
                                                    color: const Color(0xff656d74),
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          TableCell(
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: const Color(0xffffffff),
                                                ),
                                                borderRadius: BorderRadius.only(
                                                    topRight: Radius.circular(7.0)
                                                ),
                                                color: Colors.white,
                                              ),
                                              padding: EdgeInsets.only(top: 5, bottom: 5),
                                              child: Center(
                                                child: Text(
                                                  'Total Time Rented',
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 15,
                                                    color: const Color(0xff656d74),
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ]
                                      ),
                                    ]
                                  ),
                                ),

                                Container(
                                  margin: EdgeInsets.only(top: 8),
                                  child: ListView(
                                    children: documents.map((doc) =>
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border(
                                            top: BorderSide(width: 0.75, color: Colors.black45),
                                            left: BorderSide(width: 1.5, color: Colors.black45),
                                            right: BorderSide(width: 1.5, color: Colors.black45),
                                            bottom: BorderSide(width: 0.75, color: Colors.black45),
                                          ),
                                          color: Colors.white,
                                        ),
                                        child: Table(
                                          columnWidths: {
                                            0: FlexColumnWidth(5),
                                            1: FlexColumnWidth(4),
                                          },
                                          border: TableBorder(
                                            verticalInside: BorderSide(
                                              color: Colors.black45,
                                              width: 1.5,
                                            ),
                                            horizontalInside: BorderSide(
                                              color: Colors.black45,
                                              width: 1.5,
                                            ),
                                          ),
                                          children: [
                                            TableRow(
                                                children: [
                                                  TableCell(
                                                    child: Container(
                                                      padding: EdgeInsets.only(top: 5, bottom: 5),
                                                      child: Center(
                                                        child: Text(
                                                          dateFormat.format(doc['dateRented'].toDate()).toString(),
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 14,
                                                            color: const Color(0xff656d74),
                                                            fontWeight: FontWeight.w700,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  TableCell(
                                                    child: Container(
                                                      padding: EdgeInsets.only(top: 5, bottom: 5),
                                                      child: Center(
                                                        child: Text(
                                                          (doc['totalMin'].toInt().toString() + " min " + ((doc['totalMin'] * 60) - (doc['totalMin'].toInt() * 60 )).toInt().toString() + " sec"),
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontSize: 14,
                                                            color: const Color(0xff656d74),
                                                            fontWeight: FontWeight.w700,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ]
                                            ),
                                          ]
                                        ),
                                      ),
                                    )
                                  .toList()),
                                ),
                              ]
                            );
                          }
                        }
                      }
                    ),
                  ),
                ),
              ]
            ),
          ),
        ]
      ),
    );
  }
}
