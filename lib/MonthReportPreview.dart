import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/flutter_full_pdf_viewer.dart';
import 'package:share/share.dart';
import './AdminGenerateReportByMonth.dart';

class MonthReportPreview extends StatefulWidget{
  String path;

  MonthReportPreview(String screen){
    path = screen;
  }

  MonthReportPreviewState createState(){
    return MonthReportPreviewState(path);
  }
}

class MonthReportPreviewState extends State<MonthReportPreview> {
  String path;

  MonthReportPreviewState(String screen){
    path = screen;
  }

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          backgroundColor: Color(0xff656d74),
          elevation: 15,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.close),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AdminGenerateReportByMonthPage()));
                },
              );
            },
          ),

          title: Text("Report Preview"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.share),
              onPressed: () async {
                Share.shareFiles(['$path'], text: 'Attached is the generated report.');
              },
            ),
          ],
        ),
      ),
      path: path,
    );
  }

}