import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import './LogIn.dart';
import './AdminReadFeedback.dart';
import './RegisterNewBicycle.dart';
import './AdminCheckRentingStopStatus.dart';
import './AdminGenerateReport.dart';
import './AdminCheckBicyclestatus.dart';

class AdminLandingPage extends StatelessWidget {
  AdminLandingPage({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 40),
            height: 145,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),
            child: Align(
              alignment: Alignment.center,
              child: Image(
                height: 300,
                image: FirebaseImage(
                    'gs://ugobike-d3e33.appspot.com/logo_word.png'),
              ),
            ),
          ),

          Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute<void>(
                        builder: (BuildContext context) => AdminFeedbackPage(),
                        fullscreenDialog: true,
                      ),
                    );
                  },
                  child: Container(
                    margin: const EdgeInsets.only(left: 30, right: 15),
                    height: 130,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      color: const Color(0xffffffff),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x14000000),
                          offset: Offset(5, 5),
                          blurRadius: 10,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(top: 3),
                          child: Image(
                            height: 80,
                            image: FirebaseImage(
                                'gs://ugobike-d3e33.appspot.com/admin_feedback.png'),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 0, left: 2, right: 2, bottom: 5),
                          child: Text(
                            'User Feedback\n& Issue',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 15,
                              color: const Color(0xff656d74),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              Expanded(
                flex: 5,
                child: GestureDetector(
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => AdminCheckRentingStopStatus())),
                  child: Container(
                    margin: const EdgeInsets.only(left: 15, right: 30),
                    height: 130,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      color: const Color(0xffffffff),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x14000000),
                          offset: Offset(5, 5),
                          blurRadius: 10,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(top: 3),
                          child: Image(
                            height: 80,
                            image: FirebaseImage('gs://ugobike-d3e33.appspot.com/admin_stop.png'),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 0, left: 2, right: 2, bottom: 5),
                          child: Text(
                            'Renting\nStop',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 15,
                              color: const Color(0xff656d74),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: GestureDetector(
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => AdminCheckBicyclestatus())),
                  child: Container(
                    margin: const EdgeInsets.only(left: 30, right: 15, top: 20),
                    height: 130,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      color: const Color(0xffffffff),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x14000000),
                          offset: Offset(5, 5),
                          blurRadius: 10,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(top: 3),
                          child: Image(
                            height: 80,
                            image: FirebaseImage(
                                'gs://ugobike-d3e33.appspot.com/admin_bike.png'),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              top: 0, left: 2, right: 2, bottom: 5),
                          child: Text(
                            'Bicycle\nStatus',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 15,
                              color: const Color(0xff656d74),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              Expanded(
                flex: 5,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute<void>(
                        builder: (BuildContext context) => AdminGenerateReportPage(),
                        fullscreenDialog: true,
                      ),
                    );
                  },
                  child: Container(
                    margin: const EdgeInsets.only(left: 15, right: 30, top: 20),
                    height: 130,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      color: const Color(0xffffffff),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x14000000),
                          offset: Offset(5, 5),
                          blurRadius: 10,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(top: 3),
                          child: Image(
                            height: 80,
                            image: FirebaseImage(
                                'gs://ugobike-d3e33.appspot.com/admin_repot.png'),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              top: 0, left: 2, right: 2, bottom: 5),
                          child: Text(
                            'Generate\nReport',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 15,
                              color: const Color(0xff656d74),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),

          Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute<void>(
                        builder: (BuildContext context) => RegisterNewBicycle(),
                        fullscreenDialog: true,
                      ),
                    );
                  },
                  child: Container(
                    margin: const EdgeInsets.only(left: 30, right: 15, top: 20),
                    height: 130,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      color: const Color(0xffffffff),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x14000000),
                          offset: Offset(5, 5),
                          blurRadius: 10,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(top: 3),
                          child: Image(
                            height: 80,
                            image: FirebaseImage(
                                'gs://ugobike-d3e33.appspot.com/admin_bike.png'),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              top: 0, left: 2, right: 2, bottom: 5),
                          child: Text(
                            'Add New\nBicycle',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 15,
                              color: const Color(0xff656d74),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: Container(),
              )
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 30.0),
            child: Padding(
              padding: EdgeInsets.only(right: 40, left: 40),
              child: Center(
                child: ButtonTheme(
                  minWidth: 150.0,
                  height: 40.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      //side: BorderSide(color: Colors.red)
                    ),
                    color: const Color(0xff656d74),
                    onPressed: () {
                      _signOut();
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                    child: Text('Sign out',
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 15,
                            color: const Color(0xffffffff),
                            fontWeight: FontWeight.w700)),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _signOut() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();

    await FirebaseAuth.instance.signOut();
    await googleSignIn.signOut();
  }
}
