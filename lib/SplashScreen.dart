import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import './LogIn.dart';
import './BottomBar.dart';
import './AdminLandingPage.dart';

class SplashScreen extends StatefulWidget  {
  SplashScreen({
    Key key,
  }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  String _versionName = 'V1.0';
  final splashDelay = 5;
  String curState;

  @override
  void initState() {
    super.initState();

    getCurrentUser();

    if(curState.compareTo("search") == 0){
      _loadScreen();
    }

    else if(curState.compareTo("login") == 0){
      _loadLogin();
    }

    else if(curState.compareTo("admin") == 0){
      _loadAdmin();
    }
  }

Future <void> getCurrentUser() async {
    final User curUser = FirebaseAuth.instance.currentUser;

    if(curUser != null){

      if(curUser.uid.compareTo("wwA9F0mkKKSqta7XXgkmkd2qCpx2") == 0){
        print("Admin found");
        setState(() => curState = "admin");
      }
      else{
        print("Current user found");
        setState(() => curState = "search");
      }
    }

    else{
      print("No current user");
      setState(() => curState = "login");
    }
  }

  _loadLogin() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
  }

  _loadScreen() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, homePage);
  }

  void homePage() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => BottomBar(0)));
  }

  _loadAdmin() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, adminPage);
  }

  void adminPage() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => AdminLandingPage()));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
          children: <Widget>[
        Pinned.fromSize(
          bounds: Rect.fromLTWH(0.0, 0.0, 385.0, 410.0),
          size: Size(375.0, 812.0),
          pinLeft: true,
          pinRight: true,
          pinTop: true,
          fixedHeight: true,
            child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x1f000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),
          ),
      ),
        Pinned.fromSize(
          bounds: Rect.fromLTWH(-115.0, -173.0, 310.0, 330.0),
          size: Size(375.0, 812.0),
          pinLeft: true,
          pinTop: true,
          fixedWidth: true,
          fixedHeight: true,
            child: Container(
            decoration: BoxDecoration(
            borderRadius:
            BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
            color: const Color(0xffbabdbf),
              ),
            ),
          ),
        Pinned.fromSize(
            bounds: Rect.fromLTWH(250.0, 221.0, 310.0, 310.0),
            size: Size(375.0, 812.0),
            pinRight: true,
            fixedWidth: true,
            fixedHeight: true,
          child: Container(
          decoration: BoxDecoration(
          borderRadius:
          BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
          color: const Color(0xffbfa780),
          ),
          ),
        ),

          Pinned.fromSize(
            bounds: Rect.fromLTWH(61.7, 82.3, 251.6, 200.7),
            size: Size(375.0, 812.0),
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child: Stack(
              children: <Widget>[
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(0.0, 0.0, 251.6, 188.7),
                  size: Size(251.6, 188.7),
                  pinLeft: true,
                  pinRight: true,
                  pinTop: true,
                  pinBottom: true,
                  child: Stack(
                    children: <Widget>[
                      Pinned.fromSize(
                        bounds: Rect.fromLTWH(0.0, 0.0, 251.6, 180.7),
                        size: Size(251.6, 188.7),
                        pinLeft: true,
                        pinRight: true,
                        pinTop: true,
                        pinBottom: true,
                        child:
                        // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(70.0),
                            image: DecorationImage(
                              image: const AssetImage('assets/images/logo.png'),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Pinned.fromSize(
                  bounds: Rect.fromLTWH(75.0, 125.9, 135.0, 50.0),
                  size: Size(251.6, 188.7),
                  fixedWidth: true,
                  fixedHeight: true,
                  child: Text(
                    'UGoBike',
                    style: TextStyle(
                      fontFamily: 'Segoe UI',
                      fontSize: 25,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(300.0, 395.0, 242.0, 400.0),
            size: Size(475.0, 875.0),
            pinRight: true,
            fixedWidth: true,
            fixedHeight: true,
            child:
            // Adobe XD layer: '200-2005970_fritz-h…' (shape)
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage('assets/images/bicyclemain.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
