//later put for validate user got successfully rent a bike or not then prompt to timer.dart page
import 'dart:async';
import 'package:UGoBike/Unlock.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:ui';
import 'Renting.dart';

class ValidateRail extends StatelessWidget {
  final String railCode;

  final RailInfo = FirebaseFirestore.instance;

  String rentedBikeID;
  bool cont = true;

  ValidateRail(this.railCode);

  Future<void> updateData() async {
    try {
      RailInfo.collection('Rail').doc(railCode).update({'bicycleID': ""});
      RailInfo.collection('Rail').doc(railCode).update({'status': false});
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTextStyle(
        style: Theme.of(context).textTheme.headline6,
        textAlign: TextAlign.center,
        child: Container(
          alignment: FractionalOffset.center,
          color: Colors.white,
          child: StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance.collection('Rail').where("railID", isEqualTo: railCode).snapshots(),
            builder:(BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              List<Widget> children;
              if (!snapshot.hasData) {
                children = <Widget>[CircularProgressIndicator()];
              }

              snapshot.data.docs.map((document) {
                if (document['bicycleID'] != "") {
                  rentedBikeID = (document['bicycleID']);

                  print("Successfully" + rentedBikeID);

                  children = <Widget>[
                    Icon(
                      Icons.check_circle_outline,
                      color: Colors.green,
                      size: 100,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Text('Unlock Bike $rentedBikeID Successfully'),
                    )
                  ];

                  Future.delayed(const Duration(seconds: 2), () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            Renting(rentedBikeID, railCode)));
                  });

                  Future.delayed(const Duration(seconds: 7), () {
                    if (cont){
                      updateData();
                      cont = false;
                    }
                  });
                }

                else if (document['bicycleID'] == "") {
                  children = <Widget>[
                    Icon(
                      Icons.error_outline,
                      color: Colors.red,
                      size: 100,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Text('Fail to Unlock, Invalid Rail.'),
                    ),
                    FlatButton(
                        color: const Color(0xffbfa780),
                        onPressed: () async {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => Unlock()));
                        },
                        child: Text(
                          'Back',
                          style: new TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 12,
                            color: const Color(0xffffffff),
                            fontWeight: FontWeight.w700,
                            height: 2.5,
                          ),
                          textAlign: TextAlign.center,
                        ))
                  ];
                }
              }).toList();
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: children,
              );
            },
          ),
        ),
      ),
    );
  }
}
