import 'package:UGoBike/AdminLandingPage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class RegisterNewBicycle extends StatefulWidget {
  @override
  newBicycle createState() => newBicycle();
}

class newBicycle extends State<RegisterNewBicycle> {

  CollectionReference bicycle = FirebaseFirestore.instance.collection('Bicycle');
  CollectionReference rail = FirebaseFirestore.instance.collection('Rail');
  final DateFormat dF = DateFormat('dd-MM-yyyy');
  int docCount;
  String railToInsertBike;
  String bikeID;
  TextEditingController railCode = TextEditingController();

  addBicycle(BuildContext context) {
    if (docCount < 10)
    {
      bicycle.add({
        'bicycleID': 'B00$docCount',
        'dateRegister': Timestamp.now(),
      }).catchError((error) => print("Failed to add feedback: $error"));

      bikeID = 'B00$docCount';
    }

    else if (docCount >= 10 || docCount < 100)
    {
      bicycle.add({
        'bicycleID': 'B0$docCount',
        'dateRegister': Timestamp.now(),
      }).catchError((error) => print("Failed to add feedback: $error"));

      bikeID = 'B0$docCount';
    }

    else
    {
      bicycle.add({
        'bicycleID': 'B$docCount',
        'dateRegister': Timestamp.now(),
      }).catchError((error) => print("Failed to add feedback: $error"));

      bikeID = 'B$docCount';
    }

    AlertDialog alert = AlertDialog(
      backgroundColor: const Color(0xfffafdff),
      title: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Image.asset(
              'assets/images/pedalBike.png',
              height: 60,
              width: 60,
            ),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text('Bicycle Successfully\nAdded',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    color: const Color(0xff656d74),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ],
      ),

      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 5, left: 8, right: 8),
            height: 45,
            width: 500,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black45,
                width: 1.5,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(5.0),
              ),
              color: const Color(0xfff8f8f8),
            ),
            child: Center(
              child: Text(
                bikeID,
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  color: const Color(0xa6656d74),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 10, left: 8, right: 8),
            height: 45,
            width: 500,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black45,
                width: 1.5,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(5.0),
              ),
              color: const Color(0xfff8f8f8),
            ),
            child: Center(
              child: Text(
                dF.format(DateTime.now()).toString(),
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  color: const Color(0xa6656d74),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),

          Row(
            children: <Widget>[
              InkWell(
                child: Container(
                  margin: const EdgeInsets.only(top: 20, right: 40),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    color: const Color(0xffbfa780),
                  ),

                  padding: const EdgeInsets.fromLTRB(10,10,10,10),
                  child: Text("Add More Bicycle",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 13,
                      color: const Color(0xffffffff),
                    ),
                  ),
                ),

                onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => RegisterNewBicycle())),
              ),

              InkWell(
                child: Container(
                  margin: const EdgeInsets.only(top: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    color: const Color(0xff707070),
                  ),

                  padding: const EdgeInsets.fromLTRB(10,10,10,10),
                  child: Text("Finish",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 13,
                      color: const Color(0xffffffff),
                    ),
                  ),
                ),

                onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => AdminLandingPage())),
              ),
            ],
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  updateRail(){
    rail.doc(railToInsertBike).update({'bicycleID': "$bikeID"});
    rail.doc(railToInsertBike).update({'status': true});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xfffafdff),
      body: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              height: 150,
              width: 500,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50.0),
                  bottomLeft: Radius.circular(50.0),
                ),
                color: const Color(0xff656d74),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x26000000),
                    offset: Offset(0, 15),
                    blurRadius: 30,
                  ),
                ],
              ),

              child: Stack(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(top: 30, left: 20),
                    child: IconButton(
                      icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                      onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => AdminLandingPage())),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Image(
                      height: 200,
                      image: const AssetImage('assets/images/logo.png'),
                    ),
                  ),
                ],
              ),
            ),

            Container(
                margin: const EdgeInsets.only(top: 15),
                alignment: Alignment.center,
                child: Text(
                  "Register New Bicycle",
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 25,
                    color: const Color(0xff656d74),
                    fontWeight: FontWeight.w700,
                    height: 1.2,
                  ),
                )
            ),

            Container(
              margin: const EdgeInsets.only(top: 70, left: 25),
              alignment: Alignment.topLeft,
              child: Text(
                "Rail to Insert Bicycle",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  color: const Color(0xff656d74),
                  fontWeight: FontWeight.w700,
                  height: 1.2,
                ),
              ),
            ),

            Container(
              margin: const EdgeInsets.only(top: 5, left: 25, right: 25),
              height: 45,
              width: 500,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black45,
                  width: 1.5,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
              ),
              child: TextFormField(
                controller: railCode,
                maxLines: 1,
                decoration: InputDecoration(
                  hintText: "Insert Rail ID",
                  hintStyle: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    color: const Color(0xa6656d74),
                  ),
                  fillColor: const Color(0xffffffff),
                  filled: true,
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),

                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  color: const Color(0xff656d74),
                ),
                textAlign: TextAlign.left,
              ),
            ),

            Container(
                margin: const EdgeInsets.only(top: 40, left: 25),
                alignment: Alignment.topLeft,
                child: Text(
                  "Bicycle ID",
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    color: const Color(0xff656d74),
                    fontWeight: FontWeight.w700,
                    height: 1.2,
                  ),
                )
            ),

            Container(
              margin: const EdgeInsets.only(top: 5, left: 25, right: 25),
              height: 45,
              width: 500,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black45,
                  width: 1.5,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
                color: const Color(0xfff8f8f8),
              ),
              child: Center(
                child: FutureBuilder<QuerySnapshot>(
                    future: FirebaseFirestore.instance.collection('Bicycle').get(),
                    builder: (context, snapshot){
                      if(snapshot.data == null) return CircularProgressIndicator();

                      if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      else{
                        docCount = snapshot.data.size + 1;

                        if (docCount < 10)
                        {
                          return Text(
                            "B00$docCount",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 20,
                              color: const Color(0xa6656d74),
                            ),
                            textAlign: TextAlign.center,
                          );
                        }

                        else if (docCount >= 10 || docCount < 100)
                        {
                          return Text(
                            "B0$docCount",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 20,
                              color: const Color(0xa6656d74),
                            ),
                            textAlign: TextAlign.center,
                          );
                        }

                        else
                        {
                          return Text(
                            "B$docCount",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 20,
                              color: const Color(0xa6656d74),
                            ),
                            textAlign: TextAlign.center,
                          );
                        }
                      }
                    }
                ),
              ),
            ),

            Container(
              margin: const EdgeInsets.only(top: 40, left: 25),
              alignment: Alignment.topLeft,
              child: Text(
                "Date Register",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  color: const Color(0xff656d74),
                  fontWeight: FontWeight.w700,
                  height: 1.2,
                ),
              ),
            ),

            Container(
              margin: const EdgeInsets.only(top: 5, left: 25, right: 25),
              height: 45,
              width: 500,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black45,
                  width: 1.5,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
                color: const Color(0xfff8f8f8),
              ),
              child: Center(
                child: Text(
                  dF.format(DateTime.now()).toString(),
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    color: const Color(0xa6656d74),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),

            Container(
              margin: const EdgeInsets.only(top: 70),
              child: Row(
                  children: <Widget> [
                    InkWell(
                      onTap: () async{
                        bool check = await checkRail(railCode.text);

                        if(check){
                          railToInsertBike = railCode.text;
                          addBicycle(context);
                          updateRail();
                        }

                        else{
                          showDialog(
                              context: context,
                              child: new AlertDialog(
                                backgroundColor: const Color(0xfffafdff),
                                content: const Text("Invalid rail. Please try again.",
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 20,
                                    color: const Color(0xff656d74),
                                  ),
                                ),

                                actions: [
                                  ButtonTheme(
                                    minWidth: 80.0,
                                    height: 35.0,
                                    child: new FlatButton(
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(4.0),
                                        ),
                                        color: const Color(0xff707070),
                                        onPressed: (){
                                          Navigator.of(context, rootNavigator: true).pop('dialog');
                                        },
                                        child: const Text("Ok",
                                            style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontSize: 13,
                                              color: const Color(0xffffffff),
                                            ),
                                            textAlign: TextAlign.center)
                                    ),
                                  )
                                ],
                              )
                          );
                        }



                      },
                      child: Container(
                        margin: const EdgeInsets.only(left: 60),
                        //alignment: Alignment.topLeft,
                        height: 40,
                        width: 90,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          color: const Color(0xffbfa780),
                        ),
                        child: Center(
                          child: Text(
                            'CONFIRM',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 13,
                              color: const Color(0xffffffff),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),

                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        //alignment: Alignment.topRight,
                        margin: const EdgeInsets.only(left: 55),
                        height: 40,
                        width: 90,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          color: const Color(0xff707070),
                        ),
                        child: Center(
                          child: Text(
                            'CANCEL',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 13,
                              color: const Color(0xffffffff),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ]
              ),
            ),
          ]
      ),
    );
  }

  static Future<bool> checkRail(String docID) async{

    bool check = false;

    await FirebaseFirestore.instance.doc("Rail/$docID").get().then((doc){
      if (doc.exists){
        if (doc['status'].toString() == "false"){
          check = true;
        }
      }
    });

    return check;
  }

}