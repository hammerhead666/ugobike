import 'dart:async';
import 'package:UGoBike/validateReturn.dart';
import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'dart:ui';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Renting extends StatefulWidget {
  final String RentedBikeCode;
  final String railCode;

  Renting(this.RentedBikeCode, this.railCode);

  RentingState createState() => RentingState(this.RentedBikeCode, this.railCode);
}

class RentingState extends State<Renting> {
  GoogleMapController _controller;
  BitmapDescriptor currentLocIcon, stopLocIcon;
  final String RentedBikeCode;
  final String railCode;

  RentingState(this.RentedBikeCode, this.railCode);

  void initState() {

    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(25, 25)), 'assets/images/stoplocation.png')
        .then((onValue) {
      stopLocIcon = onValue;
    });

    super.initState();
    startTimer();
  }

  Timer _timer;
  int seconds = 0;
  int minutes = 0;
  int hours = 0;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (seconds < 0) {
            timer.cancel();
          } else {
            seconds = seconds + 1;
            if (seconds > 59) {
              minutes += 1;
              seconds = 0;
              if (minutes > 59) {
                hours += 1;
                minutes = 0;
              }
            }
          }
        },
      ),
    );
  }

  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  final _formKey = GlobalKey<FormState>();

  String _scanBarcode = 'Unknown';

  TextEditingController RailCodeController = TextEditingController();

  Future confirmEnd() async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)),
          title: Text(
            'Scan Rail QR code',
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 18,
              color: const Color(0xff656d74),
            ),
            textAlign: TextAlign.center,
          ),
          content: Text(
            'Please scan the QR code on the rail that you have pushed in the bicycle.',
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 13,
              color: const Color(0xff656d74),
            ),
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            new RaisedButton(
                onPressed: () async {
                  Navigator.of(context).pop();
                  scanQR();
                },
                color: const Color(0xffbfa780),
                child: new Text(
                  'Scan QR',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 12,
                    color: const Color(0xffffffff),
                  ),
                  textAlign: TextAlign.center,
                )),
            new RaisedButton(
              onPressed: () async {
                Navigator.of(context).pop();
                manualInsert();
              },
              color: const Color(0xff656d74),
              child: new Text(
                'Manual Input',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 12,
                  color: const Color(0xffffffff),
                ),
                textAlign: TextAlign.center,
              ),
            )
          ],
        );
      }
    );
  }

  Future scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });

    _timer.cancel();

    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>
            validateReturn(_scanBarcode, RentedBikeCode, seconds, minutes, hours, railCode)));
  }

  Future manualInsert() async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)),
          title: Text(
            'Insert Rail Number',
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 18,
              color: const Color(0xff656d74),
            ),
            textAlign: TextAlign.center,
          ),
          content: SingleChildScrollView(
              child: new Form(
            key: _formKey,
            child: new Column(
              children: <Widget>[
                new TextFormField(
                  controller: RailCodeController,
                  decoration: InputDecoration(
                    hintText: 'Rxxx',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Enter the Rail Number';
                    }
                    return null;
                  },
                ),
              ],
            ),
          )),
          actions: [
            new RaisedButton(
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  _timer.cancel();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => validateReturn(
                          RailCodeController.text,
                          RentedBikeCode,
                          seconds, minutes, hours, railCode)));
                }
              },
              color: const Color(0xffbfa780),
              child: new Text(
                'NEXT',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 12,
                  color: const Color(0xffffffff),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color(0xfffafdff),
        body: Stack(
          children: <Widget>[
            Pinned.fromSize(
              bounds: Rect.fromLTWH(-84.0, 27.0, 492.0, 1000.0),
              size: Size(375.0, 812.0),
              pinLeft: true,
              pinRight: true,
              pinTop: true,
              pinBottom: true,
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 1000.0, 1000.0),
                    size: Size(1000.0, 1000.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 1000.0, 1000.0),
                          size: Size(1000.0, 1000.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: Stack(
                            children: <Widget>[
                              Pinned.fromSize(
                                bounds: Rect.fromLTWH(0.0, 0.0, 1000.0, 1000.0),
                                size: Size(1000.0, 1000.0),
                                pinLeft: true,
                                pinRight: true,
                                pinTop: true,
                                pinBottom: true,
                                child:
                                // Adobe XD layer: '0' (shape)
                                Container(
                                  //height: MediaQuery.of(context).size.height,
                                  //width: MediaQuery.of(context).size.width,
                                  child: Stack(
                                    children: [
                                      GoogleMap(
                                          mapType: MapType.normal,
                                          markers: _createMarker(),
                                          initialCameraPosition: CameraPosition(
                                            target: LatLng(1.566858, 103.634609),
                                            zoom: 16.5,
                                          ),
                                          onMapCreated: (GoogleMapController controller) {
                                            _controller = controller;
                                          }
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Pinned.fromSize(
              bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 163.0),
              size: Size(375.0, 812.0),
              pinLeft: true,
              pinRight: true,
              pinTop: true,
              fixedHeight: true,
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 146.0),
                    size: Size(375.0, 163.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(50.0),
                          bottomLeft: Radius.circular(50.0),
                        ),
                        color: const Color(0xff656d74),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x26000000),
                            offset: Offset(0, 15),
                            blurRadius: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(89.0, 14.0, 198.0, 149.0),
                    size: Size(375.0, 163.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child:
                    // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(70.0),
                        image: DecorationImage(
                          image: const AssetImage('assets/images/logo.png'),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 200),
              height: 150,
              width: 500,
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget> [
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget> [
                      SvgPicture.string(
                        _svg_q9zp8b,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                      SizedBox(width: 15),
                      Text(
                        '$hours : $minutes : $seconds',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 25,
                          color: const Color(0xff656d74),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  InkWell(
                    onTap: () async {
                      return showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                            title: Text(
                              'Confirm to end trip?',
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 18,
                                color: const Color(0xff656d74),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            content: Text(
                              'Please make sure that you have push the bicycle into an empty rail.',
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 13,
                                color: const Color(0xff656d74),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            actions: <Widget>[
                              new RaisedButton(
                                onPressed: () async {
                                  Navigator.of(context).pop();
                                  confirmEnd();
                                },
                                color: const Color(0xffbfa780),
                                child: new Text(
                                  'CONFIRM',
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12,
                                    color: const Color(0xffffffff),
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              new RaisedButton(
                                onPressed: () async {
                                  Navigator.of(context).pop();
                                },
                                color: const Color(0xff656d74),
                                child: new Text(
                                  'CANCEL',
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12,
                                    color: const Color(0xffffffff),
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Container(
                      width: 150,
                      height: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                        color: const Color(0xffbaa47f),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget> [
                          SvgPicture.string(
                            _svg_922pxo,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(width: 15),
                          Text(
                            'End Trip',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 20,
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.center,
                          )
                        ]
                      ),
                    ),
                  ),
                ]
              ),
            ),
          ],
        ),
      ),
    );
  }

  Set<Marker> _createMarker() {
    MarkerId markerId2 = MarkerId("S001");
    MarkerId markerId3 = MarkerId("S002");
    MarkerId markerId4 = MarkerId("S003");
    MarkerId markerId5 = MarkerId("S004");
    MarkerId markerId6 = MarkerId("S005");
    MarkerId markerId7 = MarkerId("S006");
    MarkerId markerId8 = MarkerId("S007");
    MarkerId markerId9 = MarkerId("S008");
    MarkerId markerId10 = MarkerId("S009");
    MarkerId markerId11 = MarkerId("S010");
    MarkerId markerId12 = MarkerId("S011");
    MarkerId markerId13 = MarkerId("S012");
    MarkerId markerId14 = MarkerId("S013");
    MarkerId markerId15 = MarkerId("S014");
    MarkerId markerId16 = MarkerId("S015");
    MarkerId markerId17 = MarkerId("S016");
    MarkerId markerId18 = MarkerId("S017");
    MarkerId markerId19 = MarkerId("S018");

    Marker marker2 = Marker(
      markerId: markerId2,
      position: LatLng(1.566858, 103.634609),
      icon: stopLocIcon,
      consumeTapEvents: true,
    );

    Marker marker3 = Marker(
      markerId: markerId3,
      position: LatLng(1.564163, 103.635089),
      icon: stopLocIcon,
    );

    Marker marker4 = Marker(
      markerId: markerId4,
      position: LatLng(1.562851, 103.631900),
      icon: stopLocIcon,
    );

    Marker marker5 = Marker(
      markerId: markerId5,
      position: LatLng(1.561551, 103.632121),
      icon: stopLocIcon,
    );

    Marker marker6 = Marker(
      markerId: markerId6,
      position: LatLng(1.562327, 103.628742),
      icon: stopLocIcon,
    );

    Marker marker7 = Marker(
      markerId: markerId7,
      position: LatLng(1.567547, 103.624617),
      icon: stopLocIcon,
    );

    Marker marker8 = Marker(
      markerId: markerId8,
      position: LatLng(1.559979, 103.631364),
      icon: stopLocIcon,
    );

    Marker marker9 = Marker(
      markerId: markerId9,
      position: LatLng(1.561338, 103.634795),
      icon: stopLocIcon,
    );

    Marker marker10 = Marker(
      markerId: markerId10,
      position: LatLng(1.559746, 103.633785),
      icon: stopLocIcon,
    );

    Marker marker11 = Marker(
      markerId: markerId11,
      position: LatLng(1.557973, 103.632795),
      icon: stopLocIcon,
    );

    Marker marker12 = Marker(
      markerId: markerId12,
      position: LatLng(1.564535, 103.637235),
      icon: stopLocIcon,
    );

    Marker marker13 = Marker(
      markerId: markerId13,
      position: LatLng(1.559520, 103.636724),
      icon: stopLocIcon,
    );

    Marker marker14 = Marker(
      markerId: markerId14,
      position: LatLng(1.559338, 103.641289),
      icon: stopLocIcon,
    );

    Marker marker15 = Marker(
      markerId: markerId15,
      position: LatLng(1.554764, 103.644877),
      icon: stopLocIcon,
    );

    Marker marker16 = Marker(
      markerId: markerId16,
      position: LatLng(1.559486, 103.648654),
      icon: stopLocIcon,
    );

    Marker marker17 = Marker(
      markerId: markerId17,
      position: LatLng(1.556679, 103.656014),
      icon: stopLocIcon,
    );

    Marker marker18 = Marker(
      markerId: markerId18,
      position: LatLng(1.557619, 103.648285),
      icon: stopLocIcon,
    );

    Marker marker19 = Marker(
      markerId: markerId19,
      position: LatLng(1.563413, 103.651825),
      icon: stopLocIcon,
    );

    Map<MarkerId,Marker> markers = {};

    markers[markerId2]=marker2;
    markers[markerId3]=marker3;
    markers[markerId4]=marker4;
    markers[markerId5]=marker5;
    markers[markerId6]=marker6;
    markers[markerId7]=marker7;
    markers[markerId8]=marker8;
    markers[markerId9]=marker9;
    markers[markerId10]=marker10;
    markers[markerId11]=marker11;
    markers[markerId12]=marker12;
    markers[markerId13]=marker13;
    markers[markerId14]=marker14;
    markers[markerId15]=marker15;
    markers[markerId16]=marker16;
    markers[markerId17]=marker17;
    markers[markerId18]=marker18;
    markers[markerId19]=marker19;

    return Set.of(markers.values);
  }
}

const String _svg_922pxo =
    '<svg viewBox="-34.0 4.5 18.0 20.4" ><path transform="translate(-39.0, 0.49)" d="M 13.83199882507324 6.400000095367432 L 13.92799758911133 6.868000507354736 L 14.31199836730957 8.800000190734863 L 20.59999847412109 8.800000190734863 L 20.59999847412109 16.00000190734863 L 16.5679988861084 16.00000190734863 L 16.47200012207031 15.53200054168701 L 16.0880012512207 13.60000038146973 L 7.40000057220459 13.60000038146973 L 7.40000057220459 6.400000095367432 L 13.83200073242188 6.400000095367432 M 15.80000114440918 4 L 5.000000476837158 4 L 5.000000476837158 24.39999961853027 L 7.40000057220459 24.39999961853027 L 7.40000057220459 16.00000190734863 L 14.1200008392334 16.00000190734863 L 14.60000038146973 18.39999961853027 L 23 18.39999961853027 L 23 6.400000095367432 L 16.28000068664551 6.400000095367432 L 15.80000114440918 4 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_q9zp8b =
    '<svg viewBox="0.1 1.0 40.0 43.0" ><path transform="translate(-3.0, 0.0)" d="M 29.73666954040527 1.009999990463257 L 16.40333557128906 1.009999990463257 L 16.40333557128906 5.106236457824707 L 29.73666954040527 5.106236457824707 L 29.73666954040527 1.009999990463257 Z M 20.84778022766113 27.63553237915039 L 25.2922248840332 27.63553237915039 L 25.2922248840332 15.34682750701904 L 20.84778022766113 15.34682750701904 L 20.84778022766113 27.63553237915039 Z M 38.69221496582031 14.07699584960938 L 41.84778213500977 11.16866588592529 C 40.8922233581543 10.12412548065186 39.8477783203125 9.14102840423584 38.71444320678711 8.280819892883301 L 35.55888748168945 11.18914604187012 C 32.11444473266602 8.649479866027832 27.7811164855957 7.133872985839844 23.07000160217285 7.133872985839844 C 12.02555561065674 7.133872985839844 3.069999694824219 15.38779067993164 3.069999694824219 25.56693267822266 C 3.069999694824219 35.74607849121094 12.00333213806152 43.99999237060547 23.07000160217285 43.99999237060547 C 34.13666534423828 43.99999237060547 43.07000732421875 35.74607849121094 43.07000732421875 25.56693267822266 C 43.07000732421875 21.24540710449219 41.42555999755859 17.2515754699707 38.69222640991211 14.07699394226074 Z M 23.06999588012695 39.92424011230469 C 14.46999454498291 39.92424011230469 7.514440536499023 33.51362991333008 7.514440536499023 25.58741569519043 C 7.514440536499023 17.66119766235352 14.46999454498291 11.25059223175049 23.06999588012695 11.25059223175049 C 31.67000007629395 11.25059223175049 38.62554931640625 17.66119956970215 38.62554931640625 25.58741569519043 C 38.62554931640625 33.51362991333008 31.67000007629395 39.92424011230469 23.06999588012695 39.92424011230469 Z" fill="#656d74" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
