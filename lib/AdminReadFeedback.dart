import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AdminFeedbackPage extends StatefulWidget {
  @override
  AdminFeedbackPageState createState() {
    return AdminFeedbackPageState();
  }
}

class AdminFeedbackPageState extends State<AdminFeedbackPage>{

  CollectionReference ref = FirebaseFirestore.instance.collection('Feedback');
  CollectionReference users = FirebaseFirestore.instance.collection('Profile');
  CollectionReference stop = FirebaseFirestore.instance.collection('BicycleStop');

  final DateFormat formatter = DateFormat('yyyy-MM-dd');

  int _selectedSort = 0;
  List<DropdownMenuItem<int>> sortList = [];

  void loadSortList() {
    sortList = [];
    sortList.add(new DropdownMenuItem(
      child: new Text('Date (Latest to Oldest)'),
      value: 0,
    ));
    sortList.add(new DropdownMenuItem(
      child: new Text('Date (Oldest to Latest)'),
      value: 1,
    ));
  }

  List<Widget> getSortWidget(){
    List<Widget> formWidget = new List();
    formWidget.add(new DropdownButton(
      items: sortList,
      value: _selectedSort,
      hint: new Text('Sort by'),
      onChanged: (value) {
        setState(() {
          _selectedSort = value;
        });
      },
      isExpanded: true,
    ));

    return formWidget;
  }

  int _selectedFilter = 0;
  List<DropdownMenuItem<int>> filterList = [];

  void loadFilterList() {
    filterList = [];
    filterList.add(new DropdownMenuItem(
      child: new Text('All'),
      value: 0,
    ));
    filterList.add(new DropdownMenuItem(
      child: new Text('Feedback'),
      value: 1,
    ));
    filterList.add(new DropdownMenuItem(
      child: new Text('Issue'),
      value: 2,
    ));
    filterList.add(new DropdownMenuItem(
      child: new Text('Unread'),
      value: 3,
    ));
    filterList.add(new DropdownMenuItem(
      child: new Text('Read'),
      value: 4,
    ));
  }

  List<Widget> getFilterWidget(){
    List<Widget> formWidget = new List();
    formWidget.add(new DropdownButton(
      items: filterList,
      value: _selectedFilter,
      hint: new Text('Filter by'),
      onChanged: (value) {
        setState(() {
          _selectedFilter = value;
        });
      },
      isExpanded: true,
    ));

    return formWidget;
  }


  @override
  Widget build(BuildContext context) {
    loadSortList();
    loadFilterList();

    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 20),
            height: 150,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  child: IconButton(
                      icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                      onPressed: () {
                        Navigator.pop(context);
                      }
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 28),
                  child: Image(
                    height: 200,
                    image: const AssetImage('assets/images/logo.png'),
                  ),
                ),
              ],
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 20, left: 25, right: 25),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: Container(
                    child: Text(
                      "Filter by: ",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                        color: const Color(0xff656d74),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Container(
                    child: Text(
                      "Sort by: ",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                        color: const Color(0xff656d74),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          Container(
            margin: const EdgeInsets.only( bottom: 10, left: 25, right: 25),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                       margin: const EdgeInsets.only(right: 25),
                    child: new ListView(
                      shrinkWrap: true,
                      children: getFilterWidget(),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(right: 10),
                    child: new ListView(
                      shrinkWrap: true,
                      children: getSortWidget(),
                    ),
                  ),
                ),
              ],
            ),
          ),


          if(_selectedFilter == 0 && _selectedSort == 0)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.orderBy('dateReported', descending: true).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedFilter == 0 && _selectedSort == 1)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.orderBy('dateReported', descending: false).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedFilter == 1 && _selectedSort == 0)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where('category', isEqualTo: 'Feedback').orderBy('dateReported', descending: true).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedFilter == 1 && _selectedSort == 1)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where('category', isEqualTo: 'Feedback').orderBy('dateReported', descending: false).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedFilter == 2 && _selectedSort == 0)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where('category', isEqualTo: 'Issue').orderBy('dateReported', descending: true).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedFilter == 2 && _selectedSort == 1)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where('category', isEqualTo: 'Issue').orderBy('dateReported', descending: false).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedFilter == 3 && _selectedSort == 0)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where('status', isEqualTo: 'unread').orderBy('dateReported', descending: true).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedFilter == 3 && _selectedSort == 1)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where('status', isEqualTo: 'unread').orderBy('dateReported', descending: false).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedFilter == 4 && _selectedSort == 0)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where('status', isEqualTo: 'read').orderBy('dateReported', descending: true).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedFilter == 4 && _selectedSort == 1)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where('status', isEqualTo: 'read').orderBy('dateReported', descending: false).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 310,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['category'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateReported'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          child: FutureBuilder<DocumentSnapshot>(
                                            future: users.doc(doc['userID']).get(),
                                            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                              if (snapshot.hasError) {
                                                return Text("Something went wrong");
                                              }
                                              if (snapshot.connectionState == ConnectionState.done) {
                                                Map<String, dynamic> data = snapshot.data.data();
                                                return Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text("${data['userEmail']}",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xffbfa780),
                                                    ),
                                                  ),
                                                );
                                              }
                                              return Text("loading");
                                            },
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:5),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              doc['bicycleID'],
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:15, left: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        if(doc['feedback'].length > 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'].substring(0,60) + "...",
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        if(doc['feedback'].length <= 60)
                                          Container(
                                            height: 50,
                                            width: 325,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              borderRadius: BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                                              ],
                                            ),
                                            margin: const EdgeInsets.only(top:15, bottom: 10),
                                            padding: const EdgeInsets.only(top: 5, bottom:5, left:10, right:10),

                                            child: Text(
                                              doc['feedback'],
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 15,
                                                color: const Color(0xff656d74),
                                              ),
                                            ),
                                          ),

                                        Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: ButtonTheme(
                                              minWidth: 95.0,
                                              height: 30.0,
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  //side: BorderSide(color: Colors.red)
                                                ),
                                                color: const Color(0xffbfa780),
                                                onPressed: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                      builder: (BuildContext context) => FullFeedback(doc.id),
                                                      fullscreenDialog: true,
                                                    ),
                                                  );
                                                },
                                                child: Text('Read More >',
                                                    style:
                                                    TextStyle(fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700)
                                                ),
                                              ),
                                            ),
                                          ),
                                        )

                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

        ],
      ),
    );
  }
}

class FullFeedback extends StatefulWidget{
  @override

  String feedbackID;

  FullFeedback(String id){
    feedbackID = id;
  }

  FullFeedbackState createState(){
    return FullFeedbackState(feedbackID);
  }
}

class FullFeedbackState extends State<FullFeedback>{

  String feedbackID;

  FullFeedbackState(String id){
    feedbackID = id;
  }

  CollectionReference ref = FirebaseFirestore.instance.collection('Feedback');
  CollectionReference users = FirebaseFirestore.instance.collection('Profile');
  CollectionReference stop = FirebaseFirestore.instance.collection('BicycleStop');
  final DateFormat formatter = DateFormat('yyyy-MM-dd');


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          backgroundColor: Color(0xff656d74),
          elevation: 15,
        ),
      ),

      body: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: FutureBuilder<DocumentSnapshot>(
              future: ref.doc(feedbackID).get(),
              builder:
                  (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {

                if (snapshot.hasError) {
                  return Text("Something went wrong");
                }

                if (snapshot.connectionState == ConnectionState.done) {
                  Map<String, dynamic> doc = snapshot.data.data();
                  return Container(
                    height: 530,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black45,
                      ),
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                      ],
                    ),
                    margin: const EdgeInsets.only(top: 30, bottom: 30, left: 22, right: 22),
                    padding: const EdgeInsets.only(top: 15, bottom:10, left:12, right:12),

                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 4,
                              child: Text(
                                doc['category'],
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 16,
                                  color: const Color(0xff656d74),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 6,
                              child: Text(
                                formatter.format(doc['dateReported'].toDate()).toString(),
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 16,
                                  color: const Color(0xff656d74),
                                ),
                              ),
                            ),
                          ],
                        ),

                        Divider(
                          thickness: 1,
                          color: Colors.black54,
                        ),

                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                FutureBuilder<DocumentSnapshot>(
                                    future: users.doc(doc['userID']).get(),
                                    builder:
                                        (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                      if (snapshot.hasError) {
                                        return Text("Something went wrong");
                                      }

                                      if (snapshot.connectionState == ConnectionState.done) {
                                        Map<String, dynamic> data = snapshot.data.data();
                                        return Text("${data['userName']}",
                                          style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 16,
                                            color: const Color(0xff656d74),
                                            fontWeight: FontWeight.w700,
                                          ),
                                          textAlign: TextAlign.left,
                                        );
                                      }

                                      return Text("loading");
                                    }
                                ),

                                Container(
                                  margin: EdgeInsets.only(left:10),
                                  child: FutureBuilder<DocumentSnapshot>(
                                      future: users.doc(doc['userID']).get(),
                                      builder:
                                          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                        if (snapshot.hasError) {
                                          return Text("Something went wrong");
                                        }

                                        if (snapshot.connectionState == ConnectionState.done) {
                                          Map<String, dynamic> data = snapshot.data.data();
                                          if((data['userGender'.toString()]).compareTo("Male") == 0){
                                            return Image(
                                              height: 18,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/gender_male.png'),
                                            );
                                          }

                                          else if ((data['userGender'.toString()]).compareTo("Female") == 0){
                                            return Image(
                                              height: 20,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/gender_female.png'),
                                            );
                                          }

                                          else{
                                            return Text("");
                                          }
                                        }
                                        return Text("loading");
                                      }
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: const EdgeInsets.only(top: 2),
                            child: FutureBuilder<DocumentSnapshot>(
                              future: users.doc(doc['userID']).get(),
                              builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                if (snapshot.hasError) {
                                  return Text("Something went wrong");
                                }
                                if (snapshot.connectionState == ConnectionState.done) {
                                  Map<String, dynamic> data = snapshot.data.data();
                                  return Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("${data['userEmail']}",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 15,
                                        color: const Color(0xff656d74),
                                      ),
                                    ),
                                  );
                                }
                                return Text("loading");
                              },
                            ),
                          ),
                        ),

                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: const EdgeInsets.only(top: 2),
                            child: FutureBuilder<DocumentSnapshot>(
                              future: users.doc(doc['userID']).get(),
                              builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                if (snapshot.hasError) {
                                  return Text("Something went wrong");
                                }
                                if (snapshot.connectionState == ConnectionState.done) {
                                  Map<String, dynamic> data = snapshot.data.data();
                                  return Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("${data['userPhone']}",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 16,
                                        color: const Color(0xff656d74),
                                      ),
                                    ),
                                  );
                                }
                                return Text("loading");
                              },
                            ),
                          ),
                        ),

                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: const EdgeInsets.only(top: 20),
                            child: Text(
                              doc['bicycleID'],
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 16,
                                color: const Color(0xff656d74),
                              ),
                            ),
                          ),
                        ),

                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: const EdgeInsets.only(top: 5),
                            child: Row(
                              children: <Widget>[
                                Image(
                                  height: 22,
                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                ),

                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: FutureBuilder<DocumentSnapshot>(
                                    future: stop.doc(doc['rentStop']).get(),
                                    builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                      if (snapshot.hasError) {
                                        return Text("Something went wrong");
                                      }
                                      if (snapshot.connectionState == ConnectionState.done) {
                                        Map<String, dynamic> data = snapshot.data.data();
                                        return Text("${data['stopName']}",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 14.6,
                                            color: const Color(0xff656d74),
                                          ),
                                        );
                                      }
                                      return Text("loading");
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        Row(
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(left:10),
                                child:Image(
                                  height: 12,
                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                ),
                              )
                            ]
                        ),

                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: const EdgeInsets.only(top: 5),
                            child: Row(
                              children: <Widget>[
                                Image(
                                  height: 22,
                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                ),

                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: FutureBuilder<DocumentSnapshot>(
                                    future: stop.doc(doc['returnStop']).get(),
                                    builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                      if (snapshot.hasError) {
                                        return Text("Something went wrong");
                                      }
                                      if (snapshot.connectionState == ConnectionState.done) {
                                        Map<String, dynamic> data = snapshot.data.data();
                                        return Text("${data['stopName']}",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 14.6,
                                            color: const Color(0xff656d74),
                                          ),
                                        );
                                      }
                                      return Text("loading");
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            height: 200,
                            width: 325,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black26,
                              ),
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(color: Colors.grey[200], spreadRadius: 1),
                              ],
                            ),
                            margin: const EdgeInsets.only(top:25, bottom:10),
                            padding: const EdgeInsets.only(top:10, bottom:10, left:10, right:10),

                            child: SingleChildScrollView(
                              child: Text(
                                doc['feedback'],
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 15,
                                  color: const Color(0xff656d74),
                                ),
                              ),
                            ),
                          ),
                        ),

                        if(doc['status'].toString() == "read")
                          Container(
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: ButtonTheme(
                                minWidth: 105.0,
                                height: 32.0,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    //side: BorderSide(color: Colors.red)
                                  ),
                                  color: const Color(0xffbfa780),
                                  onPressed: (){
                                    ref.doc(feedbackID).update({'status':'unread'});
                                    Navigator.pop(context);
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                        builder: (BuildContext context) => FullFeedback(feedbackID),
                                        fullscreenDialog: true,
                                      ),
                                    );
                                  },
                                  child:
                                  Text('Mark as Unread',
                                      style:
                                      TextStyle(fontFamily: 'Montserrat',
                                          fontSize: 13,
                                          color: const Color(0xffffffff),
                                          fontWeight: FontWeight.w700)
                                  ),
                                ),
                              ),
                            ),
                          ),

                        if(doc['status'].toString() == "unread")
                          Container(
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: ButtonTheme(
                                minWidth: 105.0,
                                height: 32.0,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    //side: BorderSide(color: Colors.red)
                                  ),
                                  color: const Color(0xffbfa780),
                                  onPressed: (){
                                    ref.doc(feedbackID).update({'status':'read'});
                                    Navigator.pop(context);
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                        builder: (BuildContext context) => FullFeedback(feedbackID),
                                        fullscreenDialog: true,
                                      ),
                                    );
                                  },
                                  child: Text('Mark as Read',
                                      style:
                                      TextStyle(fontFamily: 'Montserrat',
                                          fontSize: 13,
                                          color: const Color(0xffffffff),
                                          fontWeight: FontWeight.w700)
                                  ),
                                ),
                              ),
                            ),
                          ),

                        if(!(doc.containsKey('status')))
                          Container(
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: ButtonTheme(
                                minWidth: 105.0,
                                height: 32.0,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    //side: BorderSide(color: Colors.red)
                                  ),
                                  color: const Color(0xffbfa780),
                                  onPressed: (){
                                    ref.doc(feedbackID).update({'status':'read'});
                                    Navigator.pop(context);
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                        builder: (BuildContext context) => FullFeedback(feedbackID),
                                        fullscreenDialog: true,
                                      ),
                                    );
                                  },
                                  child: Text('Mark as Read',
                                      style:
                                      TextStyle(fontFamily: 'Montserrat',
                                          fontSize: 13,
                                          color: const Color(0xffffffff),
                                          fontWeight: FontWeight.w700)
                                  ),
                                ),
                              ),
                            ),
                          ),

                      ],
                    ),
                  );
                }
                return Text("loading");
              },
            ),
          ),
        ],
      ),
    );
  }
}



