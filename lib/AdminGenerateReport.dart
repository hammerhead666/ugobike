import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'dart:async';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import './core/models/rentingHistoryModel.dart';
import './AdminGenerateReportByMonth.dart';
import './AdminGenerateReportByStop.dart';
import './GeneralReportPreview.dart';
import './AdminLandingPage.dart';

class AdminGenerateReportPage extends StatefulWidget{
  @override
  AdminGenerateReportPageState createState(){
    return AdminGenerateReportPageState();
  }
}

class AdminGenerateReportPageState extends State<AdminGenerateReportPage>{

  final pdf = pw.Document(deflate: zlib.encode);
  CollectionReference ref = FirebaseFirestore.instance.collection('RentingHistory');
  final List<RentingHistory> historyList = [];
  double totalProfit = 0;
  var current = DateFormat('yyyy-MM').format(DateTime.now());

  @override
  Widget build(BuildContext context){

    return Scaffold(
      backgroundColor: const Color(0xfffafdff),

      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 20),
            height: 150,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  child: IconButton(
                      icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (BuildContext context) => AdminLandingPage(),
                            fullscreenDialog: true,
                          ),
                        );
                      }
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 28),
                  child: Image(
                    height: 200,
                    image: const AssetImage('assets/images/logo.png'),
                  ),
                ),
              ],
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 35),
            child: Text(
              'Generate Report',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 25,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
                height: 1.2,
              ),
              textAlign: TextAlign.center,
            ),
          ),

          Center(
            child: GestureDetector(
              onTap: () async{
                await fetchAndSetList();
                await writeOnPDF();
                await savePdf();

                Directory documentDirectory = await getApplicationDocumentsDirectory();

                String documentPath = documentDirectory.path;

                String fullPath = "$documentPath/general_report_${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}.pdf";

                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => GeneralReportPreview(fullPath)
                ));
              },
              child: Container(
                  margin: const EdgeInsets.only(top: 55),
                  height: 70,
                  width: 340,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    color: const Color(0xffffffff),
                    boxShadow: [
                      BoxShadow(
                        color: const Color(0x14000000),
                        offset: Offset(5, 5),
                        blurRadius: 10,
                      ),
                    ],
                  ),
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(left:25, right:20),
                        child: SvgPicture.string(
                          _svg_ierd46,
                          allowDrawingOutsideViewBox: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left:20, right:25),
                        child: Text(
                          'General Report of\nthe Month',
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 16.5,
                            color: const Color(0xff656d74),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  )
              ),
            ),
          ),

          Center(
            child: GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                    builder: (BuildContext context) => AdminGenerateReportByStopPage(),
                    fullscreenDialog: true,
                  ),
                );
              },
              child: Container(
                margin: const EdgeInsets.only(top: 30),
                height: 70,
                width: 340,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  color: const Color(0xffffffff),
                  boxShadow: [
                    BoxShadow(
                      color: const Color(0x14000000),
                      offset: Offset(5, 5),
                      blurRadius: 10,
                    ),
                  ],
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(left:20, right:18),
                      child: Image(
                        height: 85,
                        image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left:20, right:25),
                      child: Text(
                        'View Report by Stop',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 16.5,
                          color: const Color(0xff656d74),
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

          Center(
            child: GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                    builder: (BuildContext context) => AdminGenerateReportByMonthPage(),
                    fullscreenDialog: true,
                  ),
                );
              },
              child: Container(
                margin: const EdgeInsets.only(top: 30),
                height: 70,
                width: 340,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  color: const Color(0xffffffff),
                  boxShadow: [
                    BoxShadow(
                      color: const Color(0x14000000),
                      offset: Offset(5, 5),
                      blurRadius: 10,
                    ),
                  ],
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(left:25, right:20),
                      child: SvgPicture.string(
                        _svg_1q6v29,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left:20, right:25),
                      child: Text(
                        'View Report by Month',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 16.5,
                          color: const Color(0xff656d74),
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),

    );
  }

  Future <void> fetchAndSetList() async{

    if(current.toString() == "2021-01"){
      DateTime start = new DateTime(2021,1,1);
      DateTime end = new DateTime(2021,1,31);

      await ref.where("dateRented", isGreaterThanOrEqualTo: start).where("dateRented", isLessThanOrEqualTo: end).orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach((f) => {
            historyList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }

    if(current.toString() == "2021-02"){
      DateTime start = new DateTime(2021,2,1);
      DateTime end = new DateTime(2021,2,28);

      await ref.where("dateRented", isGreaterThanOrEqualTo: start).where("dateRented", isLessThanOrEqualTo: end).orderBy('dateRented', descending: true).get().then(
              (QuerySnapshot snapshot) => snapshot.docs.forEach((f) => {
            historyList.add(RentingHistory(
                dateRented: f.data()['dateRented'],
                bicycleID: f.data()['bicycleID'],
                rentStop: f.data()['rentStop'],
                rentingFee: f.data()['rentingFee'],
                returnStop: f.data()['returnStop'],
                totalMin: f.data()['totalMin'],
                userID: f.data()['userID']
            )),
          })
      );
    }
  }

  Future writeOnPDF() async{

    const imageProvider = const AssetImage('assets/images/logo.png');
    final image = await flutterImageProvider(imageProvider);

    for(var index in historyList)
      totalProfit = totalProfit + index.rentingFee;


    if(current.toString() == "2021-01"){
      pdf.addPage(
          pw.MultiPage(
            pageFormat: PdfPageFormat.a4,
            margin: pw.EdgeInsets.only(top:32, bottom:32, left:20, right:20),
            orientation: pw.PageOrientation.portrait,
            maxPages: 3,

            build: (pw.Context context){
              return <pw.Widget>  [

                pw.Row(
                    children: <pw.Widget>[
                      pw.Container(
                        height: 120,
                        width: 1000,
                        decoration: pw.BoxDecoration(
                          borderRadius: pw.BorderRadius.circular(10.0),
                          color: const PdfColor.fromInt(0xff656d74),
                        ),
                        child: pw.Image(image),
                      ),

                      pw.Expanded(
                        // flex: 7,
                          child: pw.Container(
                              margin: pw.EdgeInsets.only(left: 35),
                              child: pw.Text(
                                "UGoBike Report January 2021",
                                style: pw.TextStyle(
                                  fontSize: 35,
                                  fontWeight: pw.FontWeight.bold,
                                ),
                              )
                          )
                      )
                    ]
                ),

                pw.Container(
                  margin: pw.EdgeInsets.only(top: 40),
                  child: pw.Header(
                      level: 0,
                      child: pw.Text("")
                  ),
                ),

                pw.Align(
                  alignment: pw.Alignment.topRight,
                  child: pw.Container(
                    margin: pw.EdgeInsets.only(top: 30, bottom: 50),
                    child: pw.Text(
                      "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                      style: pw.TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: pw.TextAlign.right,
                    ),
                  ),
                ),

                pw.Expanded(
                  child: pw.Container(
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Row(
                                children: <pw.Widget>[
                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      width: 122,
                                      height: 40,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Date Rented",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 82,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Rent Stop",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 96,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Return Stop",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 82,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Bicycle ID",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 84,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Total Time",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 95,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Renting Fee",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),
                                ]
                            ),
                            pw.Container(
                                child: pw.Column(
                                    children: <pw.Widget>[
                                      for(var index in historyList)
                                        pw.Row(
                                            children: [
                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 122,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Container(
                                                      child: pw.Text(
                                                        "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                                        style: pw.TextStyle(
                                                          fontSize: 12,
                                                        ),
                                                        textAlign: pw.TextAlign.center,
                                                      )
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 82,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.rentStop.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 96,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.returnStop.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 82,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.bicycleID.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 84,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    "${index.totalMin.toString()}" + " min",
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 95,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                            ]
                                        ),
                                    ]
                                )
                            ),

                          ]
                      )
                  ),
                ),


                pw.Container(
                  // margin: pw.EdgeInsets.only(top: 35),
                    child: pw.Text(
                      "Total Number of Rent(s):   " + "${historyList.length.toString()}",
                      style: pw.TextStyle(
                        fontSize: 20,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

                pw.Container(
                    margin: pw.EdgeInsets.only(top: 15),
                    child: pw.Text(
                      "Total Profit of the Month:  RM ${totalProfit.toStringAsFixed(2)}",
                      style: pw.TextStyle(
                        fontSize: 21,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

              ];
            },

          )
      );
    }

    if(current.toString() == "2021-02"){
      pdf.addPage(
          pw.MultiPage(
            pageFormat: PdfPageFormat.a4,
            margin: pw.EdgeInsets.only(top:32, bottom:32, left:20, right:20),
            orientation: pw.PageOrientation.portrait,
            maxPages: 3,

            build: (pw.Context context){
              return <pw.Widget>  [

                pw.Row(
                    children: <pw.Widget>[
                      pw.Container(
                        height: 120,
                        width: 1000,
                        decoration: pw.BoxDecoration(
                          borderRadius: pw.BorderRadius.circular(10.0),
                          color: const PdfColor.fromInt(0xff656d74),
                        ),
                        child: pw.Image(image),
                      ),

                      pw.Expanded(
                        // flex: 7,
                          child: pw.Container(
                              margin: pw.EdgeInsets.only(left: 35),
                              child: pw.Text(
                                "UGoBike Report February 2021",
                                style: pw.TextStyle(
                                  fontSize: 35,
                                  fontWeight: pw.FontWeight.bold,
                                ),
                              )
                          )
                      )
                    ]
                ),

                pw.Container(
                  margin: pw.EdgeInsets.only(top: 40),
                  child: pw.Header(
                      level: 0,
                      child: pw.Text("")
                  ),
                ),

                pw.Align(
                  alignment: pw.Alignment.topRight,
                  child: pw.Container(
                    margin: pw.EdgeInsets.only(top: 30, bottom: 50),
                    child: pw.Text(
                      "Date Generated:\n" + "${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}",
                      style: pw.TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: pw.TextAlign.right,
                    ),
                  ),
                ),

                pw.Expanded(
                  child: pw.Container(
                      child: pw.Column(
                          children: <pw.Widget>[
                            pw.Row(
                                children: <pw.Widget>[
                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      width: 122,
                                      height: 40,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Date Rented",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 82,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Rent Stop",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 96,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Return Stop",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 82,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Bicycle ID",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 84,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Total Time",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),

                                  pw.Container(
                                      padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                      height: 40,
                                      width: 95,
                                      decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                            color: PdfColors.black,
                                          )
                                      ),
                                      child: pw.Text(
                                        "Renting Fee",
                                        style: pw.TextStyle(
                                          fontSize: 14.5,
                                          fontWeight: pw.FontWeight.bold,
                                        ),
                                        textAlign: pw.TextAlign.center,
                                      )
                                  ),
                                ]
                            ),

                            if(historyList.length == 0)
                              pw.Container(
                                  child: pw.Column(
                                      children: <pw.Widget>[
                                        for(var index in historyList)
                                          pw.Row(
                                              children: [
                                                pw.Container(
                                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                    height: 38,
                                                    width: 122,
                                                    decoration: pw.BoxDecoration(
                                                        border: pw.Border.all(
                                                          color: PdfColors.grey800,
                                                        )
                                                    ),
                                                    child: pw.Container(
                                                        child: pw.Text(
                                                          "NIL",
                                                          style: pw.TextStyle(
                                                            fontSize: 12,
                                                          ),
                                                          textAlign: pw.TextAlign.center,
                                                        )
                                                    )
                                                ),

                                                pw.Container(
                                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                    height: 38,
                                                    width: 82,
                                                    decoration: pw.BoxDecoration(
                                                        border: pw.Border.all(
                                                          color: PdfColors.grey800,
                                                        )
                                                    ),
                                                    child: pw.Text(
                                                      "NIL",
                                                      style: pw.TextStyle(
                                                        fontSize: 15,
                                                      ),
                                                      textAlign: pw.TextAlign.center,
                                                    )
                                                ),

                                                pw.Container(
                                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                    height: 38,
                                                    width: 96,
                                                    decoration: pw.BoxDecoration(
                                                        border: pw.Border.all(
                                                          color: PdfColors.grey800,
                                                        )
                                                    ),
                                                    child: pw.Text(
                                                      "NIL",
                                                      style: pw.TextStyle(
                                                        fontSize: 15,
                                                      ),
                                                      textAlign: pw.TextAlign.center,
                                                    )
                                                ),

                                                pw.Container(
                                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                    height: 38,
                                                    width: 82,
                                                    decoration: pw.BoxDecoration(
                                                        border: pw.Border.all(
                                                          color: PdfColors.grey800,
                                                        )
                                                    ),
                                                    child: pw.Text(
                                                      "NIL",
                                                      style: pw.TextStyle(
                                                        fontSize: 15,
                                                      ),
                                                      textAlign: pw.TextAlign.center,
                                                    )
                                                ),

                                                pw.Container(
                                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                    height: 38,
                                                    width: 84,
                                                    decoration: pw.BoxDecoration(
                                                        border: pw.Border.all(
                                                          color: PdfColors.grey800,
                                                        )
                                                    ),
                                                    child: pw.Text(
                                                      "NIL",
                                                      style: pw.TextStyle(
                                                        fontSize: 15,
                                                      ),
                                                      textAlign: pw.TextAlign.center,
                                                    )
                                                ),

                                                pw.Container(
                                                    padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                    height: 38,
                                                    width: 95,
                                                    decoration: pw.BoxDecoration(
                                                        border: pw.Border.all(
                                                          color: PdfColors.grey800,
                                                        )
                                                    ),
                                                    child: pw.Text(
                                                      "NIL",
                                                      style: pw.TextStyle(
                                                        fontSize: 15,
                                                      ),
                                                      textAlign: pw.TextAlign.center,
                                                    )
                                                ),

                                              ]
                                          ),
                                      ]
                                  )
                              ),


                            pw.Container(
                                child: pw.Column(
                                    children: <pw.Widget>[
                                      for(var index in historyList)
                                        pw.Row(
                                            children: [
                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 122,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Container(
                                                      child: pw.Text(
                                                        "${DateFormat('yyyy-MM-dd HH:mm:ss').format(index.dateRented.toDate())}",
                                                        style: pw.TextStyle(
                                                          fontSize: 12,
                                                        ),
                                                        textAlign: pw.TextAlign.center,
                                                      )
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 82,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.rentStop.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 96,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.returnStop.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 82,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    index.bicycleID.toString(),
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 84,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    "${index.totalMin.toString()}" + " min",
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                              pw.Container(
                                                  padding: pw.EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
                                                  height: 38,
                                                  width: 95,
                                                  decoration: pw.BoxDecoration(
                                                      border: pw.Border.all(
                                                        color: PdfColors.grey800,
                                                      )
                                                  ),
                                                  child: pw.Text(
                                                    "RM " + "${index.rentingFee.toStringAsFixed(2)}",
                                                    style: pw.TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                    textAlign: pw.TextAlign.center,
                                                  )
                                              ),

                                            ]
                                        ),
                                    ]
                                )
                            ),

                          ]
                      )
                  ),
                ),


                pw.Container(
                  // margin: pw.EdgeInsets.only(top: 35),
                    child: pw.Text(
                      "Total Number of Rent(s):   " + "${historyList.length.toString()}",
                      style: pw.TextStyle(
                        fontSize: 20,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

                pw.Container(
                    margin: pw.EdgeInsets.only(top: 15),
                    child: pw.Text(
                      "Total Profit of the Month:  RM ${totalProfit.toStringAsFixed(2)}",
                      style: pw.TextStyle(
                        fontSize: 21,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      textAlign: pw.TextAlign.left,
                    )
                ),

              ];
            },

          )
      );
    }
  }

  Future savePdf() async{
    Directory documentDirectory = await getApplicationDocumentsDirectory();

    String documentPath = documentDirectory.path;

    print("Path:" + "$documentPath/general_report_${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}.pdf");

    File file = File("$documentPath/general_report_${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}.pdf");

    file.writeAsBytesSync(await pdf.save());
  }

}


const String _svg_ef2urf =
    '<svg viewBox="31.4 54.6 9.6 16.6" ><path transform="translate(-237.75, -83.36)" d="M 278.7493896484375 137.9653625488281 L 269.19140625 146.2719116210938 L 278.7493896484375 154.5784454345703" fill="none" stroke="#ffffff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" /></svg>';
const String _svg_ww2325 =
    '<svg viewBox="0.0 0.0 60.4 60.3" ><path  d="M 0 0 L 60.38252258300781 0 L 60.38252258300781 60.25265884399414 L 0 60.25265884399414 L 0 0 Z" fill="none" stroke="none" stroke-width="4.125" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_ierd46 =
    '<svg viewBox="7.5 2.5 45.3 50.2" ><path transform="translate(-4.83, -1.61)" d="M 52.6300163269043 9.146055221557617 L 50.11407852172852 9.146055221557617 L 50.11407852172852 4.125 L 45.08219909667969 4.125 L 45.08219909667969 9.146055221557617 L 24.9546947479248 9.146055221557617 L 24.9546947479248 4.125 L 19.92281532287598 4.125 L 19.92281532287598 9.146055221557617 L 17.40687561035156 9.146055221557617 C 14.61418628692627 9.146055221557617 12.40015983581543 11.40552806854248 12.40015983581543 14.1671085357666 L 12.375 49.31449508666992 C 12.375 52.07607269287109 14.61418628692627 54.33554840087891 17.40687561035156 54.33554840087891 L 52.6300163269043 54.33554840087891 C 55.39754867553711 54.33554840087891 57.66188812255859 52.07607269287109 57.66188812255859 49.31449508666992 L 57.66188812255859 14.1671085357666 C 57.66188812255859 11.40552806854248 55.39754867553711 9.146055221557617 52.6300163269043 9.146055221557617 Z M 52.6300163269043 49.31449508666992 L 17.40687561035156 49.31449508666992 L 17.40687561035156 21.69869422912598 L 52.6300163269043 21.69869422912598 L 52.6300163269043 49.31449508666992 Z M 22.43875503540039 26.71974945068359 L 35.01844787597656 26.71974945068359 L 35.01844787597656 39.27238845825195 L 22.43875503540039 39.27238845825195 L 22.43875503540039 26.71974945068359 Z" fill="#bfa780" stroke="none" stroke-width="4.125" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_aen4sm =
    '<svg viewBox="0.0 0.0 58.5 53.3" ><path  d="M 0 0 L 58.469970703125 0 L 58.469970703125 53.3231201171875 L 0 53.3231201171875 L 0 0 Z" fill="none" stroke="none" stroke-width="3.75" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_1q6v29 =
    '<svg viewBox="4.9 2.2 48.7 48.9" ><path transform="translate(-2.63, -1.53)" d="M 51.35248184204102 8.193593978881836 L 48.91622924804688 8.193593978881836 L 48.91622924804688 3.75 L 44.04373550415039 3.75 L 44.04373550415039 8.193593978881836 L 19.68124389648438 8.193593978881836 L 19.68124389648438 3.75 L 14.80874729156494 3.75 L 14.80874729156494 8.193593978881836 L 12.37249851226807 8.193593978881836 C 9.692624092102051 8.193593978881836 7.500000476837158 10.19321060180664 7.500000476837158 12.63718605041504 L 7.500000476837158 48.18593215942383 C 7.500000476837158 50.62990570068359 9.692624092102051 52.62952423095703 12.37249851226807 52.62952423095703 L 51.35248184204102 52.62952423095703 C 54.03235244750977 52.62952423095703 56.2249755859375 50.62990570068359 56.2249755859375 48.18593215942383 L 56.2249755859375 12.63718605041504 C 56.2249755859375 10.19321060180664 54.03235244750977 8.193593978881836 51.35248184204102 8.193593978881836 Z M 51.35248184204102 48.18593215942383 L 12.37249851226807 48.18593215942383 L 12.37249851226807 19.30257797241211 L 51.35248184204102 19.30257797241211 L 51.35248184204102 48.18593215942383 Z" fill="#bfa780" stroke="none" stroke-width="3.75" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_me5mux =
    '<svg viewBox="26.0 1038.0 50.0 63.6" ><defs><filter id="shadow"><feDropShadow dx="0" dy="3" stdDeviation="6"/></filter></defs><path transform="translate(26.0, 1038.0)" d="M 25 0 C 38.80712127685547 0 50 10.67789268493652 50 23.84973526000977 C 50 37.02157974243164 38.80712127685547 63.59930801391602 25 63.59930801391602 C 11.19288349151611 63.59930801391602 0 37.02157974243164 0 23.84973526000977 C 0 10.67789268493652 11.19288349151611 0 25 0 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" filter="url(#shadow)"/></svg>';
const String _svg_7r29ss =
    '<svg viewBox="0.0 0.0 24.6 20.6" ><path transform="translate(0.0, -4.0)" d="M 5.959543228149414 11.71578884124756 L 7.700304985046387 5.697474002838135 C 7.997257232666016 4.681561946868896 8.765240669250488 4 9.62537956237793 4 L 12.2877197265625 4 L 12.2877197265625 6.571929931640625 L 9.62537956237793 6.571929931640625 L 8.130373001098633 11.71578884124756 L 13.05569934844971 11.71578884124756 L 13.42433071136475 10.42982482910156 L 12.2877197265625 10.42982482910156 L 12.2877197265625 7.857894897460938 L 17.4076042175293 7.857894897460938 L 17.4076042175293 10.42982482910156 L 15.61564350128174 10.42982482910156 L 13.75200653076172 16.85964775085449 L 14.43806934356689 16.85964775085449 C 14.88862228393555 13.99194717407227 16.8034553527832 11.8701057434082 19.19956207275391 11.7286491394043 C 22.06669425964355 11.54861450195313 24.575439453125 14.54491233825684 24.575439453125 18.1456127166748 C 24.575439453125 21.74631690979004 22.32269096374512 24.575439453125 19.45555686950684 24.575439453125 C 16.93657493591309 24.575439453125 14.89885997772217 22.40215873718262 14.43806934356689 19.43157958984375 L 10.13736915588379 19.43157958984375 C 9.686820030212402 22.29928016662598 7.77198314666748 24.4211254119873 5.375877380371094 24.56258010864258 C 2.508743524551392 24.72975158691406 0 21.74631690979004 0 18.13275337219238 C 0 14.53205299377441 2.252749443054199 11.70292854309082 5.119883060455322 11.70292854309082 L 5.959543228149414 11.70292854309082 Z M 16.56794357299805 19.43157958984375 C 16.9775333404541 20.9361572265625 18.09366798400879 22.00350952148438 19.45555686950684 22.00350952148438 C 21.17583847045898 22.00350952148438 22.52748489379883 20.30603790283203 22.52748489379883 18.1456127166748 C 22.52748489379883 15.98519325256348 21.17583847045898 14.28771877288818 19.45555686950684 14.28771877288818 C 18.09366798400879 14.28771877288818 16.9775333404541 15.35507011413574 16.56794357299805 16.85964775085449 L 19.45555686950684 16.85964775085449 L 19.45555686950684 19.43157958984375 L 16.56794357299805 19.43157958984375 Z M 10.13736724853516 16.85964775085449 L 11.5709342956543 16.85964775085449 L 12.31843662261963 14.28771877288818 L 9.215789794921875 14.28771877288818 C 9.666338920593262 15.03357887268066 9.994011878967285 15.89517402648926 10.13736724853516 16.85964775085449 Z M 5.119883060455322 22.00350952148438 C 6.840164661407471 22.00350952148438 8.191813468933105 20.30603790283203 8.191813468933105 18.1456127166748 C 8.191813468933105 16.94966697692871 7.77198314666748 15.92089462280273 7.116638660430908 15.21361351013184 L 6.133622169494629 18.60856246948242 L 4.20854663848877 17.73410606384277 L 5.201802730560303 14.30057907104492 C 5.171082973480225 14.30057907104492 5.140365600585938 14.28771877288818 5.109645366668701 14.28771877288818 C 3.389363765716553 14.28771877288818 2.037715435028076 15.98519325256348 2.037715435028076 18.1456127166748 C 2.037715435028076 20.30603790283203 3.39960241317749 22.00350952148438 5.119883060455322 22.00350952148438 Z" fill="#bfa780" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_ejqh8k =
    '<svg viewBox="31.0 1043.0 40.0 40.0" ><path transform="translate(31.0, 1043.0)" d="M 20 0 C 31.04569053649902 0 40 8.954304695129395 40 20 C 40 31.04569053649902 31.04569053649902 40 20 40 C 8.954304695129395 40 0 31.04569053649902 0 20 C 0 8.954304695129395 8.954304695129395 0 20 0 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
