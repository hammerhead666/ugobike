import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class HistoryPage extends StatefulWidget{
  @override
  HistoryPageState createState(){
    return HistoryPageState();
  }
}
class HistoryPageState extends State<HistoryPage> {

  CollectionReference ref = FirebaseFirestore.instance.collection('RentingHistory');

  final FirebaseAuth auth = FirebaseAuth.instance;
  final DateFormat formatter = DateFormat('yyyy-MM-dd h:m');

  int _selectedSort = 0;
  List<DropdownMenuItem<int>> sortList = [];

  void loadSortList() {
    sortList = [];
    sortList.add(new DropdownMenuItem(
      child: new Text('Sort by Date (Latest to Oldest)'),
      value: 0,
    ));
    sortList.add(new DropdownMenuItem(
      child: new Text('Sort by Date (Oldest to Latest)'),
      value: 1,
    ));
    sortList.add(new DropdownMenuItem(
      child: new Text('Sort by Fee (Lowest to Highest)'),
      value: 2,
    ));
    sortList.add(new DropdownMenuItem(
      child: new Text('Sort by Fee (Highest to Lowest)'),
      value: 3,
    ));
  }

  List<Widget> getFormWidget(){
    List<Widget> formWidget = new List();
    formWidget.add(new DropdownButton(
      items: sortList,
      value: _selectedSort,
      hint: new Text('Sort by'),
      onChanged: (value) {
        setState(() {
          _selectedSort = value;
        });
      },
      isExpanded: true,
    ));

    return formWidget;
  }

  getCurrentUser() {
    final User user = auth.currentUser;
    final uid = user.uid;

    return uid;
  }

  @override
  Widget build(BuildContext context) {
    loadSortList();
    return Scaffold(
      backgroundColor: const Color(0xfffafdff),
      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            height: 150,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
                child:Image(
                  height: 200,
                  image: const AssetImage('assets/images/logo.png'),
                ),
              ),
          ),

          Container(
              margin: EdgeInsets.only(top: 5, bottom: 10, left: 35, right: 35),
              child: new ListView(
                shrinkWrap: true,
                children: getFormWidget(),
              )
          ),

          if(_selectedSort == 0)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where("userID", isEqualTo: getCurrentUser()).orderBy('dateRented', descending: true).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 160,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['bicycleID'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateRented'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(left:5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:10, left:5, right: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 6,
                                                  child: Text
                                                    (doc['totalMin'].toInt().toString() + " min " + ((doc['totalMin'] * 60) - (doc['totalMin'].toInt() * 60 )).toInt().toString() + " sec",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 14,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Text(
                                                  "RM " + doc['rentingFee'].toStringAsFixed(2),
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 14,
                                                    color: const Color(0xff656d74),
                                                  ),
                                                  textAlign: TextAlign.right,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedSort == 1)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where("userID", isEqualTo: getCurrentUser()).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 160,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['bicycleID'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateRented'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(left:5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:10, left:5, right: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 6,
                                                  child: Text
                                                    (doc['totalMin'].toInt().toString() + " min " + ((doc['totalMin'] * 60) - (doc['totalMin'].toInt() * 60 )).toInt().toString() + " sec",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 14,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Text(
                                                  "RM " + doc['rentingFee'].toStringAsFixed(2),
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 14,
                                                    color: const Color(0xff656d74),
                                                  ),
                                                  textAlign: TextAlign.right,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedSort == 2)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where("userID", isEqualTo: getCurrentUser()).orderBy('rentingFee').get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 160,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['bicycleID'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateRented'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(left:5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:10, left:5, right: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 6,
                                                  child: Text
                                                    (doc['totalMin'].toInt().toString() + " min " + ((doc['totalMin'] * 60) - (doc['totalMin'].toInt() * 60 )).toInt().toString() + " sec",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 14,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Text(
                                                  "RM " + doc['rentingFee'].toStringAsFixed(2),
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 14,
                                                    color: const Color(0xff656d74),
                                                  ),
                                                  textAlign: TextAlign.right,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),

          if(_selectedSort == 3)
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: FutureBuilder<QuerySnapshot>(
                    future: ref.where("userID", isEqualTo: getCurrentUser()).orderBy('rentingFee', descending: true).get(),
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        final List<DocumentSnapshot> documents = snapshot.data.docs;

                        if(documents.isEmpty){
                          return Align(
                            alignment: Alignment.center,
                            child: Image(
                              height: 200,
                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/no_record.png'),
                            ),
                          );
                        }
                        else{
                          return ListView(
                              children: documents
                                  .map((doc) =>
                                  Container(
                                    height: 160,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black45,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(color: Colors.grey[200], spreadRadius: 3),
                                      ],
                                    ),
                                    margin: const EdgeInsets.only(bottom: 20, left: 22, right: 22),
                                    padding: const EdgeInsets.only(top: 10, bottom:10, left:12, right:12),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 4,
                                              child: Text(
                                                doc['bicycleID'],
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 6,
                                              child: Text(
                                                formatter.format(doc['dateRented'].toDate()).toString(),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 15,
                                                  color: const Color(0xff656d74),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),

                                        Divider(
                                          thickness: 1,
                                          color: Colors.black54,
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(left:5),
                                          child: Row(
                                            children: <Widget>[
                                              Image(
                                                height: 22,
                                                image: FirebaseImage('gs://ugobike-d3e33.appspot.com/start_mark.png'),
                                              ),

                                              if(doc['rentStop'] == "S001")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Angkasa",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S002")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("M01, Kolej Tun Dr Ismail",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S003")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L01, Kolej Tun Hussein Onn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S004")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Cengal",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S005")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("K01, Kolej Tun Razak",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S006")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Food Court Kolej Datin Seri Endon",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S007")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("H01, Kolej Tun Fatimah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S008")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("L50, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S009")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Meranti",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S010")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("G01, Kolej Rahman Putra",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S011")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("N28a, School of Computing",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S012")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Sultanah Zanariah, PSZ",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S013")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("P19, Lecture Hall",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S014")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Dewan Astana, Kolej Tuanku Canselor",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 13,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S015")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Arked Lestari",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S016")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Stadium Azman Hashim, UTM",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S017")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Scholar's Inn",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),

                                              if(doc['rentStop'] == "S018")
                                                Container(
                                                    margin: const EdgeInsets.only(left:21),
                                                    child: Text
                                                      ("Perpustakaan Raja Zarith Sofiah",
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontSize: 15,
                                                        color: const Color(0xff656d74),
                                                      ),
                                                    )
                                                ),
                                            ],
                                          ),
                                        ),

                                        Row(
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(left:15),
                                                child:Image(
                                                  height: 12,
                                                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/dot.png'),
                                                ),
                                              )
                                            ]
                                        ),

                                        Row(
                                          children: <Widget>[
                                            Image(
                                              height: 40,
                                              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/end_mark.png'),
                                            ),

                                            if(doc['returnStop'] == "S001")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Angkasa",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S002")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("M01, Kolej Tun Dr Ismail",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S003")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L01, Kolej Tun Hussein Onn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S004")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Cengal",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S005")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("K01, Kolej Tun Razak",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S006")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Food Court Kolej Datin Seri Endon",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S007")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("H01, Kolej Tun Fatimah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S008")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("L50, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S009")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Meranti",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S010")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("G01, Kolej Rahman Putra",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S011")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("N28a, School of Computing",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S012")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Sultanah Zanariah, PSZ",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S013")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("P19, Lecture Hall",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S014")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Dewan Astana, Kolej Tuanku Canselor",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 13,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S015")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Arked Lestari",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S016")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Stadium Azman Hashim, UTM",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S017")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Scholar's Inn",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),

                                            if(doc['returnStop'] == "S018")
                                              Container(
                                                  margin: const EdgeInsets.only(left:15),
                                                  child: Text
                                                    ("Perpustakaan Raja Zarith Sofiah",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 15,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                          ],
                                        ),

                                        Container(
                                          margin: const EdgeInsets.only(top:10, left:5, right: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 6,
                                                  child: Text
                                                    (doc['totalMin'].toInt().toString() + " min " + ((doc['totalMin'] * 60) - (doc['totalMin'].toInt() * 60 )).toInt().toString() + " sec",
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 14,
                                                      color: const Color(0xff656d74),
                                                    ),
                                                  )
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Text(
                                                  "RM " + doc['rentingFee'].toStringAsFixed(2),
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 14,
                                                    color: const Color(0xff656d74),
                                                  ),
                                                  textAlign: TextAlign.right,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                                  .toList());
                        }
                      }

                      else if(snapshot.hasError){
                        return Text("Something went wrong!");
                      }

                      return Text("Loading");
                    }
                ),
              ),
            ),
        ],
      ),
    );
  }


}



const String _svg_8fmw79 =
    '<svg viewBox="0.0 0.0 21.6 21.6" ><path transform="translate(-150.22, -254.68)" d="M 161.0066375732422 254.6779479980469 C 155.0566711425781 254.6779479980469 150.2160034179688 259.5186462402344 150.2160034179688 265.468017578125 C 150.2160034179688 271.4179992675781 155.0566711425781 276.2586669921875 161.0066375732422 276.2586669921875 C 163.3968658447266 276.2586669921875 165.6614227294922 275.4929809570313 167.5553436279297 274.0450744628906 C 167.9118804931641 273.7724914550781 167.9801635742188 273.2625427246094 167.7070770263672 272.9054565429688 C 167.4350433349609 272.5489196777344 166.9251403808594 272.4817504882813 166.5679931640625 272.7537536621094 C 164.9597015380859 273.9832763671875 163.0365142822266 274.6329956054688 161.0066375732422 274.6329956054688 C 155.9529724121094 274.6329956054688 151.8416595458984 270.5216979980469 151.8416595458984 265.468017578125 C 151.8416595458984 260.4149475097656 155.9529724121094 256.3036499023438 161.0066375732422 256.3036499023438 C 166.0602569580078 256.3036499023438 170.1715850830078 260.4149475097656 170.1715850830078 265.468017578125 C 170.1715850830078 266.869384765625 169.8632354736328 268.2143249511719 169.2563323974609 269.4649963378906 C 169.0601348876953 269.8692321777344 169.2286682128906 270.3553161621094 169.6323852539063 270.5515441894531 C 170.0366516113281 270.7471313476563 170.5221710205078 270.5791625976563 170.7188720703125 270.1748657226563 C 171.4341583251953 268.7014770507813 171.7972259521484 267.1175537109375 171.7972259521484 265.468017578125 C 171.7972259521484 259.5186462402344 166.95654296875 254.6779479980469 161.0066375732422 254.6779479980469 Z" fill="#b4bfc1" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_9w6veh =
    '<svg viewBox="5.6 5.6 10.3 10.3" ><path transform="translate(-154.97, -259.43)" d="M 169.8334503173828 265.1091613769531 L 163.4738159179688 267.4539184570313 C 163.4592437744141 267.4588012695313 163.4472961425781 267.4685668945313 163.4332122802734 267.4750366210938 C 163.4012298583984 267.4886169433594 163.3708801269531 267.5048522949219 163.3405456542969 267.5226745605469 C 163.3123474121094 267.5400695800781 163.2847442626953 267.5579528808594 163.2587280273438 267.5779724121094 C 163.232177734375 267.5986328125 163.2077789306641 267.621337890625 163.1839141845703 267.6451721191406 C 163.1600952148438 267.6690368652344 163.1378784179688 267.6933898925781 163.1172943115234 267.719970703125 C 163.0966796875 267.7459716796875 163.0787963867188 267.7730712890625 163.06201171875 267.8018188476563 C 163.0441589355469 267.83154296875 163.0278778076172 267.8619384765625 163.0137786865234 267.8944091796875 C 163.0078125 267.9085693359375 162.9980773925781 267.92041015625 162.9926452636719 267.9350891113281 L 160.6484222412109 274.2947082519531 C 160.5384521484375 274.5922241210938 160.6121368408203 274.9265747070313 160.8364715576172 275.1503601074219 C 160.991455078125 275.3058471679688 161.1995239257813 275.3887634277344 161.4113922119141 275.3887634277344 C 161.5051879882813 275.3887634277344 161.6005401611328 275.3719482421875 161.6921234130859 275.3383178710938 L 168.0517120361328 272.994140625 C 168.0658111572266 272.98876953125 168.0777282714844 272.97900390625 168.0923309326172 272.9730224609375 C 168.1243438720703 272.9589538574219 168.1546783447266 272.9427185058594 168.1850280761719 272.9248352050781 C 168.2132110595703 272.9074401855469 168.2403106689453 272.8901062011719 168.2662963867188 272.8695068359375 C 168.2933959960938 272.8489685058594 168.3177642822266 272.8267517089844 168.3416137695313 272.8028259277344 C 168.365478515625 272.7785339355469 168.3876953125 272.7541198730469 168.4082794189453 272.7276000976563 C 168.4288787841797 272.7015075683594 168.4461975097656 272.6744079589844 168.4635620117188 272.6462707519531 C 168.4814147949219 272.6159362792969 168.4976959228516 272.5855407714844 168.5117950439453 272.5536499023438 C 168.5177612304688 272.5390014648438 168.5274963378906 272.5270690917969 168.5329132080078 272.5123901367188 L 170.8771209716797 266.1533508300781 C 170.9871215820313 265.8553161621094 170.9134368896484 265.5215148925781 170.6891021728516 265.2971801757813 C 170.4652709960938 265.0722961425781 170.1309356689453 264.9985961914063 169.8334503173828 265.1091613769531 Z M 167.4469604492188 270.7583312988281 L 165.2279510498047 268.5392761230469 L 168.7420654296875 267.2442016601563 L 167.4469604492188 270.7583312988281 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_tytbin =
    '<svg viewBox="-285.7 -10.1 28.0 28.0" ><path transform="translate(-285.69, -10.12)" d="M 0 0 L 28 0 L 28 28 L 0 28 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_g6elob =
    '<svg viewBox="-284.2 -7.1 25.0 22.0" ><path transform="translate(-285.19, -10.12)" d="M 15.28571224212646 3 C 9.369049072265625 3 4.571428775787354 7.925556182861328 4.571428775787354 14 L 1 14 L 5.630952835083008 18.75444412231445 L 5.714286327362061 18.9255542755127 L 10.5238094329834 14 L 6.952381134033203 14 C 6.952381134033203 9.270000457763672 10.67857074737549 5.444445133209229 15.28571224212646 5.444445133209229 C 19.89285469055176 5.444445133209229 23.61904525756836 9.270000457763672 23.61904525756836 14 C 23.61904525756836 18.72999954223633 19.89285469055176 22.55555534362793 15.28571224212646 22.55555534362793 C 12.9880952835083 22.55555534362793 10.90476131439209 21.58999633789063 9.40476131439209 20.03777885437012 L 7.714284896850586 21.7733325958252 C 9.654762268066406 23.76555442810059 12.32142925262451 25 15.28571224212646 25 C 21.20237922668457 25 25.99999809265137 20.07444381713867 25.99999809265137 14 C 25.99999809265137 7.925556659698486 21.20237922668457 3 15.28571224212646 3 Z M 14.09523868560791 9.111111640930176 L 14.09523868560791 15.22222232818604 L 19.19047546386719 18.32666778564453 L 20.0476188659668 16.8477783203125 L 15.88095188140869 14.30555629730225 L 15.88095188140869 9.111111640930176 L 14.09523868560791 9.111111640930176 Z" fill="#656d74" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_5d1p4k =
    '<svg viewBox="70.9 764.2 9.9 9.9" ><path transform="translate(-38.12, 509.57)" d="M 113.9467620849609 264.5289306640625 C 111.2308197021484 264.5289306640625 109.02099609375 262.3191223144531 109.02099609375 259.6037292480469 C 109.02099609375 256.8877868652344 111.2308197021484 254.6779479980469 113.9467620849609 254.6779479980469 C 116.6627044677734 254.6779479980469 118.8719787597656 256.8877868652344 118.8719787597656 259.6037292480469 C 118.8719787597656 262.3191223144531 116.6627044677734 264.5289306640625 113.9467620849609 264.5289306640625 Z M 113.9467620849609 256.3036499023438 C 112.1271057128906 256.3036499023438 110.6466522216797 257.7840576171875 110.6466522216797 259.6037292480469 C 110.6466522216797 261.4228515625 112.1271057128906 262.9032897949219 113.9467620849609 262.9032897949219 C 115.7664184570313 262.9032897949219 117.2463226318359 261.4228515625 117.2463226318359 259.6037292480469 C 117.2463226318359 257.7840576171875 115.7664184570313 256.3036499023438 113.9467620849609 256.3036499023438 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_r7yor =
    '<svg viewBox="66.5 775.3 19.1 10.5" ><path transform="translate(-34.37, 500.22)" d="M 119.0939025878906 285.6096496582031 C 118.6452255249023 285.6096496582031 118.2810745239258 285.2460632324219 118.2810745239258 284.7968139648438 C 118.2810745239258 280.3408508300781 114.6558303833008 276.7156372070313 110.2004318237305 276.7156372070313 C 106.7274780273438 276.7156372070313 103.6490173339844 278.9254760742188 102.5408401489258 282.2147521972656 C 102.4032135009766 282.622802734375 102.469856262207 283.0595397949219 102.7239990234375 283.4133911132813 C 102.9846496582031 283.77587890625 103.3889007568359 283.9839782714844 103.8337936401367 283.9839782714844 L 115.0887985229492 283.9839782714844 C 115.5380249023438 283.9839782714844 115.9016418457031 284.3475952148438 115.9016418457031 284.7968139648438 C 115.9016418457031 285.2460632324219 115.5380249023438 285.6096496582031 115.0887985229492 285.6096496582031 L 103.8337936401367 285.6096496582031 C 102.8735580444336 285.6096496582031 101.9653549194336 285.1430969238281 101.4039688110352 284.3616333007813 C 100.8404006958008 283.5776062011719 100.6935501098633 282.6054382324219 101.0002593994141 281.695556640625 C 102.331672668457 277.7446899414063 106.0289916992188 275.0899963378906 110.2004318237305 275.0899963378906 C 115.5526657104492 275.0899963378906 119.9067306518555 279.4446105957031 119.9067306518555 284.7968139648438 C 119.9067306518555 285.2460632324219 119.5431442260742 285.6096496582031 119.0939025878906 285.6096496582031 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _svg_u271hv =
    '<svg viewBox="0.0 0.0 28.0 28.0" ><path  d="M 28 0 L 0 0 L 0 28 L 28 28 L 28 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_u0femm =
    '<svg viewBox="2.0 0.0 24.0 24.0" ><path transform="translate(0.0, -2.0)" d="M 4.400000095367432 2 L 23.60000038146973 2 C 24.92000198364258 2 25.98800086975098 3.080000162124634 25.98800086975098 4.400000095367432 L 26.00000190734863 26.00000190734863 L 21.20000076293945 21.20000076293945 L 4.400000095367432 21.20000076293945 C 3.079999685287476 21.20000076293945 2 20.1200008392334 2 18.80000114440918 L 2 4.400000095367432 C 2 3.080000162124634 3.079999685287476 2 4.400000095367432 2 Z M 4.400000095367432 18.80000114440918 L 22.19600105285645 18.80000114440918 L 22.90400123596191 19.50800132751465 L 23.60000038146973 20.20400047302246 L 23.60000038146973 4.400000095367432 L 4.400000095367432 4.400000095367432 L 4.400000095367432 18.80000114440918 Z M 15.20000076293945 14.00000095367432 L 12.80000114440918 14.00000095367432 L 12.80000114440918 16.40000152587891 L 15.20000076293945 16.40000152587891 L 15.20000076293945 14.00000095367432 Z M 15.20000076293945 6.800000190734863 L 12.80000114440918 6.800000190734863 L 12.80000114440918 11.60000038146973 L 15.20000076293945 11.60000038146973 L 15.20000076293945 6.800000190734863 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_iqp8cs =
    '<svg viewBox="207.0 284.0 26.6 24.6" ><path transform="translate(203.0, 280.0)" d="M 4.392521858215332 6.477694511413574 C 7.773052215576172 10.46354961395264 14.01531887054443 17.8504638671875 14.01531887054443 17.8504638671875 L 14.01531887054443 27.0841064453125 C 14.01531887054443 27.93052291870117 14.76840686798096 28.623046875 15.68884754180908 28.623046875 L 19.0359058380127 28.623046875 C 19.95634841918945 28.623046875 20.70943641662598 27.93052291870117 20.70943641662598 27.0841064453125 L 20.70943641662598 17.8504638671875 C 20.70943641662598 17.8504638671875 26.93496513366699 10.46354961395264 30.31549644470215 6.477694511413574 C 31.16899681091309 5.461993217468262 30.3824405670166 4 28.99341011047363 4 L 5.71461009979248 4 C 4.325580596923828 4 3.539021730422974 5.461993217468262 4.392521858215332 6.477694511413574 Z" fill="#656d74" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_eterkn =
    '<svg viewBox="0.0 0.0 24.0 24.0" ><path  d="M 0 0 L 24 0 L 24 24 L 0 24 L 0 0 Z" fill="none" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_xpntr7 =
    '<svg viewBox="-41.0 -38.0 90.0 90.0" ><path transform="translate(-43.0, -40.0)" d="M 46.95499801635742 1.999999523162842 C 22.1149959564209 1.999999523162842 2 22.16000556945801 2 47 C 2 71.83999633789063 22.1149959564209 92 46.95499801635742 92 C 71.83999633789063 92 92 71.83999633789063 92 47 C 92 22.15999794006348 71.83999633789063 1.999999523162842 46.95499801635742 1.999999523162842 Z M 46.99999618530273 83 C 27.1099967956543 83 10.99999904632568 66.88999176025391 10.99999904632568 47 C 10.99999904632568 27.1100025177002 27.1099967956543 11 46.99999618530273 11 C 66.88999938964844 11 83 27.1100025177002 83 47 C 83 66.88999176025391 66.88999938964844 83 46.99999618530273 83 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_rf7f43 =
    '<svg viewBox="0.0 -20.5 29.4 47.9" ><path transform="translate(-11.0, -27.45)" d="M 18.3555908203125 6.999998569488525 L 11 6.999998569488525 L 11 38.37815093994141 L 36.74456405639648 54.85167694091797 L 40.42235946655273 48.41916275024414 L 18.3555908203125 34.45588302612305 L 18.3555908203125 6.999998569488525 Z" fill="#babdbf" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
