import 'package:UGoBike/AdminCheckBicyclestatus1.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class AdminCheckBicyclestatus extends StatefulWidget {
  @override
  bicyclestatus createState() => bicyclestatus();
}

class bicyclestatus extends State<AdminCheckBicyclestatus>{

  final DateFormat dateFormat = DateFormat('yyyy-MM-dd h:m');

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      backgroundColor: const Color(0xfffafdff),
      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            height: 150,
            width: 500,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50.0),
                bottomLeft: Radius.circular(50.0),
              ),
              color: const Color(0xff656d74),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x26000000),
                  offset: Offset(0, 15),
                  blurRadius: 30,
                ),
              ],
            ),

            child: Stack(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 30, left: 20),
                  child: IconButton(
                      icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                      onPressed: () {
                        Navigator.pop(context);
                      }
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Image(
                    height: 200,
                    image: const AssetImage('assets/images/logo.png'),
                  ),
                ),
              ],
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 15),
            alignment: Alignment.center,
            child: Text(
              "Check Bicycle Status",
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 25,
                color: const Color(0xff656d74),
                fontWeight: FontWeight.w700,
                height: 1.2,
              ),
            )
          ),

          Expanded(
            child: Container(
              margin: const EdgeInsets.only(bottom: 0),
              child: FutureBuilder<QuerySnapshot>(
                future: FirebaseFirestore.instance.collection('Bicycle').get(),
                builder: (context, snapshot){
                  if(snapshot.data == null) return CircularProgressIndicator();

                  if(snapshot.hasError){
                    return Text("Something went wrong!");
                  }

                  else{
                    final List<DocumentSnapshot> documents = snapshot.data.docs;

                    return ListView(
                      children: documents.map((doc) =>
                        Container(
                          margin: const EdgeInsets.only(left: 20, right: 20, bottom: 15),
                          height: 90,
                          width: 500,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black45,
                              width: 1.5,
                            ),
                            borderRadius: BorderRadius.all(
                                Radius.circular(7.0),
                            ),
                            color: Colors.white,
                          ),

                          child: Stack(
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(left: 5, top: 5),
                                child: Text(
                                  doc['bicycleID'],
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 20,
                                    color: const Color(0xff656d74),
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),

                              Container(
                                margin: const EdgeInsets.only(left: 5, top: 30),
                                child: Text(
                                    ("Date Registered: " + dateFormat.format(doc['dateRegister'].toDate()).toString()),
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 17,
                                    color: const Color(0xff656d74),
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),

                              InkWell(
                                onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => AdminCheckBicyclestatus1(doc.data()['bicycleID']))),
                                child: Container(
                                  margin: const EdgeInsets.only(right: 8, bottom: 5),
                                  alignment: Alignment.bottomRight,
                                  child: Text(
                                    "Click For More Details >>>",
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 15,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.italic,
                                    ),
                                  ),
                                ),
                              ),
                            ]
                          ),
                        ),
                      )
                    .toList());
                  }
                }
              ),
            )
          )

        ]
      ),
    );
  }
}
