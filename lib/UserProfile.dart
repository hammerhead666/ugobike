import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import './LogIn.dart';
import './BottomBar.dart';
import './Support.dart';
import './UserGuide.dart';
import './RentingHistory.dart';


class UserProfile extends StatefulWidget{
  @override
  UserProfileState createState(){
    return UserProfileState();
  }
}

final FirebaseAuth auth = FirebaseAuth.instance;
CollectionReference profile = FirebaseFirestore.instance.collection('Profile');


final lastSignInTime = auth.currentUser.metadata.lastSignInTime;
final DateFormat formatter = DateFormat('yyyy-MM-dd');
final lastSignInDate = formatter.format(lastSignInTime);


getCurrentUser() {
  final User user = auth.currentUser;
  final uid = user.uid;

  return uid;
}

Future<void> _signOut() async {
  final GoogleSignIn googleSignIn = GoogleSignIn();

  await FirebaseAuth.instance.signOut();
  await googleSignIn.signOut();
}

class UserProfileState extends State<UserProfile> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xfffafdff),
      body: Stack(
        children: <Widget>[
          //Card(
          //elevation: 5.0,
          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          // clipBehavior: Clip.antiAlias,
          Container(
            margin: const EdgeInsets.only(top:160.0, left: 5, right: 5),
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey[50],
              ),
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              boxShadow: [
                BoxShadow(color: Colors.grey[200], spreadRadius: 2),
              ],
            ),
            height: 185,
            //color: Colors.white,
            child: Row(
              children: <Widget>[
                Image(
                  height: 100,
                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/profile_avatar.png'),
                ),

                Expanded(
                  flex: 8,
                  child: Column(
                    mainAxisSize:MainAxisSize.min,
                    crossAxisAlignment:CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 10, bottom:5),
                          child: FutureBuilder<DocumentSnapshot>(
                              future: profile.doc(getCurrentUser()).get(),
                              builder:
                                  (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                if (snapshot.hasError) {
                                  return Text("Something went wrong");
                                }

                                if (snapshot.connectionState == ConnectionState.done) {
                                  Map<String, dynamic> data = snapshot.data.data();
                                  return Text("${data['userName']}",
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 20,
                                      color: const Color(0xff656d74),
                                      fontWeight: FontWeight.w700,
                                    ),
                                    textAlign: TextAlign.left,
                                  );
                                }

                                return Text("loading");
                              }
                          )
                      ),

                      Padding(
                        padding: EdgeInsets.only(bottom:5),
                        child: FutureBuilder<DocumentSnapshot>(
                            future: profile.doc(getCurrentUser()).get(),
                            builder:
                                (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                              if (snapshot.hasError) {
                                return Text("Something went wrong");
                              }

                              if (snapshot.connectionState == ConnectionState.done) {
                                Map<String, dynamic> data = snapshot.data.data();
                                if((data['userGender'.toString()]).compareTo("Male") == 0){
                                  return Image(
                                    height: 20,
                                    image: FirebaseImage('gs://ugobike-d3e33.appspot.com/gender_male.png'),
                                  );
                                }

                                else if ((data['userGender'.toString()]).compareTo("Female") == 0){
                                  return Image(
                                    height: 20,
                                    image: FirebaseImage('gs://ugobike-d3e33.appspot.com/gender_female.png'),
                                  );
                                }

                                else{
                                  return Text("Please update profile info!",
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 10.5,
                                      color: Colors.redAccent,
                                      fontStyle: FontStyle.italic,
                                    ),
                                    textAlign: TextAlign.left,
                                  );
                                }
                              }

                              return Text("loading");
                            }
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(top: 15, bottom:10),
                        child:RichText(
                          textAlign: TextAlign.start,
                          text: TextSpan(
                              text: 'Edit Profile > ',
                              style: TextStyle(
                                fontFamily: 'Montserrat Light',
                                fontSize: 15,
                                color: const Color(0xffbfa780),
                              ),
                              recognizer: TapGestureRecognizer()..onTap = () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                    builder: (BuildContext context) => FullScreenDialog(),
                                    fullscreenDialog: true,
                                  ),
                                );
                              }
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          //  ),
          GestureDetector(
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => BottomBar(1))),
            child: new Container(
              margin: const EdgeInsets.only(top:350.0),
              height: 70,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 0.2, color: Colors.blueGrey),
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 8,
                    child: Padding(
                      padding: EdgeInsets.only(top: 10, left:30, bottom:10),
                      child: Text(
                        'Renting History',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 17,
                          color: const Color(0xff656d74),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex:2,
                    child: Padding(
                      padding: EdgeInsets.only(top: 10, right:30, bottom:10),
                      child: Text(
                        '>',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 18,
                          color: const Color(0xff656d74),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

          GestureDetector(
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Support())),

            child: new Container(
              margin: const EdgeInsets.only(top:420.0),
              height: 70,
              decoration: BoxDecoration(
                border: Border(
                  // bottom: BorderSide(width: 0.2, color: Colors.blueGrey),
                  // top: BorderSide(width: 0.2, color: Colors.blueGrey),
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 8,
                    child: Padding(
                      padding: EdgeInsets.only(top: 10, left:30, bottom:10),
                      child: Text(
                        'Support',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 17,
                          color: const Color(0xff656d74),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex:2,
                    child: Padding(
                      padding: EdgeInsets.only(top: 10, right:30, bottom:10),
                      child: Text(
                        '>',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 18,
                          color: const Color(0xff656d74),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

          GestureDetector(
            onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => UserGuide())),

            child: new Container(
              margin: const EdgeInsets.only(top:490.0),
              height: 70,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 0.2, color: Colors.blueGrey),
                  top: BorderSide(width: 0.2, color: Colors.blueGrey),
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 8,
                    child: Padding(
                      padding: EdgeInsets.only(top: 10, left:30, bottom:10),
                      child: Text(
                        'User Guide',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 17,
                          color: const Color(0xff656d74),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex:2,
                    child: Padding(
                      padding: EdgeInsets.only(top: 10, right:30, bottom:10),
                      child: Text(
                        '>',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 18,
                          color: const Color(0xff656d74),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top:560.0),
            child: Padding(
              padding: EdgeInsets.only(right: 40, left: 40),
              child: Center(
                child: ButtonTheme(
                  minWidth: 150.0,
                  height: 45.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      //side: BorderSide(color: Colors.red)
                    ),
                    color: const Color(0xff656d74),
                    onPressed: (){
                      _signOut();
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                    child: Text('Sign out',
                        style:
                        TextStyle(fontFamily: 'Montserrat',
                            fontSize: 15,
                            color: const Color(0xffffffff),
                            fontWeight: FontWeight.w700)
                    ),
                  ),
                ),
              ),
            ),
          ),

          Pinned.fromSize(
            bounds: Rect.fromLTWH(0.0, 0.0, 375.0, 146.0),
            size: Size(375.0, 812.0),
            pinLeft: true,
            pinRight: true,
            pinTop: true,
            fixedHeight: true,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50.0),
                  bottomLeft: Radius.circular(50.0),
                ),
                color: const Color(0xff656d74),
                /*boxShadow: [
                    BoxShadow(
                      color: const Color(0x26000000),
                      offset: Offset(0, 15),
                      blurRadius: 30,
                    ),
                  ],*/
              ),
            ),
          ),
          Pinned.fromSize(
            bounds: Rect.fromLTWH(92.0, 14.0, 198.0, 149.0),
            size: Size(375.0, 812.0),
            pinTop: true,
            fixedWidth: true,
            fixedHeight: true,
            child:
            // Adobe XD layer: 'Bicycle-Logo-Design…' (shape)
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(70.0),
                image: DecorationImage(
                  image: const AssetImage('assets/images/logo.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }
}

class FullScreenDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          backgroundColor: Color(0xff656d74),
          leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => BottomBar(3)));
                    }
                );
              }
          ),
          elevation: 15,
          actions: <Widget>[
            FlatButton(
              textColor: Colors.white,
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => EditProfile()));
              },
              child: Text("Edit",
                style: TextStyle(
                  fontFamily: 'Montserrat Thin',
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),

      body: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment(0,-1.03),
            child: (
                Image(
                  height: 200,
                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/profile_background.jpg'),
                )
            ),
          ),

          Container(
            alignment: Alignment(0,-0.52),
            child: Image(
              height: 150,
              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/profile_avatar.png'),
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top:310.0),
            child: ListView(
              children: <Widget>[
                Center(
                  child: Container(
                    padding: const EdgeInsets.only(left:20, right:20, top: 5, bottom: 5),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black38,
                      ),
                      borderRadius: BorderRadius.circular(5),
                      color: const Color(0xff878787).withOpacity(0.2),
                    ),
                    height: 35,
                    width: 350,
                    child: FittedBox(
                        child: FutureBuilder<DocumentSnapshot>(
                            future: profile.doc(getCurrentUser()).get(),
                            builder:
                                (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                              if (snapshot.hasError) {
                                return Text("Something went wrong");
                              }

                              if (snapshot.connectionState == ConnectionState.done) {
                                Map<String, dynamic> data = snapshot.data.data();
                                return Text("${data['userName']}",
                                  style: TextStyle(
                                    fontFamily: 'Montserrat Thin',
                                    //fontSize: 14,
                                    color: Colors.black54,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  textAlign: TextAlign.left,
                                );
                              }
                              return Text("loading");
                            }
                        )
                    ),
                  ),
                ),

                Center(
                  child: Container(
                    margin: const EdgeInsets.only(top: 8),
                    width: 350,
                    child: Row(
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(right:15,left: 15, top:5, bottom: 5),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black38,
                            ),
                            borderRadius: BorderRadius.circular(5),
                            color: const Color(0xff878787).withOpacity(0.2),
                          ),
                          height: 35,
                          width: 120,
                          child: FittedBox(
                              child: FutureBuilder<DocumentSnapshot>(
                                  future: profile.doc(getCurrentUser()).get(),
                                  builder:
                                      (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                    if (snapshot.hasError) {
                                      return Text("Something went wrong");
                                    }

                                    if (snapshot.connectionState == ConnectionState.done) {
                                      Map<String, dynamic> data = snapshot.data.data();
                                      if((data['userGender'.toString()]).compareTo("") == 0){
                                        return Text("No gender info.",
                                          style: TextStyle(
                                            fontFamily: 'Montserrat Thin',
                                            // fontSize: 10.5,
                                            color: Colors.redAccent,
                                            fontStyle: FontStyle.italic,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          textAlign: TextAlign.left,
                                        );
                                      }

                                      return Text("${data['userGender']}",
                                        style: TextStyle(
                                          fontFamily: 'Montserrat Thin',
                                          //fontSize: 14,
                                          color: Colors.black54,
                                          fontWeight: FontWeight.w400,
                                        ),
                                        textAlign: TextAlign.left,
                                      );
                                    }
                                    return Text("loading");
                                  }
                              )
                          ),
                        ),

                        Container(
                          margin: const EdgeInsets.only(left:10),
                          padding: const EdgeInsets.only(right:25, left:25, top: 6, bottom: 6),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black38,
                            ),
                            borderRadius: BorderRadius.circular(5),
                            color: const Color(0xff878787).withOpacity(0.2),
                          ),
                          height: 35,
                          width: 220,
                          child: FittedBox(
                              child: FutureBuilder<DocumentSnapshot>(
                                  future: profile.doc(getCurrentUser()).get(),
                                  builder:
                                      (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                                    if (snapshot.hasError) {
                                      return Text("Something went wrong");
                                    }

                                    if (snapshot.connectionState == ConnectionState.done) {
                                      Map<String, dynamic> data = snapshot.data.data();
                                      if((data['userPhone'.toString()]).compareTo("") == 0){
                                        return Text("No contact info.",
                                          style: TextStyle(
                                            fontFamily: 'Montserrat Thin',
                                            // fontSize: 10.5,
                                            color: Colors.redAccent,
                                            fontStyle: FontStyle.italic,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          textAlign: TextAlign.left,
                                        );
                                      }

                                      return Text("${data['userPhone']}",
                                        style: TextStyle(
                                          fontFamily: 'Montserrat Thin',
                                          //fontSize: 14,
                                          color: Colors.black54,
                                          fontWeight: FontWeight.w400,
                                        ),
                                        textAlign: TextAlign.left,
                                      );
                                    }
                                    return Text("loading");
                                  }
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                Center(
                  child: Container(
                    margin: const EdgeInsets.only(top: 8),
                    padding: const EdgeInsets.only(left:20, right:20, top:5, bottom:5),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black38,
                      ),
                      borderRadius: BorderRadius.circular(5),
                      color: const Color(0xff878787).withOpacity(0.2),
                    ),
                    height: 35,
                    width: 350,
                    child: FittedBox(
                        child: FutureBuilder<DocumentSnapshot>(
                            future: profile.doc(getCurrentUser()).get(),
                            builder:
                                (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                              if (snapshot.hasError) {
                                return Text("Something went wrong");
                              }

                              if (snapshot.connectionState == ConnectionState.done) {
                                Map<String, dynamic> data = snapshot.data.data();
                                return Text("${data['userEmail']}",
                                  style: TextStyle(
                                    fontFamily: 'Montserrat Thin',
                                    //fontSize: 14,
                                    color: Colors.black54,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  textAlign: TextAlign.left,
                                );
                              }
                              return Text("loading");
                            }
                        )
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    margin: const EdgeInsets.only(top: 125),
                    child: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'Last signed in: ',
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 16,
                                  color: const Color(0xff656d74),
                                  fontWeight: FontWeight.w500
                              ),
                            ),
                            TextSpan(
                              text: lastSignInDate.toString(),
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 16,
                                  color: const Color(0xff656d74),
                                  fontWeight: FontWeight.w500
                              ),
                            ),
                          ]
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class EditProfile extends StatefulWidget{
  @override
  EditProfileState createState() => EditProfileState();
}

class EditProfileState extends State<EditProfile>{

  TextEditingController nameController = TextEditingController();
  TextEditingController genderController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  int _selectedGender;

  void initState() {
    super.initState();
    profile.doc(getCurrentUser()).get().then((ds) {
      Map<String, dynamic> data = ds.data();
      if(ds.exists)
      {
        setState(() {
          nameController.text = data['userName'];
          genderController.text = data['userGender'];
          phoneController.text = data['userPhone'];

          if((data['userGender'.toString()]).compareTo("Male") == 0){
            _selectedGender = 0;
          }

          else if((data['userGender'.toString()]).compareTo("Female") == 0){
            _selectedGender = 1;
          }
        });
      }
    });
  }

  List<DropdownMenuItem<int>> genderList = [];

  void loadGenderList() {
    genderList = [];
    genderList.add(new DropdownMenuItem(
      child: new Text('Male'),
      value: 0,
    ));
    genderList.add(new DropdownMenuItem(
      child: new Text('Female'),
      value: 1,
    ));
  }

  List<Widget> getFormWidget(){
    List<Widget> formWidget = new List();
    formWidget.add(new DropdownButton(
      items: genderList,
      value: _selectedGender,
      hint: new Text('Gender'),
      onChanged: (value) {
        setState(() {
          _selectedGender = value;
        });
      },
      isExpanded: true,
    ));

    return formWidget;
  }

  @override
  Widget build(BuildContext context) {
    loadGenderList();
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          backgroundColor: Color(0xff656d74),
          elevation: 15,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            FlatButton(
              textColor: Colors.white,
              onPressed: () {
                String gender;
                if(_selectedGender == 0)
                {
                  gender = "Male";
                }
                else
                {
                  gender = "Female";
                }
                profile.doc(getCurrentUser()).update({"userName": nameController.text});
                profile.doc(getCurrentUser()).update({"userGender": gender});
                profile.doc(getCurrentUser()).update({"userPhone": phoneController.text});
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FullScreenDialog()));
              },
              child: Text("Save",
                style: TextStyle(
                  fontFamily: 'Montserrat Thin',
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),

      body: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment(0,-1.03),
            child: (
                Image(
                  height: 200,
                  image: FirebaseImage('gs://ugobike-d3e33.appspot.com/profile_background.jpg'),
                )
            ),
          ),

          Container(
            alignment: Alignment(0,-0.52),
            child: Image(
              height: 150,
              image: FirebaseImage('gs://ugobike-d3e33.appspot.com/profile_avatar.png'),
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top:310.0),
            child: ListView(
              children: <Widget>[
                Center(
                  child: Container(
                    padding: const EdgeInsets.only(bottom:5),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black38,
                      ),
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                    ),
                    height: 40,
                    width: 350,
                    child: TextFormField(
                      style: TextStyle(
                        fontFamily: 'Montserrat Thin',
                        fontSize: 16.5,
                        color: Colors.black87,
                        fontWeight: FontWeight.w400,
                      ),
                      controller: nameController,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        // hintText: "Hint here",
                      ),
                    ),
                  ),
                ),

                Center(
                  child: Container(
                    margin: const EdgeInsets.only(top: 8),
                    width: 350,
                    child: Row(
                      children: <Widget>[
                        Container(
                            padding: const EdgeInsets.only(right:15, left: 15, bottom: 5),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                              ),
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white,
                            ),
                            height: 40,
                            width: 120,
                            child: new ListView(
                              shrinkWrap: true,
                              children: getFormWidget(),
                            )
                        ),

                        Container(
                          margin: const EdgeInsets.only(left:10),
                          padding: const EdgeInsets.only(right:25, left:25),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black38,
                            ),
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white,
                          ),
                          height: 40,
                          width: 220,
                          child: TextFormField(
                            style: TextStyle(
                              fontFamily: 'Montserrat Thin',
                              fontSize: 16.5,
                              color: Colors.black87,
                              fontWeight: FontWeight.w400,
                            ),
                            controller: phoneController,
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintText: "Contact Number",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                Center(
                  child: Container(
                    margin: const EdgeInsets.only(top: 8),
                    padding: const EdgeInsets.only(left:20, right:20, top:5, bottom:5),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black38,
                      ),
                      borderRadius: BorderRadius.circular(5),
                      color: const Color(0xff878787).withOpacity(0.2),
                    ),
                    height: 35,
                    width: 350,
                    child: FittedBox(
                        child: FutureBuilder<DocumentSnapshot>(
                            future: profile.doc(getCurrentUser()).get(),
                            builder:
                                (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                              if (snapshot.hasError) {
                                return Text("Something went wrong");
                              }

                              if (snapshot.connectionState == ConnectionState.done) {
                                Map<String, dynamic> data = snapshot.data.data();
                                return Text("${data['userEmail']}",
                                  style: TextStyle(
                                    fontFamily: 'Montserrat Thin',
                                    //fontSize: 14,
                                    color: Colors.black54,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  textAlign: TextAlign.left,
                                );
                              }
                              return Text("loading");
                            }
                        )
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}


